#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
parser.add_option( '-d', '--isMC', dest = 'is_simulation',
                   action = 'store', type = int, default = 0,
                   help = 'is data or MC' )
parser.add_option( '-f', '--file-list', dest = 'file_list',
                   action = 'store', type = 'string', default = 'file.list',
                   help = 'name of file list' )
parser.add_option( '-z', '--DerivationVesion', dest = 'new_Deriv',
                   action = 'store', type = int, default = 0,
                   help = 'is New or Old Derovation' )
parser.add_option( '-y', '--year', dest = 'data_year',
                   action = 'store', type = 'int', default = '17',
                   help = 'year of data taking' )

#ROOT.SH.ScanDir().filePattern( 'DAOD_HIGG4D2.12822254._000106.pool.root.1' ).scan( sh, inputFilePath )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
#inputFilePath = '/eos/user/j/jugao/HIGG4D2/mc/mc16_13TeV.410013.PowhegPythiaEvtGen_P2012_Wt_inclusive_top.deriv.DAOD_HIGG4D2.e3753_s3126_r9364_r9315_p3401'
ROOT.SH.readFileList(sh, 'testSample',options.file_list);
#ROOT.SH.ScanDir().filePattern( 'DAOD_HIGG4D2.12935864._000043.pool.root.1' ).scan( sh, inputFilePath )
#sh.print()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )

output = ROOT.EL.OutputStream('myOutput')
job.outputAdd(output)
ntuple = ROOT.EL.NTupleSvc('myOutput')
job.algsAdd(ntuple)


# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'LFVAnalysis/AnalysisAlg', isMC = options.is_simulation,outputName = "myOutput", Year = options.data_year)
job.algsAdd( config )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )

