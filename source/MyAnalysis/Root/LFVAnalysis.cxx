#include "MyAnalysis/LFVAnalysis.h"

LFVAnalysis :: LFVAnalysis (const std::string& name,
                            ISvcLocator *pSvcLocator)
    :  MyxAODAnalysis (name, pSvcLocator) {
//       TH1::SetDefaultSumw2(kTRUE);
    declareProperty( "outputName", m_outputName="myOutput",
                     "Descriptive name for the processed sample" );
    DoEleID = true;
    DoEleIso = true;
    DoMuIso = true;

}

bool LFVAnalysis :: init() {
    TFile *outputFile = wk()->getOutputFile (m_outputName);
    metaTree =  new TTree("metaTree","metaTree");
    metaTree->Branch("totalWeightedEntries", &sumOfWeights);
    metaTree->Branch("inputFileName", &filename);
    metaTree->Branch("nEventsProcessed", &nEventsProcessed);
    metaTree->SetDirectory(outputFile);
    lfvTree = new TTree("LFV","LFV");
    lfvTree->SetDirectory(outputFile);

//Event Info
    lfvTree->Branch("EventWeight",&EventWeight);
    lfvTree->Branch("EventWeight_NoTrigSF",&EventWeight_NoTrigSF);
    lfvTree->Branch("PrwWeight",&PrwWeight);
    lfvTree->Branch("MCWeight",&MCWeight);
    lfvTree->Branch("Event_Trigger_Match",&Event_Trigger_Match);
    lfvTree->Branch("Event_Trigger_Pass",&Event_Trigger_Pass);
    lfvTree->Branch("RunNo",&RunNo);
    lfvTree->Branch("lumB",&lumB);
    lfvTree->Branch("EventTag",&EventTag);
    lfvTree->Branch("eventNumber",&eventNumber);
    lfvTree->Branch("dPhi_ej",&dPhi_ej);
    lfvTree->Branch("dPhi_ll",&dPhi_ll);
    lfvTree->Branch("isKeep",&isKeepEvent);
    lfvTree->Branch("xsec",&xsec);
    lfvTree->Branch("kFactor",&kFactor);
    lfvTree->Branch("isEvtClean",&isEvtClean);
    lfvTree->Branch("isVertexGood",&isVertexGood);
    lfvTree->Branch("isTrigPass",&isTrigPass);
    lfvTree->Branch("Final_elpt",&Final_elpt);
    lfvTree->Branch("Final_eleta",&Final_eleta);
    lfvTree->Branch("Final_elphi",&Final_elphi);
    lfvTree->Branch("Final_mupt",&Final_mupt);
    lfvTree->Branch("Final_mueta",&Final_mueta);
    lfvTree->Branch("Final_muphi",&Final_muphi);
    lfvTree->Branch("Final_taupt", &Final_taupt);
    lfvTree->Branch("Final_taueta",&Final_taueta);
    lfvTree->Branch("Final_tauphi",&Final_tauphi);

//  lfvTree->Branch("xsec",&xsec);

//MET Info
    lfvTree->Branch("MET_Et",&MET_Et);
    lfvTree->Branch("MET_Phi",&MET_Phi);
    lfvTree->Branch("Event_met", &event_met);
    lfvTree->Branch("Event_met_Eta", &event_met_eta);
    lfvTree->Branch("Event_met_Phi", &event_met_phi);
    lfvTree->Branch("Event_met_M", &event_met_m);
    lfvTree->Branch("Event_mT",&event_mT);

//Electron Para
    lfvTree->Branch("Ele_OQ",&Ele_OQ);
    lfvTree->Branch("Ele_Charge",&Ele_Charge);
    lfvTree->Branch("Ele_pt",&Ele_pt);
    lfvTree->Branch("Ele_eta",&Ele_eta);
    lfvTree->Branch("Ele_eta_calo",&Ele_eta_calo);
    lfvTree->Branch("Ele_phi",&Ele_phi);
    lfvTree->Branch("Ele_d0sig",&Ele_d0sig);
    lfvTree->Branch("Ele_dz0",&Ele_dz0);
    lfvTree->Branch("Ele_d0pass",&Ele_d0pass);
    lfvTree->Branch("Ele_z0pass",&Ele_z0pass);
    lfvTree->Branch("Ele_MuonOLR",&Ele_MuonOLR);
    lfvTree->Branch("Ele_ID_Medium",&Ele_ID_Medium);
    lfvTree->Branch("Ele_ID_Tight",&Ele_ID_Tight);
    lfvTree->Branch("Ele_ID_Loose",&Ele_ID_Loose);
    lfvTree->Branch("Ele_Iso",&Ele_Iso);
    lfvTree->Branch("Ele_Iso_Loose",&Ele_Iso_Loose);
    lfvTree->Branch("Ele_isTrigMch",&Ele_isTrigMch);
    lfvTree->Branch("Ele_TrigEff",&Ele_TrigEff);
    lfvTree->Branch("Ele_RecoSF",&Ele_RecoSF);
    lfvTree->Branch("Ele_IsoSF",&Ele_IsoSF);
    lfvTree->Branch("Ele_IDSF",&Ele_IDSF);
    lfvTree->Branch("Ele_L1CaloSF",&Ele_L1CaloSF);
    lfvTree->Branch("Ele_isOverlap",&Ele_isOverlap);


//Muon Para
    lfvTree->Branch("Mu_pt",&Mu_pt);
    lfvTree->Branch("Mu_eta",&Mu_eta);
    lfvTree->Branch("Mu_phi",&Mu_phi);
    lfvTree->Branch("Mu_d0sig",&Mu_d0sig);
    lfvTree->Branch("Mu_dz0",&Mu_dz0);
    lfvTree->Branch("Mu_d0pass",&Mu_d0pass);
    lfvTree->Branch("Mu_z0pass",&Mu_z0pass);
    lfvTree->Branch("Mu_charge",&Mu_charge);
    lfvTree->Branch("Mu_isHighPt",&Mu_isHighPt);
    lfvTree->Branch("Mu_isID",&Mu_isID);
    lfvTree->Branch("Mu_Iso",&Mu_Iso);
    lfvTree->Branch("Mu_TrigSF",&Mu_TrigSF);
    lfvTree->Branch("Mu_RecoSF",&Mu_RecoSF);
    lfvTree->Branch("Mu_TrackSF",&Mu_TrackSF);
    lfvTree->Branch("Mu_IsoSF",&Mu_IsoSF);
    lfvTree->Branch("Mu_isTrigMch",&Mu_isTrigMch);
    lfvTree->Branch("Mu_isOverlap",&Mu_isOverlap);

//Tau Para
    lfvTree->Branch("Tau_isOverlap",&Tau_isOverlap);
    lfvTree->Branch("Tau_pt",&Tau_pt);
    lfvTree->Branch("Tau_eta",&Tau_eta);
    lfvTree->Branch("Tau_phi",&Tau_phi);
    lfvTree->Branch("Tau_ntracks",&Tau_ntracks);
    lfvTree->Branch("Tau_charge",&Tau_charge);
    lfvTree->Branch("TauSelector",&TauSelector);
    lfvTree->Branch("TauSelector_WithOLR",&TauSelector_WithOLR);
    lfvTree->Branch("Tau_RecoSF",&Tau_RecoSF);
    lfvTree->Branch("Tau_IDSF",&Tau_IDSF);
    lfvTree->Branch("Tau_EleOLRSF",&Tau_EleOLRSF);

//Jet Para
    lfvTree->Branch("Jet_pt",&Jet_pt);
    lfvTree->Branch("Jet_eta",&Jet_eta);
    lfvTree->Branch("Jet_phi",&Jet_phi);
    lfvTree->Branch("Jet_m",&Jet_m);
    lfvTree->Branch("Jet_isGoodB",&Jet_isGoodB);
    lfvTree->Branch("Jet_JVTSF",&Jet_JVTSF);
    lfvTree->Branch("Jet_BTInefSF",&Jet_BTInefSF);
    lfvTree->Branch("Jet_BTSF",&Jet_BTSF);
    lfvTree->Branch("N_jets",&N_jets);

    lfvTree->Branch("CleanEventNum",&m_numCleanEvents );
    lfvTree->Branch("Mu_TriggerMatched",&Mu_TriggerMatched);
    lfvTree->Branch("Ele_TriggerMatched",&Ele_TriggerMatched);
    lfvTree->Branch("Ele_isTight",&Ele_isTight);
    lfvTree->Branch("Ele_isLoose",&Ele_isLoose);
    lfvTree->Branch("Ele_isLoose_IDLoose",&Ele_isLoose_IDLoose);


    h_CleanEventNum = new TH1D("h_CleanEventNum", "h_CleanEventNum", 500, 0, 500);
    wk()->addOutput (h_CleanEventNum);

    h_CutFlow_emu = new TH1D("h_CutFlow_emu","h_CutFlow_emu",50,0,50);
    wk()->addOutput(h_CutFlow_emu);
    h_CutFlow_emu->GetXaxis()->SetBinLabel(1,"All events");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(2,"Event Cleaning");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(3,"Vertex");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(4,"Trigger");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(5,"BadMuonEvent");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(6,"BadJetEvent");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(7,"Leptons");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(8,"Trigger Match");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(9,"OS pair");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(10,"DeltaPhi");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(11,"b-jet veto");
    h_CutFlow_emu->GetXaxis()->SetBinLabel(12,"MET");

    h_CutFlow_etau = new TH1D("h_CutFlow_etau","h_CutFlow_etau",50,0,50);
    wk()->addOutput(h_CutFlow_etau);
    h_CutFlow_etau->GetXaxis()->SetBinLabel(1,"All events");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(2,"Event Cleaning");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(3,"Vertex");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(4,"Trigger");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(5,"BadMuonEvent");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(6,"BadJetEvent");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(7,"Leptons");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(8,"Trigger Match");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(9,"OS pair");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(10,"DeltaPhi");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(11,"b-jet veto");
    h_CutFlow_etau->GetXaxis()->SetBinLabel(12,"MET");

    h_CutFlow_mutau = new TH1D("h_CutFlow_mutau","h_CutFlow_mutau",50,0,50);
    wk()->addOutput(h_CutFlow_mutau);
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(1,"All events");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(2,"Event Cleaning");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(3,"Vertex");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(4,"Trigger");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(5,"BadMuonEvent");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(6,"BadJetEvent");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(7,"Leptons");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(8,"Trigger Match");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(9,"OS pair");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(10,"DeltaPhi");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(11,"b-jet veto");
    h_CutFlow_mutau->GetXaxis()->SetBinLabel(12,"MET");


    h_CutFlow_el = new TH1D("h_CutFlow_el","h_CutFlow_el",50,0,50);
    wk()->addOutput(h_CutFlow_el);
    h_CutFlow_muon = new TH1D("h_CutFlow_muon","h_CutFlow_muon",50,0,50);
    wk()->addOutput(h_CutFlow_muon);
    h_CutFlow_tau = new TH1D("h_CutFlow_tau","h_CutFlow_tau",50,0,50);
    wk()->addOutput(h_CutFlow_tau);
    h_CutFlow_jet = new TH1D("h_CutFlow_jet","h_CutFlow_jet",50,0,50);
    wk()->addOutput(h_CutFlow_jet);
    h_CutFlow_ph = new TH1D("h_CutFlow_ph","h_CutFlow_ph",50,0,50);
    wk()->addOutput(h_CutFlow_ph);

    m_MetCorrMuons         = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
    m_MetCorrElectrons     = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    m_MetCorrTaus          = new xAOD::TauJetContainer(SG::VIEW_ELEMENTS);
    m_MetCorrPhotons       = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS);
    doMetCalculation =1;

    trigList_el["2017"].push_back("HLT_e26_lhtight_nod0_ivarloose");
    trigList_el["2017"].push_back("HLT_e60_lhmedium_nod0");
    trigList_el["2017"].push_back("HLT_e140_lhloose_nod0");
    return true;

}

bool LFVAnalysis :: loop () {
    m_store->clear(); //need to be called here again in case of systematic loops

    EventWeight = 1.;
    PrwWeight = 1.;
    RunNo = -1;
    EventTag=-1;
    lumB = -1;
    Event_Trigger_Pass=false;
    Event_Trigger_Match=false;

    N_jets = 0;
    dPhi_ej = 0;
    event_mT = 0;
    MET_Phi       = 0.0;
    MET_Et        = 0.0;
    event_met = 0.0;
    event_met_phi = 0.0;
    event_met_eta = 0.0;
    event_met_m = 0.0;
    isKeepEvent = true;
    isEvtClean = false;
    isVertexGood = false;
    isTrigPass = false;
    Final_elpt = 0;
    Final_eleta = 0;
    Final_elphi = 0;
    Final_mupt = 0;
    Final_mueta = 0;
    Final_muphi = 0;
    Final_taupt = 0;
    Final_taueta = 0;
    Final_tauphi = 0;

    Ele_OQ.clear();
    Ele_Charge.clear();
    Ele_pt.clear();
    Ele_eta.clear();
    Ele_eta_calo.clear();
    Ele_phi.clear();
    Ele_d0sig.clear();
    Ele_dz0.clear();
    Ele_d0pass.clear();
    Ele_z0pass.clear();
    Ele_MuonOLR.clear();
    Ele_ID_Loose.clear();
    Ele_ID_Medium.clear();
    Ele_ID_Tight.clear();
    Ele_Iso.clear();
    Ele_Iso_Loose.clear();
    Ele_isTrigMch.clear();
    Ele_TrigEff.clear();
    Ele_RecoSF.clear();
    Ele_IsoSF.clear();
    Ele_IDSF.clear();
    Ele_L1CaloSF.clear();
    Ele_isOverlap.clear();

    Tau_isOverlap.clear();
    Tau_pt.clear();
    Tau_eta.clear();
    Tau_phi.clear();
    Tau_ntracks.clear();
    Tau_charge.clear();
    TauSelector.clear();
    TauSelector_WithOLR.clear();
    Tau_RecoSF.clear();
    Tau_IDSF.clear();
    Tau_EleOLRSF.clear();

    Jet_pt.clear();
    Jet_eta.clear();
    Jet_phi.clear();
    Jet_m.clear();
    Jet_isGoodB.clear();
    Jet_JVTSF.clear();
    Jet_BTInefSF.clear();
    Jet_BTSF.clear();


    Mu_pt.clear();
    Mu_eta.clear();
    Mu_phi.clear();
    Mu_d0sig.clear();
    Mu_dz0.clear();
    Mu_d0pass.clear();
    Mu_z0pass.clear();
    Mu_charge.clear();
    Mu_isHighPt.clear();
    Mu_isID.clear();
    Mu_Iso.clear();
    Mu_TrigSF.clear();
    Mu_RecoSF.clear();
    Mu_IsoSF.clear();
    Mu_TrackSF.clear();
    Mu_isTrigMch.clear();
    Mu_isOverlap.clear();

    Mu_TriggerMatched = false;
    Ele_TriggerMatched = false;
    Ele_isTight.clear();
    Ele_isLoose.clear();
    Ele_isLoose_IDLoose.clear();

//  int mc_channel_number = 0;
    int el_n = electrons->size();
    int mu_n = muons->size();
    int tau_n = taus->size();
    int jet_n = jets->size();
    int ph_n = photons->size();

    bool ElectronTriggerPassed = false;
    bool MuonTriggerPassed = false;
    bool MuonTriggerMatched = false;
    bool ElectronTriggerMatched = false;
    Event_Trigger_Pass = false;
    Event_Trigger_Match =false;
    bool BadJetEvent = false;
    bool BadMuonEvent = false;

    int NumberGoodLooseMuons = 0;
    int NumberGoodMuons = 0;
    int NumberGoodElectrons = 0;
    int NumberGoodLooseElectrons = 0;
    int NumberGoodLooseIDLooseElectrons = 0;
    int NumberGoodTaus = 0;


    double Electron_Iso_SF = 1.0;
    double Electron_ID_SF = 1.0;
    double Electron_Reco_SF = 1.0;
    double Electron_Trig_Eff = 1.0;
    double Electron_L1Calo_Eff = 1.0;
    double Muon_Reco_SF = 1.0;
    double Muon_Track_SF =1.0;
    double Muon_Iso_SF = 1.0;

    double TauRecoSF = 1.0;
    double TauIDSF = 1.0;
    double TauEleOLRSF = 1.0;
    float JVTSF = 1.0;
    float JetSF = 1.0;
    float BTaggingSFtot = 1.0;
    float BTaggingSF = 1.0;
    float inBTaggingSFtot = 1.0;

    double Electron_Trig_Eff_final = 1.0;
    double Muon_Trig_Eff_final = 1.0;

    double xSec = 0.0;
    bool m_electronMuon = false;
    bool m_electronTau = false;
    bool m_muonTau = false;
    bool isGoodBJet = false;
    double Dphi = 0.0;
    TLorentzVector Propagator(1,1,1,1);
    TLorentzVector LooseMuon(1,1,1,1), Muon(1,1,1,1), Electron(1,1,1,1), LooseElectron(1,1,1,1), Tau(1,1,1,1), Neutrino(1,1,1,1), Neutrino2(1,1,1,1), Jet(1,1,1,1);


    h_CutFlow_emu->Fill(0);
    h_CutFlow_etau->Fill(0);
    h_CutFlow_mutau->Fill(0);
    h_CutFlow_el->Fill(0.0,el_n);
    h_CutFlow_muon->Fill(0.0,mu_n);
    h_CutFlow_tau->Fill(0.0,tau_n);
    h_CutFlow_jet->Fill(0.0,jet_n);
    h_CutFlow_ph->Fill(0.0,ph_n);


    RunNo = runNumber;
    lumB = eventInfo->lumiBlock();
    eventInfo->auxdecor<bool> ("EventonCrack") = false;

    auto els_shallowCopy = xAOD::shallowCopyContainer( *electrons );
    elsCorr = els_shallowCopy.first;
    m_store->record( els_shallowCopy.first,  "CalibElectrons"    );
    m_store->record( els_shallowCopy.second, "CalibElectronsAux.");

    auto mus_shallowCopy = xAOD::shallowCopyContainer( *muons );
    muonsCorr = mus_shallowCopy.first;
    m_store->record( mus_shallowCopy.first,  "CalibMuons"    );
    m_store->record( mus_shallowCopy.second, "CalibMuonsAux.");

    auto taus_shallowCopy = xAOD::shallowCopyContainer( *taus );
    tausCorr = taus_shallowCopy.first;
    m_store->record( taus_shallowCopy.first,  "CalibTaus"    );
    m_store->record( taus_shallowCopy.second, "CalibTausAux.");

    auto phs_shallowCopy = xAOD::shallowCopyContainer( *photons);
    phsCorr = phs_shallowCopy.first;
    m_store->record( phs_shallowCopy.first,  "CalibPhs"      );
    m_store->record( phs_shallowCopy.second, "CalibPhsAux."  );

    auto jets_shallowCopy = xAOD::shallowCopyContainer( *jets);
    jetsCorr = jets_shallowCopy.first;
    m_store->record( jets_shallowCopy.first,  "CalibJets"      );
    m_store->record( jets_shallowCopy.second,  "CalibJetsAux." );



    m_MetCorrElectrons->clear();
    m_MetCorrMuons->clear();
    m_MetCorrPhotons->clear();
    m_MetCorrTaus->clear();

//Triger List


    vec_triggers_el = &trigList_el["2017"];
    if(isMC) {
        xSec = m_mcInfo->GetSampleCrossSection( eventInfo->mcChannelNumber() );
        kFactor = GetKfactor();
//   cout<<"kF="<<kFactor<<"  " <<"xsec="<<xSec<<endl;
        prwWeight();
        PrwWeight = PRWwei;
        MCWeight = Eventwei;
        EventWeight = Eventwei * PRWwei;
        xsec = xSec;
        isKeepEvent = m_mcOverlap->KeepEvent(xTruthParticleContainer, eventInfo->mcChannelNumber());
        if( !isKeepEvent ) return EL::StatusCode::SUCCESS;
//    cout<<eventInfo->auxdecor<unsigned int>("RandomRunNumber")<<endl;
//    cout << RandomRunNumber <<endl;
//	  mc_channel_number = eventInfo->mcChannelNumber();
//	  xsec = mcInfo->GetSampleCrossSection( mc_channel_number  );
    }

    if (!isMC) {
        if(!passGRL()) return false;
    }
    if (!passEventCleaning()) return false;
    if (!JetEventCleaning()) return false;
    isEvtClean = true;
    m_numCleanEvents++;
    h_CutFlow_emu->Fill(1);
    h_CutFlow_etau->Fill(1);
    h_CutFlow_mutau->Fill(1);
    h_CutFlow_el->Fill(1.0,el_n);
    h_CutFlow_muon->Fill(1.0,mu_n);
    h_CutFlow_tau->Fill(1.0,tau_n);
    h_CutFlow_jet->Fill(1.0,jet_n);
    h_CutFlow_ph->Fill(1.0,ph_n);
    h_CleanEventNum->Fill( m_numCleanEvents );


    if (!passVTX ()) return false;
    isVertexGood = true;
    h_CutFlow_emu->Fill(2);
    h_CutFlow_etau->Fill(2);
    h_CutFlow_mutau->Fill(2);
    h_CutFlow_el->Fill(2.0,el_n);
    h_CutFlow_muon->Fill(2.0,mu_n);
    h_CutFlow_tau->Fill(2.0,tau_n);
    h_CutFlow_jet->Fill(2.0,jet_n);
    h_CutFlow_ph->Fill(2.0,ph_n);

    if (passTrigger(vw_triggers_muLFV,(sizeof(vw_triggers_muLFV))/(sizeof(string)))) MuonTriggerPassed =true;
    if (runNumber <= 284484 && runNumber > 0) {
        if (passTrigger(vw_triggers_el15LFV,(sizeof(vw_triggers_el15LFV))/(sizeof(string)))) ElectronTriggerPassed = true;
    }
    if (runNumber > 284484 ) {
        if (passTrigger(vw_triggers_elLFV,(sizeof(vw_triggers_elLFV))/(sizeof(string)))) ElectronTriggerPassed = true;
    }
    if( MuonTriggerPassed == false && ElectronTriggerPassed == false ) return false;
    if( !(MuonTriggerPassed == false && ElectronTriggerPassed == false) ) Event_Trigger_Pass=true;
    isTrigPass = true;
    h_CutFlow_emu->Fill(3);
    h_CutFlow_etau->Fill(3);
    h_CutFlow_mutau->Fill(3);
    h_CutFlow_el->Fill(3.0,el_n);
    h_CutFlow_muon->Fill(3.0,mu_n);
    h_CutFlow_tau->Fill(3.0,tau_n);
    h_CutFlow_jet->Fill(3.0,jet_n);
    h_CutFlow_ph->Fill(3.0,ph_n);

//================================================
//================Loop for MET====================
//================================================

    int el_count = -1;
    for( auto el : *elsCorr ) {
        el->auxdecor<char>("isPreSel") = false;
        el_count++;
        if (el->caloCluster() == nullptr || el->trackParticle() == nullptr) continue;
//	elPtr->caloCluster()->etaBE(2)
        m_elcali->setRandomSeed(eventInfo->eventNumber() + 100 * el_count);
        m_elcali->applyCorrection(*el);

        double el_author = ((el)->author());
        if ( !(el_author == 1 || el_author == 16) ) continue;

        if ( !(((el)->pt())*GeV > 65.0) ) continue;

        double el_eta = (el)->caloCluster()->etaBE(2);
        if (  fabs(el_eta) > 2.47 || ( fabs(el_eta) > 1.37 && fabs(el_eta) < 1.52)  ) continue;

//        cout<<(el).auxdataConst<char>("DFCommonElectronsLHMedium")<<endl;
        bool OQ = (el)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON);

        if ( !OQ ) continue;
//        if( (el)->auxdata<char>("DFCommonElectronsLHMedium") ) {cout <<"AAAAAA"<<endl;}

        if (!((el)->auxdata<char>("LHMedium")) && !((el)->auxdata<char>("LHTight"))) continue;

        const xAOD::TrackParticle* trk = (el)->trackParticle();
        d0cal = 0;
        z0cal = 0;
        if (trk) d0z0CAL(trk);

        if(fabs(d0cal) > 5.0) continue;

        if(!z0cal) continue;

        el->auxdecor<char>("isPreSel") = true;

        if (runNumber <= 311481 && runNumber > 0 && isMC) {
            if ( fabs(el_eta) > 1.37 && fabs(el_eta) < 1.52)
                eventInfo->auxdecor<bool> ("EventonCrack") = true;
        }
        m_MetCorrElectrons->push_back(el);

    }


    for( auto mu : *muonsCorr ) {
        mu->auxdecor<char>("isPreSel") = false;

//        if(runNumber <= 284484 && runNumber > 0) {
//            if(m_muonCalibrationAndSmearingTool2015->applyCorrection(*mu) == CP::CorrectionCode::Error) {
//                Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
//            }
//	}

        if(runNumber <= 311481 && runNumber > 0) {
            if(m_muonCalibrationAndSmearingTool2016->applyCorrection(*mu) == CP::CorrectionCode::Error) {
                Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
            }
        }
        if(runNumber <= 340453 && runNumber > 311481) {
            if(m_muonCalibrationAndSmearingTool2017->applyCorrection(*mu) == CP::CorrectionCode::Error) {
                Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
            }
        }

        if(runNumber > 340453) {
            if(m_muonCalibrationAndSmearingTool2018->applyCorrection(*mu) == CP::CorrectionCode::Error) {
                Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
            }
        }



        if((mu)->pt()*GeV<65.0)continue;

        if(mu->muonType() != xAOD::Muon::Combined ) continue;

        if(!(m_muonSelectionHighPt->accept(*mu)))continue;

        const xAOD::TrackParticle* trk = mu->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle);
        d0cal = 0;
        z0cal = 0;
        if (trk) d0z0CAL(trk);

        if(fabs(d0cal) > 3.0) continue;

        if(!z0cal) continue;

        mu->auxdecor<char>("isPreSel") = true;
        m_MetCorrMuons->push_back(mu);
    }

    for( auto tau : *tausCorr ) {
        tau->auxdecor<char>("isPreSel") = false;
        if( isMC ) {
            xTruthTau = T2MT->getTruth(*tau);
        }

        if(isMC) {
            if( TauSmeTool->applyCorrection(*tau) == CP::CorrectionCode::Error) {
                Error("execute()", "TauSmearingTool returns Error CorrectionCode");
            }
        }

        if( !TauSelTool_NoOLR->accept( *tau ) ) continue;
        m_MetCorrTaus->push_back(tau);


        if( !TauSelTool->accept( *tau ) ) continue;
        tau->auxdecor<char>("isPreSel") = true;
    }

//    for( auto ph: *phsCorr) {
//
//        ph->auxdecor<char>("isPreSel") = false;
//        if((ph->pt())*GeV < 10.0) continue;
////        ANA_CHECK((m_elcali->applyCorrection(*ph)));
//        h_CutFlow_ph->Fill(4);
//
//        if((ph->pt())*GeV < 10.0) continue;
//        h_CutFlow_ph->Fill(5);
//
////    cout << "CC1"<<endl;
////        double ph_eta = (ph)->caloCluster()->etaBE(2);
////        if (  fabs(ph_eta) > 2.47 || ( fabs(ph_eta) > 1.37 && fabs(ph_eta) < 1.52)  ) continue;
//        h_CutFlow_ph->Fill(6);
////    cout << "CCC1"<<endl;
//
//        if (! (ph)->isGoodOQ(xAOD::EgammaParameters::BADCLUSPHOTON) ) continue;
//        h_CutFlow_ph->Fill(7);
////    cout << "CCC2"<<endl;
//
//        if ( ( ((ph)->author() != xAOD::EgammaParameters::AuthorPhoton) && ((ph)->author() != xAOD::EgammaParameters::AuthorAmbiguous) ) ) continue;
//        h_CutFlow_ph->Fill(8);
//
////    cout << "CC2"<<endl;
//        bool clean =false;
//        bool cleanDel =false;
//        if(OldDeriv) {
//            clean      = PhotonHelpers::passOQquality(ph);
//            cleanDel   = PhotonHelpers::passOQqualityDelayed(ph);
//        } else {
//            clean = (ph)->auxdata<char>("DFCommonPhotonsCleaning");
//            cleanDel = (ph)->auxdata<char>("DFCommonPhotonsCleaningNoTime");
//        }
//
//        if( !clean    ) {
//            ph->auxdecor<char>("isPreSel") = false;
//            continue;
//        }
//        if( !cleanDel ) {
//            ph->auxdecor<char>("isPreSel") = false;
//            continue;
//        }
//        h_CutFlow_ph->Fill(9);
////    cout << "CC3"<<endl;
//
//        ph->auxdecor<char>("isPreSel") = true;
//        m_MetCorrPhotons->push_back(ph);
//    }

//    cout << "CC0"<<endl;
    if(isMC)m_jetFrwJvtTool->tagTruth(jetsCorr,m_thjetCont);

    for( auto jet: *jetsCorr) {
        jet->auxdecor<char>("isPreSel") = false;
        m_jetCalibration->applyCalibration(*jet);
    }
//    cout << "CC4"<<endl;

    m_jetFrwJvtTool->modify(*jetsCorr);
    for( auto jet: *jetsCorr) {
        float newjvt = m_pjvtag->updateJvt(*jet);
        jet->auxdata<float>("Jvt") = newjvt;
        if( (jet)->pt()*0.001 < 25.0 ) continue;
        //   if( (jet)->pt()*0.001 > 60.0 ) continue;
        if( fabs( (jet)->eta() ) > 2.5 ) continue;
        jet->auxdecor<char>("isPreSel") = true;

    }


    ANA_CHECK(toolBox->masterTool->removeOverlaps( elsCorr, muonsCorr, jetsCorr, tausCorr));

//============================================
//===========Cutflow & Selection =============
//============================================

    for( auto mu : *muonsCorr ) {
        h_CutFlow_muon->Fill(4);

        if( (mu)->pt()*GeV<65.0)continue;
        h_CutFlow_muon->Fill(5);

        if( mu->muonType() != xAOD::Muon::Combined ) continue;
        h_CutFlow_muon->Fill(6);

        if( !(m_muonSelectionHighPt->accept(*mu)))continue;
        h_CutFlow_muon->Fill(7);

        const xAOD::TrackParticle* trk = mu->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle);
        d0cal = 0;
        z0cal = 0;
        if ( trk ) d0z0CAL(trk);
        h_CutFlow_muon->Fill(8);

        muonIdentificationDecor(mu);

        muonTriggerDecor(mu,vw_triggers_muLFV,(sizeof(vw_triggers_muLFV))/(sizeof(string)));


        if( DoMuIso)    muonIsolationDecor(mu);
        Mu_isTrigMch.push_back((mu)->auxdata<char>("triggerMatched"));
        Mu_charge.push_back( mu->charge() );
        Mu_pt.push_back( mu->pt()*GeV );
        Mu_eta.push_back( mu->eta() );
        Mu_phi.push_back( mu->phi() );
        Mu_d0sig.push_back( d0cal );
        Mu_d0pass.push_back((fabs(d0cal)<3.0));
        Mu_z0pass.push_back(z0cal);
        Mu_isHighPt.push_back( m_muonSelectionHighPt->accept(*mu) );
        Mu_isOverlap.push_back( mu->auxdata<char>("isOverlap") );
        Mu_Iso.push_back((mu)->auxdata<char>("IsoPFLooseMu"));

        if(isMC) {
            Mu_TrigSF.push_back(GetMuonTriggerSF(mu));
            Mu_RecoSF.push_back((mu)->auxdata<double>("RecoWeightHighpt"));
            Mu_IsoSF.push_back((mu)->auxdata<double>("IsoWeightHighpt"));
            Mu_TrackSF.push_back((mu)->auxdata<double>("TTVAWeightHighpt"));
        } else {
            Mu_TrigSF.push_back( 1.0 );
            Mu_RecoSF.push_back( 1.0 );
            Mu_IsoSF.push_back( 1.0 );
            Mu_TrackSF.push_back( 1.0 );
        }



        if( fabs(d0cal ) > 3.0) continue;
        h_CutFlow_muon->Fill(9);

        if( !z0cal ) continue;
        h_CutFlow_muon->Fill(10);

        if( !mu->auxdata<char>("isPreSel") ) continue;
        h_CutFlow_muon->Fill(11);

        if( mu->auxdata<char>("isOverlap") ) continue;
        h_CutFlow_muon->Fill(12);

        if( m_muonSelectionHighPt->isBadMuon(*mu))  BadMuonEvent=true;

        if( (mu)->pt()*GeV > LooseMuon.Pt() ) {
            LooseMuon.SetPtEtaPhiM( (mu)->pt() * GeV, (mu)->eta(), (mu)->phi(), MuonPDGMass );
        }

        if( !(mu)->auxdata<char>("IsoPFLooseMu") ) {
            NumberGoodLooseMuons++;
        }

        if( !(mu)->auxdata<char>("IsoPFLooseMu") ) {
            h_CutFlow_muon->Fill(14);
            continue;
        }
        h_CutFlow_muon->Fill(13);

        NumberGoodMuons++;
        NumberGoodLooseMuons++;

        if( (mu)->pt()*GeV > Muon.Pt() ) {
            Muon.SetPtEtaPhiM( (mu)->pt() * GeV, (mu)->eta(), (mu)->phi(), MuonPDGMass );
        }

        MuonTriggerMatched = (mu)->auxdata<char>("triggerMatched");
        Mu_TriggerMatched = (mu)->auxdata<char>("triggerMatched");

        if( isMC ) {
            Muon_Iso_SF = (mu)->auxdata<double>("IsoWeightHighpt");
            Muon_Track_SF = (mu)->auxdata<double>("TTVAWeightHighpt");
            Muon_Reco_SF = (mu)->auxdata<double>("RecoWeightHighpt");
//	    cout<<Muon_Iso_SF<<endl;
            if(MuonTriggerMatched) {
                Muon_Trig_Eff_final = GetMuonTriggerSF(mu);
                EventWeight*=(Muon_Iso_SF*Muon_Track_SF*Muon_Reco_SF);
            }
        }



    }



    for( auto el : *elsCorr ) {

        if (el->caloCluster() == nullptr || el->trackParticle() == nullptr) continue;
        bool isElectron_IDMT = false;
        h_CutFlow_el->Fill(4);

        if (runNumber <= 284484 && runNumber > 0 &&isMC ) {
            Electron_L1Calo_Eff =GetElectronL1CaloSF(el);
        }

        double el_author = ((el)->author());
        if ( !(el_author == 1 || el_author == 16) ) continue;
        h_CutFlow_el->Fill(5);

        if ( !(((el)->pt())*GeV > 65.0) ) continue;
        double el_pt = ((el)->pt())*GeV;
        h_CutFlow_el->Fill(6);

        double el_eta = (el)->caloCluster()->etaBE(2);
        if (  fabs(el_eta) > 2.47 || ( fabs(el_eta) > 1.37 && fabs(el_eta) < 1.52)  ) continue;
        h_CutFlow_el->Fill(7);

        bool OQ = (el)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON);
        if ( !OQ ) continue;
        h_CutFlow_el->Fill(8);


        electronIdentificationDecor(el);
//	if((el)->auxdata<char>("LHMedium")) cout<<"AAAA"<<endl;
//	if((el)->auxdata<char>("DFCommonElectronsLHMedium")) cout<<"BBB"<<endl;
//        cout<<(el)->auxdataConst<char>("DFCommonElectronsLHMedium")<<endl;

        if (!(!((el)->auxdata<char>("LHMedium")) && !((el)->auxdata<char>("LHTight")) )) {
            h_CutFlow_el->Fill(9);
        }
        if (!((el)->auxdata<char>("LHMedium")) && !((el)->auxdata<char>("LHTight")) && !((el)->auxdata<char>("LHLoose"))) continue;

        isElectron_IDMT = ( ((el)->auxdata<char>("LHMedium")) || ((el)->auxdata<char>("LHTight")) );

        const xAOD::TrackParticle* trk = (el)->trackParticle();
        d0cal = 0;
        z0cal = 0;
        if (trk) d0z0CAL(trk);

        if (fabs(d0cal) > 5.0) continue;
        if (isElectron_IDMT)  h_CutFlow_el->Fill(10);

        if (!z0cal) continue;
        if (isElectron_IDMT) {
            h_CutFlow_el->Fill(11);
        }

        if( el->auxdata<char>("isOverlap") ) continue;
        if (isElectron_IDMT)  h_CutFlow_el->Fill(12);

        Ele_d0sig.push_back( d0cal );
        Ele_z0pass.push_back( z0cal );
        Ele_eta.push_back( el->eta() );
        Ele_OQ.push_back( (el)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) );
        Ele_Charge.push_back( el->charge() );
        Ele_pt.push_back( ((el)->pt())*GeV );
        Ele_phi.push_back( el->phi() );
        Ele_eta_calo.push_back( el_eta );
        Ele_d0pass.push_back( fabs(d0cal) < 5 );
        Ele_isOverlap.push_back( el->auxdata<char>("isOverlap") );
        Ele_ID_Loose.push_back( (el)->auxdata<char>("LHLoose") );
        Ele_ID_Medium.push_back( (el)->auxdata<char>("LHMedium") );
        Ele_ID_Tight.push_back( (el)->auxdata<char>("LHTight") );

        if(isElectron_IDMT) {
            if (runNumber <= 284484 && runNumber > 0) {
                electronTriggerDecor(el,vw_triggers_el15LFV,(sizeof(vw_triggers_el15LFV))/(sizeof(string)));
            }
            if (runNumber > 284484 ) {
                electronTriggerDecor(el,vw_triggers_elLFV,(sizeof(vw_triggers_elLFV))/(sizeof(string)));
            }
            ElectronTriggerMatched = (el)->auxdata<char>("triggerMatched");
            Ele_isTrigMch.push_back((el)->auxdata<char>("triggerMatched"));
        }

        if (DoEleIso)    electronIsolationDecor(el);
        if ( (el)->auxdata<char>("LHTight") && (el)->auxdata<char>("IsoFCTightEle")) {
            Ele_isTight.push_back( true );
        } else {
            Ele_isTight.push_back( false );
        }

        if ( (el)->auxdata<char>("LHMedium") ) {
            Ele_isLoose.push_back( true );
        } else {
            Ele_isLoose.push_back( false );
        }

        if ( (el)->auxdata<char>("LHLoose") ) {
            Ele_isLoose_IDLoose.push_back( true );
        } else {
            Ele_isLoose_IDLoose.push_back( false );
        }

        if (!m_isoCorrTool->applyCorrection(*el)) continue;

        Ele_Iso.push_back((el)->auxdata<char>("IsoFCTightEle"));
        Ele_Iso_Loose.push_back((el)->auxdata<char>("IsoFCLooseEle"));

        if ( isMC ) {
            if ( (el)->auxdata<char>("LHMedium") ) {
                Electron_Iso_SF = (el)->auxdata<double>("IsoWeightMedium");
                Electron_Reco_SF = (el)->auxdata<double>("RecoWeight");
                Electron_ID_SF = (el)->auxdata<double>("IDWeightMedium");
                if( ElectronTriggerMatched ) Electron_Trig_Eff = (el)->auxdata<double>("TrigEffWeightMedium");

            }

            if ( ( (el)->auxdata<char>("IsoFCTightEle") ) && ( (el)->auxdata<char>("LHTight") ) ) {
                Electron_Iso_SF = (el)->auxdata<double>("IsoWeightTight");
                Electron_Reco_SF = (el)->auxdata<double>("RecoWeight");
                Electron_ID_SF = (el)->auxdata<double>("IDWeightTight");
                if( ElectronTriggerMatched ) Electron_Trig_Eff = (el)->auxdata<double>("TrigEffWeightTight");
            }
            Ele_IsoSF.push_back( Electron_Iso_SF );
            Ele_RecoSF.push_back( Electron_Reco_SF );
            Ele_IDSF.push_back( Electron_ID_SF );
            Ele_TrigEff.push_back( Electron_Trig_Eff );
            Ele_L1CaloSF.push_back( Electron_L1Calo_Eff );
        } else {
            Ele_TrigEff.push_back( 1.0 );
            Ele_RecoSF.push_back( 1.0 );
            Ele_IDSF.push_back( 1.0 );
            Ele_IsoSF.push_back( 1.0 );
            Ele_L1CaloSF.push_back( 1.0 );
        }

        if ( (el)->auxdata<char>("IsoFCTightEle") )
            if (isElectron_IDMT)  h_CutFlow_el->Fill(13);

        if ( (el)->auxdata<char>("IsoFCTightEle") && (el)->auxdata<char>("LHTight") ) {
            NumberGoodLooseIDLooseElectrons++;
            NumberGoodLooseElectrons++;
            NumberGoodElectrons++;
            h_CutFlow_el->Fill(14);
            if (el_pt > LooseElectron.Pt()) {
                LooseElectron.SetPtEtaPhiM( (el)->pt() * GeV, (el)->eta(), (el)->phi(), ElectronPDGMass );
            }

            if (el_pt > Electron.Pt()) {
                Electron.SetPtEtaPhiM( (el)->pt() * GeV, (el)->eta(), (el)->phi(), ElectronPDGMass );
            }
        } else if ( (el)->auxdata<char>("LHMedium") ) {
            NumberGoodLooseIDLooseElectrons++;
            NumberGoodLooseElectrons++;
            h_CutFlow_el->Fill(15);
            if (el_pt > LooseElectron.Pt()) {
                LooseElectron.SetPtEtaPhiM( (el)->pt() * GeV, (el)->eta(), (el)->phi(), ElectronPDGMass );
            }
        } else if ( (el)->auxdata<char>("LHLoose") ) {
            NumberGoodLooseIDLooseElectrons++;
            if (el_pt > LooseElectron.Pt()) {
                LooseElectron.SetPtEtaPhiM( (el)->pt() * GeV, (el)->eta(), (el)->phi(), ElectronPDGMass );
            }
        }
        if( ElectronTriggerMatched ) Ele_TriggerMatched =  ElectronTriggerMatched ;

        if( isMC ) {
            if ( (el)->auxdata<char>("IsoFCTightEle") && (el)->auxdata<char>("LHTight") )
                Electron_Iso_SF = (el)->auxdata<double>("IsoWeightTight");
            Electron_Reco_SF = (el)->auxdata<double>("RecoWeight");
            Electron_ID_SF = (el)->auxdata<double>("IDWeightTight");
            if( ElectronTriggerMatched ) {
                Electron_Trig_Eff_final = (el)->auxdata<double>("TrigEffWeightTight");
                EventWeight*=(Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_L1Calo_Eff);
            }
        }


    }

    for( auto tau : *tausCorr ) {
        h_CutFlow_tau->Fill(4);

        if ( !(((tau)->pt())*GeV > 65.0) ) continue;
        h_CutFlow_tau->Fill(5);

        double tau_eta = (tau)->eta();
        if (  fabs(tau_eta) > 2.5 || ( fabs(tau_eta) > 1.37 && fabs(tau_eta) < 1.52)  ) continue;
        h_CutFlow_tau->Fill(6);

        if (fabs(tau->charge())!=1) continue;
        h_CutFlow_tau->Fill(7);

        if (!(tau->nTracks() == 1 ||tau->nTracks() ==3)) continue;
        h_CutFlow_tau->Fill(8);


        Tau_pt.push_back( tau->pt()*GeV );
        Tau_phi.push_back( tau->phi() );
        Tau_eta.push_back( tau->eta() );
        TauSelector.push_back( TauSelTool_NoOLR->accept( *tau ) );
        TauSelector_WithOLR.push_back( TauSelTool->accept( *tau ) );
        Tau_ntracks.push_back( tau->nTracks() );
        Tau_charge.push_back( tau->charge() );
        Tau_isOverlap.push_back(( tau->auxdata<char>("isOverlap") ));
//    if (!(tau->isTau(xAOD::TauJetParameters::JetBDTSigLoose)) ) continue;
        h_CutFlow_tau->Fill(9);

        if( !TauSelTool_NoOLR->accept( *tau ) ) continue;
        h_CutFlow_tau->Fill(10);

        if( !TauSelTool->accept( *tau ) ) continue;
        h_CutFlow_tau->Fill(11);

        if(!( tau->auxdata<char>("isPreSel") ) )continue;
        h_CutFlow_tau->Fill(12);

        if(( tau->auxdata<char>("isOverlap") ) )continue;
        h_CutFlow_tau->Fill(13);

        NumberGoodTaus++;

        if( isMC ) {
            m_Tau_Reco_SF->getEfficiencyScaleFactor(*tau, TauRecoSF);
            m_Tau_ElOlr_SF->getEfficiencyScaleFactor(*tau, TauEleOLRSF);
            m_Tau_JetID_SF->getEfficiencyScaleFactor(*tau, TauIDSF);
            EventWeight*=(TauRecoSF*TauIDSF*TauEleOLRSF);
            Tau_RecoSF.push_back( TauRecoSF );
            Tau_IDSF.push_back( TauIDSF );
            Tau_EleOLRSF.push_back( TauEleOLRSF );
        }



        if( ((tau)->pt())*GeV  > Tau.Pt()) {
            Tau.SetPtEtaPhiM( ((tau)->pt())*GeV, (tau)->eta(), tau->phi(), TauPDGMass);
        }

    }

    for( auto jet: *jetsCorr) {
        bool passJvt = m_jvtsf->passesJvtCut(*jet);
        float DetectorEta = 0.0;
        (jet)->getAttribute("DetectorEta",DetectorEta);
        h_CutFlow_jet->Fill(4);

        if( (jet)->pt()*0.001 < 25.0 ) continue;
        h_CutFlow_jet->Fill(5);

        if( fabs( (jet)->eta() ) > 2.5 ) continue;
        h_CutFlow_jet->Fill(6);

        if(!jet->auxdecor<char>("isPreSel") ) continue;
        h_CutFlow_jet->Fill(7);

        if(jet->auxdata<char>("isOverlap") ) continue;
        h_CutFlow_jet->Fill(8);

        if(!passJvt ) continue;
//        if( (jet)->pt()*GeV < 60.0 && fabs( DetectorEta ) <  2.4 && passJvt && (!m_cleaningTool->keep( *jet )) ) BadJetEvent=true;
//        if( (jet)->pt()*GeV < 60.0 && fabs( DetectorEta ) >= 2.4 && (!m_cleaningTool->keep( *jet )) ) BadJetEvent=true;
//        if( (jet)->pt()*GeV >= 60.0 && (!m_cleaningTool->keep( *jet )) ) BadJetEvent=true;
//
//        if( (jet)->pt()*GeV < 60.0 && fabs( DetectorEta ) < 2.4 && !passJvt ) continue;
        h_CutFlow_jet->Fill(9);

//        if( !m_cleaningTool->keep( *jet ) ) continue;
        h_CutFlow_jet->Fill(10);
        N_jets++;
        Jet_pt.push_back( (jet)->pt()*GeV );
        Jet_eta.push_back( (jet)->eta() );
        Jet_phi.push_back( (jet)->phi() );
        Jet_m.push_back( (jet)->m() );
        Jet_isGoodB.push_back( btagtool->accept( *jet) );

        if( ((jet)->pt())*GeV  > Jet.Pt()) {
            Jet.SetPtEtaPhiM( ((jet)->pt())*GeV, (jet)->eta(), jet->phi(), jet->m() );
        }

        if(isMC) {
            m_jvtsf->getEfficiencyScaleFactor(*jet,JVTSF);
            JetSF*= JVTSF;
            Jet_JVTSF.push_back((double)JVTSF);
        } else {
            Jet_JVTSF.push_back(1.0);
        }

        if( !btagtool->accept(*jet)) {
            if(isMC) {
                btagefftool->getInefficiencyScaleFactor(*jet,BTaggingSF);
                JetSF*=BTaggingSF;
                inBTaggingSFtot*=BTaggingSF;
                Jet_BTInefSF.push_back((double)BTaggingSF);
            } else {
                Jet_BTInefSF.push_back(1.0);
            }
            h_CutFlow_jet->Fill(11);
            continue;
        } else {
            Jet_BTInefSF.push_back(1.0);
        }

        if(isMC) {
            btagefftool->getScaleFactor(*jet,BTaggingSF);
            JetSF*=BTaggingSF;
            BTaggingSFtot*=BTaggingSF;
            Jet_BTSF.push_back((double)BTaggingSF);
        } else {
            Jet_BTSF.push_back(1.0);
        }

        h_CutFlow_jet->Fill(12);
        isGoodBJet =true;

    }
    EventWeight*=JetSF;
    if(doMetCalculation) {
        std::string coreMetKey = "MET_Core_" + jetType;
        std::string metAssocKey = "METAssoc_" + jetType;

        if( !xAOD::setOriginalObjectLink( *muons, *muonsCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original muon object links!" );
        if( !m_store->record(muonsCorr, "MCalibMuons").isSuccess() ) Info( "StoreObjects()", "ERROR::Failed to store CalibMuons in Event Store" );

        if( !xAOD::setOriginalObjectLink( *electrons, *elsCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original electron object links!" );
        if( !m_store->record(elsCorr, "MCalibElectrons").isSuccess() ) Info( "StoreObjects()", "ERROR::Failed to store CalibElectrons in Event Store" );

        if( !xAOD::setOriginalObjectLink( *taus, *tausCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original tau object links!" );
        if( !m_store->record(tausCorr, "MCalibTaus").isSuccess() ) Info( "StoreObjects()", "ERROR::Failed to store CalibTaus in Event Store" );

        //if( !xAOD::setOriginalObjectLink( *jets, *m_jetContCopy ) ) Info( "BuildMET()", "ERROR::Failed to set the original jet object links!" );
        if( !xAOD::setOriginalObjectLink( *jets, *jetsCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original jet object links!" );
        if( !m_store->record(jetsCorr, "MCalibJets").isSuccess() ) Info( "StoreObjects()", "ERROR::Failed to store CalibJets in Event Store" );

//        if( !xAOD::setOriginalObjectLink( *photons, *phsCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original jet object links!" );
//        if( !m_store->record(phsCorr, "MCalibPhotons").isSuccess() ) Info( "StoreObjects()", "ERROR::Failed to store CalibPhotons in Event Store" );



        coreMetKey.erase(coreMetKey.length() - 4);//this removes the string "Jets" from the end of the jetType
        metAssocKey.erase(metAssocKey.length() - 4 );
        //retrieve the MET core
        const xAOD::MissingETContainer* coreMet  = nullptr;
        evtStore()->retrieve(coreMet, "MET_Core_AntiKt4EMPFlow");

        //retrieve the MET association map
        const xAOD::MissingETAssociationMap* metMap = nullptr;
        evtStore()->retrieve(metMap, "METAssoc_AntiKt4EMPFlow");

        xAOD::MissingETContainer*    newMetContainer    = new xAOD::MissingETContainer();
        xAOD::MissingETAuxContainer* newMetAuxContainer = new xAOD::MissingETAuxContainer();
        newMetContainer->setStore(newMetAuxContainer);
        metMap->resetObjSelectionFlags();


        metMaker->rebuildMET( "RefEle", xAOD::Type::Electron, newMetContainer, m_MetCorrElectrons,metMap );
//        if( !metMaker->rebuildMET( "RefPhoton", xAOD::Type::Photon, newMetContainer,m_MetCorrPhotons,metMap ).isSuccess() ) {
//            Info( "FeedMetMaker()", "Error in feeding MetMaker with corrected photons" );
//        }

        metMaker->rebuildMET( "RefTau", xAOD::Type::Tau, newMetContainer, m_MetCorrTaus,metMap );
        metMaker->rebuildMET( "RefMuon", xAOD::Type::Muon, newMetContainer,m_MetCorrMuons,metMap );
        metMaker->rebuildJetMET( "RefJet", "SoftClus", "PVSoftTrk", newMetContainer,jetsCorr, coreMet,metMap, true );

        (metMaker)->buildMETSum("FinalTrk", newMetContainer, MissingETBase::Source::Track );
        xAOD::MissingET* metFinal = 0;
        if(!(metFinal=(*newMetContainer)["FinalTrk"])) std::cout<<"met retrieve failed"<<std::endl;
        Neutrino.SetPtEtaPhiM( metFinal->met()*GeV, Tau.Eta(), metFinal->phi(), 0.0 );
        Neutrino2.SetPxPyPzE( metFinal->mpx()*GeV, metFinal->mpy()*GeV, 0, sqrt((metFinal->mpx()*GeV)*(metFinal->mpx()*GeV)+(metFinal->mpy()*GeV)*(metFinal->mpy()*GeV)) );
        MET_Et = metFinal->met()*GeV;
        MET_Phi = metFinal->phi();
        MET_Px = metFinal->mpx()*GeV;
        MET_Py = metFinal->mpy()*GeV;
        MET_SumEt = metFinal->sumet()*GeV;
        event_met = Neutrino2.Pt();
        event_met_phi = Neutrino2.Phi();
        event_met_eta = Neutrino2.Eta();
        event_met_m = Neutrino2.M();


        newMetContainer->clear();
        m_store->clear();
        delete newMetContainer;
        delete newMetAuxContainer;

    }

    if(eventInfo->auxdecor<bool> ("EventonCrack")) return StatusCode::SUCCESS;

    if( BadMuonEvent ) {
        return StatusCode::SUCCESS;
    }
    h_CutFlow_emu->Fill(4);
    h_CutFlow_etau->Fill(4);
    h_CutFlow_mutau->Fill(4);

    if( BadJetEvent ) {
        return StatusCode::SUCCESS;
    }
    h_CutFlow_emu->Fill(5);
    h_CutFlow_etau->Fill(5);
    h_CutFlow_mutau->Fill(5);

    if((!( NumberGoodTaus==0 && NumberGoodLooseMuons==1 && NumberGoodLooseElectrons==1)) && (!(NumberGoodMuons==0 && NumberGoodElectrons==1 && NumberGoodTaus==1)) && (!(NumberGoodMuons==1 && NumberGoodElectrons==0 && NumberGoodTaus==1)) && (!(NumberGoodLooseElectrons==1) && !(NumberGoodLooseIDLooseElectrons==1))) {
        return StatusCode::SUCCESS;
    }

    if( NumberGoodTaus==0 && NumberGoodLooseMuons==1 && NumberGoodLooseElectrons==1 ) {
        Propagator = Electron+Muon;
        m_electronMuon = true;
        EventTag=0;
        dPhi_ej = fabs(LooseElectron.Phi() - Jet.Phi());
        event_mT = sqrt((sqrt(LooseElectron.E()*LooseElectron.E()-LooseElectron.Pz()*LooseElectron.Pz())+Neutrino2.E())*(sqrt(LooseElectron.E()*LooseElectron.E()-LooseElectron.Pz()*LooseElectron.Pz())+Neutrino2.E())-(LooseElectron.Px()+Neutrino2.Px())*(LooseElectron.Px()+Neutrino2.Px())-(LooseElectron.Py()+Neutrino2.Py())*(LooseElectron.Py()+Neutrino2.Py()));
        Final_elpt = LooseElectron.Pt();
        Final_eleta = LooseElectron.Eta();
        Final_elphi = LooseElectron.Phi();
        Final_mupt =  LooseMuon.Pt();
        Final_mueta = LooseMuon.Eta();
        Final_muphi = LooseMuon.Phi();
    } else if( NumberGoodMuons==0 && NumberGoodElectrons==1 && NumberGoodTaus==1 ) {
        Propagator = Electron+Tau+Neutrino;
        m_electronTau = true;
        EventTag=1;
        dPhi_ej = fabs(LooseElectron.Phi() - Jet.Phi());
        event_mT = sqrt((sqrt(LooseElectron.E()*LooseElectron.E()-LooseElectron.Pz()*LooseElectron.Pz())+Neutrino2.E())*(sqrt(LooseElectron.E()*LooseElectron.E()-LooseElectron.Pz()*LooseElectron.Pz())+Neutrino2.E())-(LooseElectron.Px()+Neutrino2.Px())*(LooseElectron.Px()+Neutrino2.Px())-(LooseElectron.Py()+Neutrino2.Py())*(LooseElectron.Py()+Neutrino2.Py()));
        Final_elpt =  Electron.Pt();
        Final_eleta = Electron.Eta();
        Final_elphi = Electron.Phi();
        Final_taupt = Tau.Pt();
        Final_taueta = Tau.Eta();
        Final_tauphi = Tau.Phi();

    } else if( NumberGoodMuons==1 && NumberGoodElectrons==0 && NumberGoodTaus==1 ) {
        Propagator = Muon+Tau+Neutrino;
        m_muonTau = true;
        EventTag=2;
        dPhi_ej = fabs(LooseElectron.Phi() - Jet.Phi());
        event_mT = sqrt((sqrt(LooseElectron.E()*LooseElectron.E()-LooseElectron.Pz()*LooseElectron.Pz())+Neutrino2.E())*(sqrt(LooseElectron.E()*LooseElectron.E()-LooseElectron.Pz()*LooseElectron.Pz())+Neutrino2.E())-(LooseElectron.Px()+Neutrino2.Px())*(LooseElectron.Px()+Neutrino2.Px())-(LooseElectron.Py()+Neutrino2.Py())*(LooseElectron.Py()+Neutrino2.Py()));
        Final_taupt = Tau.Pt();
        Final_taueta = Tau.Eta();
        Final_tauphi = Tau.Phi();
        Final_mupt =  Muon.Pt();
        Final_mueta = Muon.Eta();
        Final_muphi = Muon.Phi();

    } else if( NumberGoodLooseElectrons==1 || NumberGoodLooseIDLooseElectrons==1) {
        EventTag=3;
        dPhi_ej = fabs(LooseElectron.Phi() - Jet.Phi());
        event_mT = sqrt((sqrt(LooseElectron.E()*LooseElectron.E()-LooseElectron.Pz()*LooseElectron.Pz())+Neutrino2.E())*(sqrt(LooseElectron.E()*LooseElectron.E()-LooseElectron.Pz()*LooseElectron.Pz())+Neutrino2.E())-(LooseElectron.Px()+Neutrino2.Px())*(LooseElectron.Px()+Neutrino2.Px())-(LooseElectron.Py()+Neutrino2.Py())*(LooseElectron.Py()+Neutrino2.Py()));
        Final_elpt = LooseElectron.Pt();
        Final_eleta = LooseElectron.Eta();
        Final_elphi = LooseElectron.Phi();
    }

    if( !(ElectronTriggerMatched == false && MuonTriggerMatched == false) ) {
        Event_Trigger_Match=true;
    }

    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(6);
    if(m_electronTau)h_CutFlow_etau->Fill(6);
    if(m_muonTau)h_CutFlow_mutau->Fill(6);

    if( ElectronTriggerMatched == false && MuonTriggerMatched == false ) {
        return StatusCode::SUCCESS;   //Test Trigger
    }
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(7);
    if(m_electronTau)h_CutFlow_etau->Fill(7);
    if(m_muonTau)h_CutFlow_mutau->Fill(7);

    if( isMC ) {
        EventWeight_NoTrigSF	  = EventWeight;
        if( ElectronTriggerMatched ) EventWeight*=Electron_Trig_Eff_final;
        if( MuonTriggerMatched ) EventWeight*=Muon_Trig_Eff_final;
    }


    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(8);
    if(m_electronTau)h_CutFlow_etau->Fill(8);
    if(m_muonTau)h_CutFlow_mutau->Fill(8);

    Dphi = 0.0;
    if( m_electronMuon )Dphi = Electron.Phi() - Muon.Phi();
    else if( m_electronTau ) Dphi = Electron.Phi() - Tau.Phi();
    else if( m_muonTau ) Dphi = Muon.Phi() - Tau.Phi();
    if( fabs(Dphi)>pi ) {
        if( Dphi>0 ) {
            Dphi = 2*pi-Dphi;
        } else {
            Dphi = 2*pi+Dphi;
        }
    }
    dPhi_ll = Dphi;

    lfvTree->Fill();

    if( fabs(Dphi) < 2.7 ) return StatusCode::SUCCESS;
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(9);
    if(m_electronTau)h_CutFlow_etau->Fill(9);
    if(m_muonTau)h_CutFlow_mutau->Fill(9);

    if(isGoodBJet == true) {
        return StatusCode::SUCCESS;
    }
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(10);
    if(m_electronTau)h_CutFlow_etau->Fill(10);
    if(m_muonTau)h_CutFlow_mutau->Fill(10);

    if( MET_Et > 30.0) return StatusCode::SUCCESS;
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(11);
    if(m_electronTau)h_CutFlow_etau->Fill(11);
    if(m_muonTau)h_CutFlow_mutau->Fill(11);

    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(12.0,EventWeight);
    if(m_electronTau)h_CutFlow_etau->Fill(12.0,EventWeight);
    if(m_muonTau)h_CutFlow_mutau->Fill(12.0,EventWeight);

    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(13.0,PRWwei);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(14.0,PRWwei*Electron_Reco_SF);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(15.0,PRWwei*Electron_Reco_SF*Electron_ID_SF);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(16.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(17.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(18.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff*Electron_L1Calo_Eff);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(19.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff*Electron_L1Calo_Eff*Muon_Reco_SF);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(20.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff*Electron_L1Calo_Eff*Muon_Reco_SF*Muon_Iso_SF);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(21.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff*Electron_L1Calo_Eff*Muon_Reco_SF*Muon_Trig_Eff_final*Muon_Iso_SF);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(22.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff*Electron_L1Calo_Eff*Muon_Reco_SF*Muon_Trig_Eff_final*Muon_Iso_SF*Muon_Track_SF);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(23.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff*Electron_L1Calo_Eff*Muon_Reco_SF*Muon_Trig_Eff_final*Muon_Iso_SF*Muon_Track_SF*MCWeight);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(24.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff*Electron_L1Calo_Eff*Muon_Reco_SF*Muon_Trig_Eff_final*Muon_Iso_SF*Muon_Track_SF*MCWeight*JVTSF);
    if(m_electronMuon && NumberGoodMuons==1 && NumberGoodElectrons==1)h_CutFlow_emu->Fill(25.0,PrwWeight*Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*Electron_Trig_Eff*Electron_L1Calo_Eff*Muon_Reco_SF*Muon_Trig_Eff_final*Muon_Iso_SF*Muon_Track_SF*MCWeight*JVTSF*BTaggingSFtot);

//  delete els_shallowCopy.first;
//  delete mus_shallowCopy.first;
//  delete taus_shallowCopy.first;
//  delete phs_shallowCopy.first;
//  delete jets_shallowCopy.first;
//
//  delete els_shallowCopy.second;
//  delete mus_shallowCopy.second;
//  delete taus_shallowCopy.second;
//  delete phs_shallowCopy.second;
//  delete jets_shallowCopy.second;


    return true;
}






bool LFVAnalysis :: finish() {

    delete m_MetCorrMuons    ;
    delete m_MetCorrElectrons;
    delete m_MetCorrTaus     ;
    delete m_MetCorrPhotons  ;

    for(int it=0; it<(int)vec_filename->size(); it++) {
        sumOfWeights = vec_sumOfWeights->at(it);
        filename = vec_filename->at(it);
        nEventsProcessed = vec_sumOfEvents->at(it);
//    std::cout<<"bookkeeping "<<filename<<", "<<sumOfWeights<<std::endl;

        metaTree->Fill();



    }


//   ifstream inf;
//   inf.open("/afs/cern.ch/work/j/jugao/private/LFV/source/MyAnalysis/Root/in.txt");
//  char c;
//  inf >> noskipws;
//  while(inf >>c)
//    {
//	     cout << c;
//        if (c == '\n'){            //
////            cout << "\n";        //
//        }
//    }
//      inf.close();


    std::cout<<"EM Event Cut Flow:"<<std::endl;
    std::cout<<"All events       :"<<h_CutFlow_emu->GetBinContent(1)<<std::endl;
    std::cout<<"Event Cleaning   :"<<h_CutFlow_emu->GetBinContent(2)<<std::endl;
    std::cout<<"Vertex           :"<<h_CutFlow_emu->GetBinContent(3)<<std::endl;
    std::cout<<"Trigger          :"<<h_CutFlow_emu->GetBinContent(4)<<std::endl;
    std::cout<<"BadMuonEvent     :"<<h_CutFlow_emu->GetBinContent(5)<<std::endl;
    std::cout<<"BadJetEvent      :"<<h_CutFlow_emu->GetBinContent(6)<<std::endl;
    std::cout<<"Leptons          :"<<h_CutFlow_emu->GetBinContent(7)<<std::endl;
    std::cout<<"Trigger Match    :"<<h_CutFlow_emu->GetBinContent(8)<<std::endl;
    std::cout<<"OS pair          :"<<h_CutFlow_emu->GetBinContent(9)<<std::endl;
    std::cout<<"DeltaPhi         :"<<h_CutFlow_emu->GetBinContent(10)<<std::endl;
    std::cout<<"b-jet veto       :"<<h_CutFlow_emu->GetBinContent(11)<<std::endl;
    std::cout<<"MET              :"<<h_CutFlow_emu->GetBinContent(12)<<std::endl;
    std::cout<<"MET WT           :"<<h_CutFlow_emu->GetBinContent(13)<<std::endl;
    std::cout<<"WT 1          :"<<h_CutFlow_emu->GetBinContent(14)<<std::endl;
    std::cout<<"WT 2          :"<<h_CutFlow_emu->GetBinContent(15)<<std::endl;
    std::cout<<"WT 3          :"<<h_CutFlow_emu->GetBinContent(16)<<std::endl;
    std::cout<<"WT 4          :"<<h_CutFlow_emu->GetBinContent(17)<<std::endl;
    std::cout<<"WT 5          :"<<h_CutFlow_emu->GetBinContent(18)<<std::endl;
    std::cout<<"WT 6          :"<<h_CutFlow_emu->GetBinContent(19)<<std::endl;
    std::cout<<"WT 7          :"<<h_CutFlow_emu->GetBinContent(20)<<std::endl;
    std::cout<<"WT 8          :"<<h_CutFlow_emu->GetBinContent(21)<<std::endl;
    std::cout<<"WT 9          :"<<h_CutFlow_emu->GetBinContent(22)<<std::endl;
    std::cout<<"WT 10          :"<<h_CutFlow_emu->GetBinContent(23)<<std::endl;
    std::cout<<"WT 11          :"<<h_CutFlow_emu->GetBinContent(24)<<std::endl;
    std::cout<<"WT 12          :"<<h_CutFlow_emu->GetBinContent(25)<<std::endl;
    std::cout<<"WT 13          :"<<h_CutFlow_emu->GetBinContent(26)<<std::endl;



    std::cout<<"===================================================="<<std::endl;

    std::cout<<"ET Event Cut Flow:"<<std::endl;
    std::cout<<"All events       :"<<h_CutFlow_etau->GetBinContent(1)<<std::endl;
    std::cout<<"Event Cleaning   :"<<h_CutFlow_etau->GetBinContent(2)<<std::endl;
    std::cout<<"Vertex           :"<<h_CutFlow_etau->GetBinContent(3)<<std::endl;
    std::cout<<"Trigger          :"<<h_CutFlow_etau->GetBinContent(4)<<std::endl;
    std::cout<<"BadMuonEvent     :"<<h_CutFlow_etau->GetBinContent(5)<<std::endl;
    std::cout<<"BadJetEvent      :"<<h_CutFlow_etau->GetBinContent(6)<<std::endl;
    std::cout<<"Leptons          :"<<h_CutFlow_etau->GetBinContent(7)<<std::endl;
    std::cout<<"Trigger Match    :"<<h_CutFlow_etau->GetBinContent(8)<<std::endl;
    std::cout<<"OS pair          :"<<h_CutFlow_etau->GetBinContent(9)<<std::endl;
    std::cout<<"DeltaPhi         :"<<h_CutFlow_etau->GetBinContent(10)<<std::endl;
    std::cout<<"b-jet veto       :"<<h_CutFlow_etau->GetBinContent(11)<<std::endl;
    std::cout<<"MET              :"<<h_CutFlow_etau->GetBinContent(12)<<std::endl;
    std::cout<<"MET WT           :"<<h_CutFlow_etau->GetBinContent(13)<<std::endl;

    std::cout<<"===================================================="<<std::endl;

    std::cout<<"MT Event Cut Flow:"<<std::endl;
    std::cout<<"All events       :"<<h_CutFlow_mutau->GetBinContent(1)<<std::endl;
    std::cout<<"Event Cleaning   :"<<h_CutFlow_mutau->GetBinContent(2)<<std::endl;
    std::cout<<"Vertex           :"<<h_CutFlow_mutau->GetBinContent(3)<<std::endl;
    std::cout<<"Trigger          :"<<h_CutFlow_mutau->GetBinContent(4)<<std::endl;
    std::cout<<"BadMuonEvent     :"<<h_CutFlow_mutau->GetBinContent(5)<<std::endl;
    std::cout<<"BadJetEvent      :"<<h_CutFlow_mutau->GetBinContent(6)<<std::endl;
    std::cout<<"Leptons          :"<<h_CutFlow_mutau->GetBinContent(7)<<std::endl;
    std::cout<<"Trigger Match    :"<<h_CutFlow_mutau->GetBinContent(8)<<std::endl;
    std::cout<<"OS pair          :"<<h_CutFlow_mutau->GetBinContent(9)<<std::endl;
    std::cout<<"DeltaPhi         :"<<h_CutFlow_mutau->GetBinContent(10)<<std::endl;
    std::cout<<"b-jet veto       :"<<h_CutFlow_mutau->GetBinContent(11)<<std::endl;
    std::cout<<"MET              :"<<h_CutFlow_mutau->GetBinContent(12)<<std::endl;
    std::cout<<"MET  WT          :"<<h_CutFlow_mutau->GetBinContent(13)<<std::endl;

    std::cout<<"===================================================="<<std::endl;

    std::cout<<"Muon Cut Flow    :"<<std::endl;
    std::cout<<"All events       :"<<h_CutFlow_muon->GetBinContent(1)<<std::endl;
    std::cout<<"Event Cleaning   :"<<h_CutFlow_muon->GetBinContent(2)<<std::endl;
    std::cout<<"Vertex           :"<<h_CutFlow_muon->GetBinContent(3)<<std::endl;
    std::cout<<"Trigger          :"<<h_CutFlow_muon->GetBinContent(4)<<std::endl;
    std::cout<<"Trigger2         :"<<h_CutFlow_muon->GetBinContent(5)<<std::endl;
    std::cout<<"Pt               :"<<h_CutFlow_muon->GetBinContent(6)<<std::endl;
    std::cout<<"Combine          :"<<h_CutFlow_muon->GetBinContent(7)<<std::endl;
    std::cout<<"eta/ID/Quality   :"<<h_CutFlow_muon->GetBinContent(8)<<std::endl;
    std::cout<<"HighPtCuts       :"<<h_CutFlow_muon->GetBinContent(9)<<std::endl;
    std::cout<<"IP sign(d0)      :"<<h_CutFlow_muon->GetBinContent(10)<<std::endl;
    std::cout<<"Delta z0         :"<<h_CutFlow_muon->GetBinContent(11)<<std::endl;
    std::cout<<"isPreSel         :"<<h_CutFlow_muon->GetBinContent(12)<<std::endl;
    std::cout<<"isOverlap        :"<<h_CutFlow_muon->GetBinContent(13)<<std::endl;
//    std::cout<<"Isolation        :"<<h_CutFlow_muon->GetBinContent(14)<<std::endl;
    std::cout<<"Tight Muons      :"<<h_CutFlow_muon->GetBinContent(14)<<std::endl;
    std::cout<<"Loose Muons      :"<<h_CutFlow_muon->GetBinContent(15)<<std::endl;

    std::cout<<"===================================================="<<std::endl;

    std::cout<<"Electron Cut Flow:"<<std::endl;
    std::cout<<"All events       :"<<h_CutFlow_el->GetBinContent(1)<<std::endl;
    std::cout<<"Event Cleaning   :"<<h_CutFlow_el->GetBinContent(2)<<std::endl;
    std::cout<<"Vertex           :"<<h_CutFlow_el->GetBinContent(3)<<std::endl;
    std::cout<<"Trigger          :"<<h_CutFlow_el->GetBinContent(4)<<std::endl;
    std::cout<<"Trigger2         :"<<h_CutFlow_el->GetBinContent(5)<<std::endl;
    std::cout<<"Author           :"<<h_CutFlow_el->GetBinContent(6)<<std::endl;
    std::cout<<"Pt               :"<<h_CutFlow_el->GetBinContent(7)<<std::endl;
    std::cout<<"Eta              :"<<h_CutFlow_el->GetBinContent(8)<<std::endl;
    std::cout<<"Object Quality   :"<<h_CutFlow_el->GetBinContent(9)<<std::endl;
    std::cout<<"Likelihood ID    :"<<h_CutFlow_el->GetBinContent(10)<<std::endl;
    std::cout<<"IP signa(d0)     :"<<h_CutFlow_el->GetBinContent(11)<<std::endl;
    std::cout<<"Delta z0         :"<<h_CutFlow_el->GetBinContent(12)<<std::endl;
    std::cout<<"Overlap          :"<<h_CutFlow_el->GetBinContent(13)<<std::endl;
    std::cout<<"Isolation        :"<<h_CutFlow_el->GetBinContent(14)<<std::endl;
    std::cout<<"Tight Electrons  :"<<h_CutFlow_el->GetBinContent(15)<<std::endl;
    std::cout<<"Loose Electrons  :"<<h_CutFlow_el->GetBinContent(16)<<std::endl;

    std::cout<<"===================================================="<<std::endl;

    std::cout<<"Tau Cut Flow     :"<<std::endl;
    std::cout<<"All events       :"<<h_CutFlow_tau->GetBinContent(1)<<std::endl;
    std::cout<<"Event Cleaning   :"<<h_CutFlow_tau->GetBinContent(2)<<std::endl;
    std::cout<<"Vertex           :"<<h_CutFlow_tau->GetBinContent(3)<<std::endl;
    std::cout<<"Trigger          :"<<h_CutFlow_tau->GetBinContent(4)<<std::endl;
    std::cout<<"Trigger2         :"<<h_CutFlow_tau->GetBinContent(5)<<std::endl;
    std::cout<<"Pt               :"<<h_CutFlow_tau->GetBinContent(6)<<std::endl;
    std::cout<<"Eta              :"<<h_CutFlow_tau->GetBinContent(7)<<std::endl;
    std::cout<<"Charge           :"<<h_CutFlow_tau->GetBinContent(8)<<std::endl;
    std::cout<<"Tracks           :"<<h_CutFlow_tau->GetBinContent(9)<<std::endl;
    std::cout<<"ID               :"<<h_CutFlow_tau->GetBinContent(10)<<std::endl;
    std::cout<<"ID               :"<<h_CutFlow_tau->GetBinContent(11)<<std::endl;
    std::cout<<"EleOLR           :"<<h_CutFlow_tau->GetBinContent(12)<<std::endl;
    std::cout<<"IsPreSel         :"<<h_CutFlow_tau->GetBinContent(13)<<std::endl;
    std::cout<<"Overlap          :"<<h_CutFlow_tau->GetBinContent(14)<<std::endl;

    std::cout<<"===================================================="<<std::endl;

    std::cout<<"Jet Cut Flow:"<<std::endl;
    std::cout<<"All events       :"<<h_CutFlow_jet->GetBinContent(1)<<std::endl;
    std::cout<<"Event Cleaning   :"<<h_CutFlow_jet->GetBinContent(2)<<std::endl;
    std::cout<<"Vertex           :"<<h_CutFlow_jet->GetBinContent(3)<<std::endl;
    std::cout<<"Trigger          :"<<h_CutFlow_jet->GetBinContent(4)<<std::endl;
    std::cout<<"Trigger2         :"<<h_CutFlow_jet->GetBinContent(5)<<std::endl;
    std::cout<<"Pt               :"<<h_CutFlow_jet->GetBinContent(6)<<std::endl;
    std::cout<<"Eta              :"<<h_CutFlow_jet->GetBinContent(7)<<std::endl;
    std::cout<<"Jet presel       :"<<h_CutFlow_jet->GetBinContent(8)<<std::endl;
    std::cout<<"Jet overlap      :"<<h_CutFlow_jet->GetBinContent(9)<<std::endl;
    std::cout<<"JVT              :"<<h_CutFlow_jet->GetBinContent(10)<<std::endl;
    std::cout<<"Clean            :"<<h_CutFlow_jet->GetBinContent(11)<<std::endl;
    std::cout<<"B-jet Veto       :"<<h_CutFlow_jet->GetBinContent(12)<<std::endl;
    std::cout<<"Bjets            :"<<h_CutFlow_jet->GetBinContent(13)<<std::endl;

    std::cout<<"===================================================="<<std::endl;

    std::cout<<"Photon Cut Flow  :"<<std::endl;
    std::cout<<"All events       :"<<h_CutFlow_ph->GetBinContent(1)<<std::endl;
    std::cout<<"Event Cleaning   :"<<h_CutFlow_ph->GetBinContent(2)<<std::endl;
    std::cout<<"Vertex           :"<<h_CutFlow_ph->GetBinContent(3)<<std::endl;
    std::cout<<"Trigger          :"<<h_CutFlow_ph->GetBinContent(4)<<std::endl;
    std::cout<<"Trigger2         :"<<h_CutFlow_ph->GetBinContent(5)<<std::endl;
    std::cout<<"Pt               :"<<h_CutFlow_ph->GetBinContent(6)<<std::endl;
    std::cout<<"Eta              :"<<h_CutFlow_ph->GetBinContent(7)<<std::endl;
    std::cout<<"Author           :"<<h_CutFlow_ph->GetBinContent(8)<<std::endl;
    std::cout<<"Object Quality   :"<<h_CutFlow_ph->GetBinContent(9)<<std::endl;
    std::cout<<"Clean            :"<<h_CutFlow_ph->GetBinContent(10)<<std::endl;


    return true;
}
