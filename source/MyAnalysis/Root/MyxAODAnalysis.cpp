#include <TSystem.h>
#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <TFile.h>
//#include "GaudiKernel/IToolSvc.h"
//#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
//#include <WebBunchCrossingTool.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include <iostream>
//#include <EventLoop/Job.h>
//#include <EventLoop/StatusCode.h>
//#include <EventLoop/Worker.h>
//#include "EventLoop/OutputStream.h"
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODRootAccess/tools/Message.h>
// Infrastructure include(s):
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include <xAODEventInfo/EventInfo.h>
//#include "GoodRunsLists/GoodRunsListSelectionTool.h"
//#include "xAODJet/JetContainer.h"
#include <xAODEgamma/ElectronContainer.h>
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCaloEvent/CaloCluster.h"
//#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
//#include "TrigConfxAOD/xAODConfigTool.h"
//#include "TrigDecisionTool/TrigDecisionTool.h"
//#include "xAODTau/TauJetContainer.h"
//#include "xAODTau/TauDefs.h"
//#include "xAODTau/TauJet.h"
//#include "xAODTruth/TruthEventContainer.h"
//#include "xAODTruth/TruthParticleContainer.h"
//#include "AsgTools/ToolHandle.h"
#include <TFile.h>
#include <TH1.h>
//#include "PileupReweighting/PileupReweightingTool.h"
//#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
//#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
//#include "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h"
//#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
//#include "PathResolver/PathResolver.h"
//#include "IsolationSelection/IsolationSelectionTool.h"
//#include "PATInterfaces/CorrectionCode.h" // to check the return correction code status of tools
//#include "xAODCore/ShallowAuxContainer.h"
#include <xAODCore/ShallowCopy.h>
#include <xAODCore/AuxContainerBase.h>
#include "xAODCore/ShallowAuxContainer.h"
#include <TTree.h>
#include "xAODBase/IParticleHelpers.h"
#include "xAODBase/IParticleContainer.h"

#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/TrackParticle.h"

//=====================Pileup===============
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include "AsgTools/AnaToolHandle.h"

//================isolation=================
#include "IsolationSelection/IsolationSelectionTool.h"

//================Electron tool==============
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"

//===================Trigger=================
#include "TrigConfxAOD/xAODConfigTool.h"
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigDecisionTool/TrigDecisionTool.h"

//===================Efficiency==============
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"

#include "PathResolver/PathResolver.h"

// Jet Information
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/Jet.h"

// met
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETContainer.h"

// Muon Information
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"


#include "JetCalibTools/JetCalibrationTool.h"

#include "METInterface/IMETSystematicsTool.h"
#include "METInterface/IMETMaker.h"
#include "METUtilities/METHelpers.h"
// this is needed to distribute the algorithm to the workers
//ClassImp(MyxAODAnalysis)
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"



MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
		ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
	//    : m_grl ("GoodRunsListSelectionTool/grl", this)
{
	// get the output file, create a new TTree and connect it to that output
	// define what braches will go in that tree

	// Here you put any code for the base initialization of variables,
	// e.g. initialize all pointers to 0. Note that things like resetting
	// statistics variables should rather go into the initialize() function.
	declareProperty( "outputName", m_outputName,
			"Descriptive name for the processed sample" );

}


StatusCode MyxAODAnalysis :: initialize ()
{
	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected.
	// TFile *outputFile = wk() -> getOutputFile ("m_sampleName");


	TFile *outputFile = wk()->getOutputFile (m_outputName);

	tree2 = new TTree ("tree2", "tree2");
	tree2->SetDirectory (outputFile);
	tree2->Branch("wei",&wei);
	tree2->Branch("runNo",&runNo);
	tree2->Branch("EventNumber", &EventNumber);
	tree2->Branch("mass_Z",&mass_Z);
	tree2->Branch("AvePC",&AvePC);
	tree2->Branch("ActPC",&ActPC);
	tree2->Branch("Z_e",&Z_e);
	tree2->Branch("Z_pz",&Z_pz);
	tree2->Branch("Z_pt",&Z_pt);
	tree2->Branch("Z_rap",&Z_rap);
	tree2->Branch("ele1_Pz",&ele1_pz);
	tree2->Branch("ele2_Pz",&ele2_pz);
	tree2->Branch("ele1_e",&ele1_e);
	tree2->Branch("ele2_e",&ele2_e);
	tree2->Branch("ele1_el",&ele1_el);
	tree2->Branch("ele2_el",&ele2_el);
	tree2->Branch("ele1_pt",&ele1_pt);
	tree2->Branch("ele2_pt",&ele2_pt);
	tree2->Branch("ele1_eta",&ele1_eta);
	tree2->Branch("ele2_eta",&ele2_eta);
	tree2->Branch("ele1_deta",&ele1_deta);
	tree2->Branch("ele2_deta",&ele2_deta);
	tree2->Branch("ele1_deta0",&ele1_deta0);
	tree2->Branch("ele2_deta0",&ele2_deta0);
	tree2->Branch("ele1_dphi", &ele1_dphi);
	tree2->Branch("ele2_dphi", &ele2_dphi);
	tree2->Branch("ele1_dphi0",&ele1_dphi0);
	tree2->Branch("ele2_dphi0",&ele2_dphi0);
	tree2->Branch("ele1_phi",&ele1_phi);
	tree2->Branch("ele2_phi",&ele2_phi);
	tree2->Branch("dPhi_mm",&dPhi_mm);
	tree2->Branch("Ele_Reco_SF",&Ele_Reco_SF);
	tree2->Branch("Ele_Trig_SF",&Ele_Trig_SF);
	tree2->Branch("Ele_Iso_SF",&Ele_Iso_SF);
	tree2->Branch("Ele_ID_SF",&Ele_ID_SF);
	tree2->Branch("Ele_Ca_SF",&Ele_Ca_SF);
	tree2->Branch("Ele_Pie_W",&Ele_Pie_W);
	tree2->Branch("Ele_Trig_Eff",&Ele_Trig_Eff);
	tree2->Branch("Ptri1",&Ptri1);
	tree2->Branch("Ptri2",&Ptri2);
	tree2->Branch("Ptri3",&Ptri3);
	tree2->Branch("Ptri4",&Ptri4);
	tree2->Branch("Ptri5",&Ptri5);
	tree2->Branch("Ptri6",&Ptri6);
	tree2->Branch("Ptri7",&Ptri7);
	tree2->Branch("Ptri8",&Ptri8);
	tree2->Branch("Ptri9",&Ptri9);
	tree2->Branch("Ptri10",&Ptri10);
	tree2->Branch("eleTight",&eleTight);
	tree2->Branch("trigMatched",&trigMatched);
	tree2->Branch("calMet",&calMet);
	tree2->Branch("calMpx",&calMpx);
	tree2->Branch("calMpy",&calMpy);

	tree3 = new TTree ("tree3", "tree3");
	tree3->SetDirectory (outputFile);
	tree3->Branch("dr1",&dr1);
	tree3->Branch("eta1",&eta1);
	tree3->Branch("pt1",&pt1);
	tree3->Branch("phi1",&phi1);
	tree3->Branch("ID1",&ID1);
	tree3->Branch("dr2",&dr2);
	tree3->Branch("eta2",&eta2);
	tree3->Branch("pt2",&pt2);
	tree3->Branch("phi2",&phi2);
	tree3->Branch("ID2",&ID2);
	tree3->Branch("px1",&px1);
	tree3->Branch("py1",&py1);
	tree3->Branch("pz1",&pz1);
	tree3->Branch("e1",&e1);
	tree3->Branch("px2",&px2);
	tree3->Branch("py2",&py2);
	tree3->Branch("pz2",&pz2);
	tree3->Branch("e2",&e2);
	tree3->Branch("Epx1",&Epx1);
	tree3->Branch("Epy1",&Epy1);
	tree3->Branch("Epz1",&Epz1);
	tree3->Branch("Ee1", &Ee1);
	tree3->Branch("Epx2",&Epx2);
	tree3->Branch("Epy2",&Epy2);
	tree3->Branch("Epz2",&Epz2);
	tree3->Branch("Ee2" ,&Ee2);
	tree3->Branch("GammaPx",&GammaPx,"GammaPx[10]/D");
	tree3->Branch("GammaPy",&GammaPy,"GammaPy[10]/D");
	tree3->Branch("GammaPz",&GammaPz,"GammaPz[10]/D");
	tree3->Branch("GammaE",&GammaE,"GammaE[10]/D");
	tree3->Branch("GammaMother",&GammaMother,"GammaMother[10]/I");
	tree3->Branch("GammaNumber",&GammaNumber);
	tree3->Branch("Tpx", &Tpx);
	tree3->Branch("Tpy", &Tpy);
	tree3->Branch("Tpz", &Tpz);
	tree3->Branch("TE", &TE);

	tree4 = new TTree ("tree4", "tree4");
	tree4->SetDirectory (outputFile);
	tree4->Branch("Opx", &Opx);
	tree4->Branch("Opy", &Opy);
	tree4->Branch("Opz", &Opz);
	tree4->Branch("OE",  &OE);
	tree4->Branch("feta1",&feta1);
	tree4->Branch("fpt1", &fpt1);
	tree4->Branch("fphi1",&fphi1);
	tree4->Branch("fID1", &fID1);
	tree4->Branch("feta2", &feta2);
	tree4->Branch( "fpt2",  &fpt2);
	tree4->Branch("fphi2", &fphi2);
	tree4->Branch( "fID2",  &fID2);

	h_event_cutflow = new TH1D("h_event_cutflow","h_event_cutflow",23,0,23);
	wk()->addOutput(h_event_cutflow);
	h_ele_cutflow = new TH1D("h_ele_cutflow","h_ele_cutflow",23,0,23);
	wk()->addOutput(h_ele_cutflow);

	ANA_MSG_INFO( "SampleName    = " << m_outputName );

	bct = new Trig::WebBunchCrossingTool("WebBunchCrossingTool");

	//  bct = asg::AnaToolHandle<Trig::WebBunchCrossingTool>("Trig::WebBunchCrossingTool/WebBunchCrossingTool");
	ANA_CHECK(bct->setProperty("OutputLevel", MSG::INFO));
	ANA_CHECK(bct->setProperty("ServerAddress", "atlas-trigconf.cern.ch"));
	m_bct = bct;
	ANA_CHECK(m_bct->initialize());

	m_muonSelection = asg::AnaToolHandle<CP::IMuonSelectionTool>("CP::MuonSelectionTool/m_muonSelection");
	ANA_CHECK (m_muonSelection.setProperty("MaxEta", 2.5));
	ANA_CHECK (m_muonSelection.setProperty("MuQuality", 0));
	ANA_CHECK (m_muonSelection.initialize());



	//===================GoodRunList===============
	m_grl = asg::AnaToolHandle<IGoodRunsListSelectionTool>("GoodRunsListSelectionTool/grl");
	//  const char* GRLFilePath = "/afs/cern.ch/work/j/jugao/private/CCSkim/MyAnalysis/data/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
	//  const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);

	std::vector<std::string> vecStringGRL;
	//  const char* GRLFilePath = "/afs/cern.ch/work/j/jugao/private/CCSkim/MyAnalysis/data/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
	//  const char* GRLFilePath = "MyAnalysis/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml";
	//  const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
	vecStringGRL.push_back(PathResolverFindCalibFile("MyAnalysis/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml"));
	vecStringGRL.push_back(PathResolverFindCalibFile("MyAnalysis/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"));
	//  vecStringGRL.push_back(PathResolverFindXMLFile(fullGRLFilePath));
	//  vecStringGRL.push_back((fullGRLFilePath));
	ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
	ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
	ANA_CHECK(m_grl.initialize());  
	//  ANA_MSG_INFO ("in initialize");


	//============pileup=============
	//uses the lumicalc files from the calibration file area!
	//  std::vector<std::string> listOfLumicalcFiles = {"/afs/cern.ch/work/j/jugao/private/CCSkim/MyAnalysis/data/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-008.root","/afs/cern.ch/work/j/jugao/private/CCSkim/MyAnalysis/data/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-008.root"};
	std::vector<std::string> listOfLumicalcFiles;
	//  listOfLumicalcFiles.push_back(PathResolverFindCalibFile("/afs/cern.ch/work/j/jugao/private/CCSkimRel21/source/MyAnalysis/data/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-010.root"));
	// listOfLumicalcFiles.push_back( PathResolverFindCalibFile("MyAnalysis/mcaa.root"));
	listOfLumicalcFiles.push_back(PathResolverFindCalibFile("MyAnalysis/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-010.root"));
	listOfLumicalcFiles.push_back(PathResolverFindCalibFile("MyAnalysis/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-010.root"));
	std::vector<std::string> configFiles;
	configFiles.push_back( PathResolverFindCalibFile("MyAnalysis/mcaa.root"));
	std::cout<<listOfLumicalcFiles.at(0)<<std::endl;
	//  std::vector<std::string> configFiles = {"/eos/user/j/jugao/PRW/mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.NTUP_PILEUP.e3601_s3126_r9364_r9315_p3127_p3126/mcaa.root"};
	pileuptool = asg::AnaToolHandle<CP::IPileupReweightingTool>("CP::PileupReweightingTool/pileuptool");
	//asg::AnaToolHandle<CP::IPileupReweightingTool> pileuptool("CP::PileupReweightingTool/tool");
	ANA_CHECK(pileuptool.setProperty("LumiCalcFiles",listOfLumicalcFiles));
	ANA_CHECK(pileuptool.setProperty("ConfigFiles",configFiles));


	//================isolation===================
	iso_ele = new CP::IsolationSelectionTool ( "iso_ele" );
	ANA_CHECK( iso_ele->setProperty("ElectronWP","Gradient") );
	ANA_CHECK(iso_ele->initialize());

	//=============electron calibration==========
	m_elcali = asg::AnaToolHandle<CP::EgammaCalibrationAndSmearingTool>("CP::EgammaCalibrationAndSmearingTool/m_elcali");
	ANA_CHECK(m_elcali.setProperty("ESModel", "es2017_R21_v0"));
	ANA_CHECK(m_elcali.setProperty("decorrelationModel", "1NP_v1"));
	ANA_CHECK(m_elcali.setProperty("randomRunNumber", "123"));

	//=============electron ID==========
	m_MediumLH = new AsgElectronLikelihoodTool("LHMedium");
	m_MediumLH->setProperty("primaryVertexContainer","PrimaryVertices" );
	ANA_CHECK(m_MediumLH->setProperty("WorkingPoint", "MediumLHElectron"));
	ANA_CHECK(m_MediumLH->initialize());


	m_LooseLH = new AsgElectronLikelihoodTool("LHLoose");
	m_LooseLH->setProperty("primaryVertexContainer","PrimaryVertices" );
//	ANA_CHECK(m_LooseLH->setProperty("WorkingPoint", "LooseLHElectron"));
	ANA_CHECK(m_LooseLH->setProperty("ConfigFile", "ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodLooseOfflineConfig2017_Smooth.conf"));
	ANA_CHECK(m_LooseLH->initialize());

	m_TightLH = new AsgElectronLikelihoodTool("AsgElectronLikelihoodToolTight");
	m_TightLH->setProperty("primaryVertexContainer","PrimaryVertices" );
	m_TightLH->setProperty("WorkingPoint", "TightLHElectron" );
	ANA_CHECK(m_TightLH->initialize());

	// Initialize and configure trigger tools
	m_trigConfigTool = asg::AnaToolHandle<TrigConf::ITrigConfigTool>("TrigConf::xAODConfigTool/xAODConfigTool");
	//  m_trigConfigTool = asg::AnaToolHandle<TrigConf::xAODConfigTool>("TrigConf::xAODConfigTool/xAODConfigTool");
	m_trigDecisionTool = asg::AnaToolHandle<Trig::TrigDecisionTool>("Trig::TrigDecisionTool/TrigDecisionTool");
	ANA_CHECK (m_trigConfigTool.initialize());
	ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
	ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
	ANA_CHECK (m_trigDecisionTool.initialize());

	//==============Efficiency=============
	m_Iso_SF = new AsgElectronEfficiencyCorrectionTool("Iso_SF");
	std::vector<std::string> inputFilesIso{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/isolation/efficiencySF.Isolation.MediumLLH_d0z0_v13_isolGradient.root"};
	ANA_CHECK(m_Iso_SF->setProperty("CorrectionFileNameList",inputFilesIso));
	ANA_CHECK(m_Iso_SF->setProperty("ForceDataType",1));
	ANA_CHECK(m_Iso_SF->initialize());

	m_ID_SF = new AsgElectronEfficiencyCorrectionTool("ID_SF");
	std::vector<std::string> inputFilesID{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/offline/efficiencySF.offline.MediumLLH_d0z0_v13.root"};
	ANA_CHECK(m_ID_SF->setProperty("CorrectionFileNameList",inputFilesID));
	ANA_CHECK(m_ID_SF->setProperty("ForceDataType",1));
	ANA_CHECK(m_ID_SF->initialize());

	m_Reco_SF = new AsgElectronEfficiencyCorrectionTool("Reco_SF");
	std::vector<std::string> inputFilesReco{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/offline/efficiencySF.offline.RecoTrk.root"};
	ANA_CHECK(m_Reco_SF->setProperty("CorrectionFileNameList",inputFilesReco));
	ANA_CHECK(m_Reco_SF->setProperty("ForceDataType",1));
	ANA_CHECK(m_Reco_SF->initialize());

	m_Trig_SF15 = new AsgElectronEfficiencyCorrectionTool("Trig_SF15");
	//  std::vector<std::string> inputFilesTrig{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2017_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.root"};
	//15mc
	std::vector<std::string> inputFilesTrig{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiencySF.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.MediumLLH_d0z0_v13_isolGradient.2015.root"};
	//16mc
	//  std::vector<std::string> inputFilesTrig{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiency.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2016.root "};
	ANA_CHECK(m_Trig_SF15->setProperty("CorrectionFileNameList",inputFilesTrig));
	ANA_CHECK(m_Trig_SF15->setProperty("ForceDataType",1));
	ANA_CHECK(m_Trig_SF15->initialize());

	m_Trig_SF16 = new AsgElectronEfficiencyCorrectionTool("Trig_SF16");
	std::vector<std::string> inputFilesTrig16{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiencySF.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2016.root "};
	ANA_CHECK(m_Trig_SF16->setProperty("CorrectionFileNameList",inputFilesTrig16));
	// ANA_CHECK(m_Trig_SF->setProperty("MapFilePath","ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/"));
	// ANA_CHECK(m_Trig_SF->setProperty("TriggerKey","SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2017_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0"));
	//  ANA_CHECK(m_Trig_SF->setProperty("IdKey", "Medium"));
	// ANA_CHECK(m_Trig_SF->setProperty("IsoKey", "Gradient"));

	ANA_CHECK(m_Trig_SF16->setProperty("ForceDataType",1));
	ANA_CHECK(m_Trig_SF16->initialize());

	m_Trig_Eff15 = new AsgElectronEfficiencyCorrectionTool("Trig_Eff15");
	//  std::vector<std::string> inputFilesTrigEff{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiency.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2017_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.root"};
	//  std::vector<std::string> inputFilesTrigEff{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiency.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2017_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.root"};
	std::vector<std::string> inputFilesTrigEff15{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiency.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.MediumLLH_d0z0_v13_isolGradient.2015.root"};
	ANA_CHECK(m_Trig_Eff15->setProperty("CorrectionFileNameList",inputFilesTrigEff15));
	ANA_CHECK(m_Trig_Eff15->setProperty("ForceDataType",1));
	ANA_CHECK(m_Trig_Eff15->initialize());


	m_Trig_Eff16 = new AsgElectronEfficiencyCorrectionTool("Trig_Eff16");
	std::vector<std::string> inputFilesTrigEff16{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiency.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2016.root "};
	ANA_CHECK(m_Trig_Eff16->setProperty("CorrectionFileNameList",inputFilesTrigEff16));
	ANA_CHECK(m_Trig_Eff16->setProperty("ForceDataType",1));
	ANA_CHECK(m_Trig_Eff16->initialize());


	m_ForwardMedium = new  AsgForwardElectronIsEMSelector("m_ForwardMedium");
	ANA_CHECK(m_ForwardMedium->setProperty("WorkingPoint", "MediumForwardElectron"));
	ANA_CHECK(m_ForwardMedium->initialize());


	m_ForwardLoose = new  AsgForwardElectronIsEMSelector("m_ForwardLoose");
	ANA_CHECK(m_ForwardLoose->setProperty("WorkingPoint", "LooseForwardElectron"));
	ANA_CHECK(m_ForwardLoose->initialize());

	//B-tagging
	btagtool = new BTaggingSelectionTool("BTaggingSelectionTool");
	//  btagtool->setProperty("MaxEta", 2.5);
	//  btagtool->setProperty("MinPt", 20000.);
	ANA_CHECK(btagtool->setProperty("JetAuthor", "AntiKt4EMTopoJets"));
	ANA_CHECK(btagtool->setProperty("TaggerName", "MV2c10"));
	ANA_CHECK(btagtool->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-02-09_v1.root" ));
	//  btagtool->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2016-11-25_v1.root" );
	ANA_CHECK(btagtool->setProperty("OperatingPoint", "FixedCutBEff_85"));
	ANA_CHECK(btagtool->initialize() );

	m_trigMatch = asg::AnaToolHandle<Trig::IMatchingTool>( "Trig::MatchingTool/MatchingTool" );
	ANA_CHECK( m_trigMatch.setProperty("TrigDecisionTool",m_trigDecisionTool.getHandle()));
	ANA_CHECK(m_trigMatch.initialize());

	//==================Muon Calibration================
	m_muonCalibrationAndSmearingTool = asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool>("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool");
	//  m_muonCalibrationAndSmearingTool = asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool>("CP::IMuonCalibrationAndSmearingTool/MuonCorrectionTool");
	ANA_CHECK (m_muonCalibrationAndSmearingTool.initialize());

	//JetCleaning
	m_cleaningTool = asg::AnaToolHandle<IJetSelector>("JetCleaningTool/JetCleaning");
	m_cleaningTool.setProperty("CutLevel", "LooseBad");
	m_cleaningTool.setProperty("DoUgly", false);
	ANA_CHECK( m_cleaningTool.initialize() );

	//JetCalibration
	m_jetCalibration = asg::AnaToolHandle<IJetCalibrationTool>("JetCalibrationTool/JetCalibTool");
	m_jetCalibration.setProperty("JetCollection","AntiKt4EMTopo");
	m_jetCalibration.setProperty("ConfigFile","JES_data2017_2016_2015_Recommendation_Feb2018_rel21.config");
	m_jetCalibration.setProperty("CalibArea","00-04-81");
	//  if(isMC){
	//    m_jetCalibration->setProperty("ConfigFile","JES_data2016_data2015_Recommendation_Dec2016.config");
	//    m_jetCalibration.setProperty("CalibSequence","JetArea_Residual_Origin_EtaJES_GSC");
	//  }
	//  else{
	//    m_jetCalibration->setProperty("ConfigFile","JES_data2016_data2015_Recommendation_Dec2016.config");
	m_jetCalibration.setProperty("CalibSequence","JetArea_Residual_Origin_EtaJES_GSC_Insitu");
	//  }
	//   ANA_CHECK( m_jetCalibration.setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC") );

	m_jetCalibration.setProperty("IsData",true);
	ANA_CHECK( m_jetCalibration.retrieve() );

	// MetMaker
	metSystTool = asg::AnaToolHandle<IMETSystematicsTool>("met::METSystematicsTool/metSystTool");
	metSystTool.retrieve();

	metMaker = asg::AnaToolHandle<IMETMaker>("met::METMaker/metMaker");
	ANA_CHECK( metMaker.setProperty("DoMuonEloss", true) );
	ANA_CHECK( metMaker.setProperty("DoRemoveMuonJets", true) );
	ANA_CHECK( metMaker.setProperty("DoSetMuonJetEMScale", true) );
	ANA_CHECK( metMaker.retrieve() );

	iso_mu = new CP::IsolationSelectionTool ( "iso_mu" );
	ANA_CHECK( iso_mu->setProperty("MuonWP","Gradient") );
	ANA_CHECK(iso_mu->initialize());

	//========varible===============
	periodStart["2015ALL"] = 276262;
	periodStart["2016A"] = 296939;
	m_eventCounter = 0;
	//   _store = new TStore();

	//  double PIE = 3.1415926;
	//  double Nummuon = 0;
	//  double GeV = 1000;
	return StatusCode::SUCCESS;

}



StatusCode MyxAODAnalysis :: execute ()
{
	// Here you do everything that needs to be done on every single
	// events, e.g. read input variables, apply cuts, and fill
	// histograms and trees.  This is where most of your actual analysis
	// code will go.
	// retrieve the eventInfo object from the event store
	//   ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

	xAOD::TEvent* event = wk()->xaodEvent();

	std::unique_ptr<xAOD::TStore> store(new xAOD::TStore());

	if( (m_eventCounter % 1000) ==0 ) ANA_MSG_INFO("Event number = "<< m_eventCounter );
	m_eventCounter++;
	//    std::unique_ptr<xAOD::TEvent> eventD(new xAOD::TEvent( xAOD::TEvent::kClassAccess ) );

	const xAOD::VertexContainer* vertex = 0;
	ANA_CHECK(evtStore()->retrieve(vertex, "PrimaryVertices"));

	const xAOD::EventInfo* eventInfo = 0;
	ANA_CHECK(evtStore()->retrieve (eventInfo, "EventInfo")); 

	double  runNumber = eventInfo->runNumber();
	//  std::unique_ptr<xAOD::TStore> store(new xAOD::TStore());

	//vertex define
	bool primVtx = false;
	double nVtx = 0;
	for(unsigned int i=0;i<vertex->size();i++){
		const xAOD::Vertex* vxcand = vertex->at(i);
		if(vxcand->nTrackParticles()>2){
			primVtx = true;
			nVtx++;
		}
	}

	//===================Event Trigger========================
	bool PassTrigger1 = false;
	bool PassTrigger2 = false;
	bool PassTrigger3 = false;
	bool PassTrigger4 = false;
	bool PassTrigger5 = false;
	bool PassTrigger6 = false;
	bool PassTrigger7 = false;
	bool PassTrigger8 = false;
	bool PassTrigger9 = false;
	bool PassTrigger10 = false;
	bool PassElectronTrigger = false;
	bool PassMuonTrigger = false;
	bool PassTriggerM1 = false;
	bool PassTriggerM2 = false;
	bool PassTriggerM3 = false;
	bool PassTriggerM4 = false;
	bool PassTriggerM5 = false;
	bool PassTriggerM6 = false;
	std::vector<double> m_pt;
	std::vector<double> t_pt;
	std::vector<double> m_eta;
	std::vector<double> m_deta;
	std::vector<double> m_deta0;
	std::vector<double> m_dphi;
	std::vector<double> m_dphi0;
	std::vector<double> m_phi;
	std::vector<double> m_e;
	std::vector<double> m_pz;
	std::vector<int> m_charge;
	std::vector<double> t_ID_SF;
	std::vector<double> t_Reco_SF;
	std::vector<double> t_Iso_SF;
	std::vector<double> t_Trig_SF;
	std::vector<double> t_Calo_SF;
	std::vector<double> t_Trig_Eff;
	std::vector<double> m0_pt;
	std::vector<double> m0_eta;
	std::vector<double> m0_phi;
	std::vector<double> m0_deta;
	std::vector<double> m0_deta0;
	std::vector<int> m0_charge;
	bool isMC = false;
	int NumberGoodEles = 0;
	double ElePDGMass = 0.511*0.001;
	double ChargeElectron = 0;
	double  PtCut = 0;
	double  EtaCut = 0;
	double  LLHCut = 0;
	double  IsoCut = 0;
	double  d0z0Cut = 0;
	int RandomRunNumber = 0;
	TLorentzVector MuonEta;
	TLorentzVector ELEL;
	TLorentzVector TP;
	TLorentzVector TP0;

	std::vector<const xAOD::IParticle*> myParticles;
	double tele_pt;
	double tele_px;
	double tele_py;
	double tele_pz;
	double tele_eta;
	double tele_phi;
	double tele_ID;
	double tele_E;
	double tele_px0;
	double tele_ID0;
	double tele_py0;
	double tele_pz0;
	double tele_pt0;
	double tele_eta0;
	double tele_phi0;
	double tele_e0;
	double tele_e;



	if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
		isMC = true; // can do something with this later
	}
	if(isMC){
		trigList_el["2015ALL"].push_back("HLT_e24_lhmedium_L1EM18VH");
	}
	else{
		trigList_el["2015ALL"].push_back("HLT_e24_lhmedium_L1EM20VH");
	}
	trigList_el["2015ALL"].push_back("HLT_e60_lhmedium");
	trigList_el["2015ALL"].push_back("HLT_e120_lhloose");
	trigList_el["2016ALL"].push_back("HLT_e24_lhmedium_nod0_L1EM20VH");
	trigList_el["2016ALL"].push_back("HLT_e24_lhtight_nod0_ivarloose");
	trigList_el["2016ALL"].push_back("HLT_e60_lhmedium_nod0");
	trigList_el["2016ALL"].push_back("HLT_e60_medium");
	trigList_el["2016ALL"].push_back("HLT_e26_lhtight_nod0_ivarloose");
	trigList_el["2016ALL"].push_back("HLT_e300_etcut");
	trigList_el["2016ALL"].push_back("HLT_e140_lhloose_nod0");


	PassTrigger1 = m_trigDecisionTool->isPassed("HLT_e24_lhmedium_L1EM18VH");
	PassTrigger2 = m_trigDecisionTool->isPassed("HLT_e60_lhmedium");
	PassTrigger3 = m_trigDecisionTool->isPassed("HLT_e120_lhloose");

	PassTrigger4 = m_trigDecisionTool->isPassed("HLT_e24_lhtight_nod0_ivarloose");
	PassTrigger5 = m_trigDecisionTool->isPassed("HLT_e24_lhmedium_nod0_L1EM20VH");
	PassTrigger6 = m_trigDecisionTool->isPassed("HLT_e60_lhmedium_nod0");
	PassTrigger7 = m_trigDecisionTool->isPassed("HLT_e60_medium");
	PassTrigger8 = m_trigDecisionTool->isPassed("HLT_e26_lhtight_nod0_ivarloose");
	PassTrigger9 = m_trigDecisionTool->isPassed("HLT_e300_etcut");
	PassTrigger10 = m_trigDecisionTool->isPassed("HLT_e140_lhloose_nod0");
	PassTriggerM3 = m_trigDecisionTool->isPassed("HLT_mu40");
	PassTriggerM4 = m_trigDecisionTool->isPassed("HLT_mu24_iloose");
	PassTriggerM5 = m_trigDecisionTool->isPassed("HLT_mu24_ivarmedium");
	PassTriggerM6 = m_trigDecisionTool->isPassed("HLT_mu20_iloose_L1MU15");
	if ( PassTrigger1 == true || PassTrigger2 == true || PassTrigger3 == true|| PassTrigger4 == true || PassTrigger5 == true || PassTrigger6 == true || PassTrigger7 == true || PassTrigger8 == true || PassTrigger9 == true || PassTrigger10 == true )  PassElectronTrigger = true;
	PassTriggerM1 = m_trigDecisionTool->isPassed("HLT_mu26_ivarmedium");
	PassTriggerM2 = m_trigDecisionTool->isPassed("HLT_mu50");

	if (PassTriggerM3 == true || PassTriggerM1 == true || PassTriggerM2 == true ||PassTriggerM4==true||PassTriggerM5==true||PassTriggerM6==true) PassMuonTrigger =true;


	//============PRW==============
	if (isMC){
		pileuptool->apply( *eventInfo, true);
		Ele_Pie_W = pileuptool->getCombinedWeight(*eventInfo);
		wei = eventInfo->mcEventWeights().at(0);
		RandomRunNumber = eventInfo->auxdecor<unsigned int>("RandomRunNumber");

		//  if ( (RandomRunNumber > 284483) &&(RandomRunNumber<297730))  std::cout<<RandomRunNumber<<std::endl;
	}

	store->clear();
	const xAOD::JetContainer* jets = 0;
	ANA_CHECK(evtStore()->retrieve (jets, "AntiKt4EMTopoJets"));

	const xAOD::ElectronContainer* electrons = 0;
	ANA_CHECK(evtStore()->retrieve (electrons, "Electrons"));

	const xAOD::ElectronContainer* forelectrons;
	ANA_CHECK(evtStore()->retrieve (forelectrons, "ForwardElectrons"));

	const xAOD::MuonContainer* muons = 0;
	ANA_CHECK (evtStore()->retrieve (muons, "Muons"));


	//  std::unique_ptr<xAOD::MuonContainer> shallowmus (mus_shallowCopy.first);
	//  std::unique_ptr<xAOD::ShallowAuxContainer> shallowmuAux (mus_shallowCopy.second);
	//  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
	//  std::unique_ptr<xAOD::MuonContainer> shallmuowels (muons_shallowCopy.first);
	//  std::unique_ptr<xAOD::ShallowAuxContainer> shallmuowAux (muons_shallowCopy.second);
	//  auto muonsCorr = xAOD::shallowCopyContainer (*muons);

	//    ANA_CHECK( store->record(MuonCorr, "CalibMus") );

	// std::cout<<"SB2"<<std::endl;

	std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets );
	calibJets = jets_shallowCopy.first;
	double NBjets = 0;
	met::addGhostMuonsToJets(*muons, *calibJets);
	for (auto jetSC:*calibJets ) {

		m_jetCalibration->applyCalibration(*jetSC);

		if (!m_cleaningTool->keep (*jetSC)) continue; 

		if(!(fabs(jetSC->eta())<2.5 && btagtool->accept( *jetSC ))) continue;
		NBjets++;


	} 

	bool setLinks = xAOD::setOriginalObjectLink(*jets,*calibJets);
	if(!setLinks) {
		ATH_MSG_WARNING("Failed to set original object links -- MET rebuilding cannot proceed.");
		return StatusCode::FAILURE;
	}

	ANA_CHECK( store->record(calibJets, "CalibJets") );


	// std::cout<<"SB3"<<std::endl;

	std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > els_shallowCopy1 = xAOD::shallowCopyContainer( *electrons );
	//  auto els_shallowCopy = xAOD::shallowCopyContainer (*electrons);
	elsCorr = els_shallowCopy1.first;
	for (auto eleSC1:*elsCorr) {
		ANA_CHECK(m_elcali->applyCorrection(*eleSC1));
	}
	bool setLinkels = xAOD::setOriginalObjectLink(*electrons,*elsCorr);
	if(!setLinkels) {
		ATH_MSG_WARNING("Failed to set original object links -- MET rebuilding cannot proceed.");
		return StatusCode::FAILURE;
	}
	ANA_CHECK( store->record(elsCorr, "Calibels") );

	int NumberGoodMus=0;

	auto els_shallowCopy = xAOD::shallowCopyContainer (*muons);
	std::unique_ptr<xAOD::MuonContainer> shallowels (els_shallowCopy.first);
	std::unique_ptr<xAOD::ShallowAuxContainer> shallowAux (els_shallowCopy.second);

	for (auto eleSC:*els_shallowCopy.first ) {
		if(m_muonCalibrationAndSmearingTool->applyCorrection(*eleSC)!= CP::CorrectionCode::Ok){
			ANA_MSG_INFO ("execute(): Problem with Muon Calibration And Smearing Tool (Error or OutOfValidityRange) ");
		}

		//total
		h_ele_cutflow -> Fill(0);

		//Event cleaning cut
		if(   (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )  ) continue;
		h_ele_cutflow -> Fill(1);

		//vertex cut
		if(!primVtx) continue;
		h_ele_cutflow -> Fill(2);

		//Tigger cut
		if(PassMuonTrigger == false) continue;
		h_ele_cutflow -> Fill(3);

		if((eleSC)->pt()*0.001 <10) continue;
		h_ele_cutflow -> Fill(4);
		PtCut++;

		//ele eta cut
		double ddeta = (eleSC)->eta();
		if(fabs(ddeta) > 2.5) continue;
		//  if(fabs(ddeta) > 2.47 || (fabs(ddeta)>1.37 && fabs(ddeta)<1.52)) continue;
		h_ele_cutflow -> Fill(5);
		EtaCut++;

		if((eleSC)->muonType()!=xAOD::Muon::Combined) continue;
		// ele author cut
		//  if((eleSC)->author() != 1 && (eleSC)->author() != 16) continue;
		h_ele_cutflow -> Fill(6);

		h_ele_cutflow -> Fill(7);

		//ele ID cut
		if(!(m_muonSelection->accept(*eleSC))) continue;
		h_ele_cutflow -> Fill(8);
		LLHCut++;

		//Isolation cut
		if (!(iso_mu->accept(*eleSC))) continue;
		h_ele_cutflow -> Fill(9);
		IsoCut++;

		//ele d0 cut
		//  const xAOD::TrackParticle *trk = eleSC->trackParticle() ;
		const xAOD::TrackParticle *trk = eleSC->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle) ;
		double d0_sign = 0.0;
		bool z0_pass = false;
		double d_z0 = 0.0;

		if(trk){
			d0_sign = fabs(xAOD::TrackingHelpers::d0significance( trk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
			if(d0_sign > 5 ) continue;

			//ele z0 cut
			//  bool z0_pass = false;
			for(unsigned int i=0;i<vertex->size();i++){
				const xAOD::Vertex* ele_vtx=vertex->at(i);
				if(ele_vtx->vertexType() == xAOD::VxType::PriVtx){
					d_z0 = fabs((trk->z0() + trk->vz() - ele_vtx->z())*sin(trk->theta()));
				}
				if(d_z0 < 0.5) z0_pass = true;
			}
		}
		if(!z0_pass) continue;
		h_ele_cutflow -> Fill(10);
		d0z0Cut ++;

		m_pt.push_back((eleSC)->pt()*0.001);
		//  if(trk) { t_pt.push_back(trk->pt()*0.001);}
		m_eta.push_back((eleSC)->eta());

		m_eta.push_back((eleSC)->eta());
		//  m_deta.push_back((eleSC)->caloCluster()->etaBE(2));
		//  m_dphi.push_back((eleSC)->caloCluster()->phi());
		m_phi.push_back((eleSC)->phi());
		m_charge.push_back((eleSC)->charge());
		//  m_deta0.push_back((eleSC)->caloCluster()->getMomentValue(xAOD::CaloCluster::ETACALOFRAME));
		//  m_dphi0.push_back((eleSC)->caloCluster()->getMomentValue(xAOD::CaloCluster::PHICALOFRAME));

		if( isMC)
		{
		}

		NumberGoodMus++;
	}
	// std::cout<<"SB4"<<std::endl;

	//  if(!xAOD::setOriginalObjectLink(*electrons, *(els_shallowCopy.first))) std::cout << "Failed to set the original object links" << std::endl;
	//    ANA_CHECK( store->record((els_shallowCopy.first), "elec") );

	//  std::cout<<"1111111"<<std::endl;
	double PtCut0 = 0;
	double EtaCut0 = 0;
	double LLHCut0 = 0;
	int NumberGoodfwdEles = 0;
	int counterm = -1;
	double distance  = -999;
	auto t_bcid = eventInfo->auxdata<unsigned int>("bcid");
	std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > els0_shallowCopy = xAOD::shallowCopyContainer( *forelectrons );
	xAOD::ElectronContainer::iterator eleSC0_itr = (els0_shallowCopy.first)->begin();
	xAOD::ElectronContainer::iterator eleSC0_end = (els0_shallowCopy.first)->end();
	for(;eleSC0_itr != eleSC0_end;++eleSC0_itr){
		counterm++;
		//    auto t_bcid = eventInfo->auxdata<unsigned int>("bcid");
		int gapbtrain = m_bct->gapBeforeTrain(t_bcid);
		if(gapbtrain>500)
			distance = m_bct->distanceFromFront(t_bcid);
		else
			distance = bct->distanceFromFront(t_bcid)+bct->gapBeforeTrain(t_bcid) + ( bct->distanceFromFront(t_bcid)+bct->distanceFromTail(t_bcid));
		//    std::cout << distance <<std::endl;
		//double distance =  bct->gapBeforeTrain()>500 ? bct->distFromFront() : bct->distFromFront()+bct->gapBeforeTrain() + ( bct->distFromFront()+bct->distFromTail() );

		//  double distance =  bct.gapBeforeTrain()>500 ? bct.distFromFront() : bct.distFromFront()+bct.gapBeforeTrain() + ( bct.distFromFront()+bct.distFromTail() );
		if ( distance <  5.5*25) continue;
		ANA_CHECK(m_elcali->applyCorrection(**eleSC0_itr));
		//      }

		//Event cleaning cut
		if(   (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )  ) continue;

		//vertex cut
		if(!primVtx) continue;

		//ele pt reconsruction
		if((*eleSC0_itr)->pt()*0.001 < 10) continue;

		//ele eta cut
		double fdeta = (*eleSC0_itr)->caloCluster()->eta();
		if(fabs(fdeta) < 2.5 || (fabs(fdeta)>3.16 && fabs(fdeta)<3.35)||fabs(fdeta) > 4.9) continue;

		//ele OQ cut
		//  bool OQ = (*eleSC0_itr)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON);
		//  if( OQ == false) continue;

		if(( m_ForwardLoose->accept(*eleSC0_itr) )) continue;
		NumberGoodfwdEles++;
		if (NumberGoodfwdEles == 1)
		{
			m0_pt.push_back((*eleSC0_itr)->pt()*0.001);
			m0_eta.push_back((*eleSC0_itr)->eta());
			m0_phi.push_back((*eleSC0_itr)->phi());
			m0_charge.push_back(0);
			m0_deta.push_back((*eleSC0_itr)->caloCluster()->eta());
		}
		//  m0_deta0.push_back((*eleSC0_itr)->caloCluster()->getMomentValue(xAOD::CaloCluster::ETACALOFRAME));
}
delete els0_shallowCopy.first;
delete els0_shallowCopy.second;
// std::cout<<"SB5"<<std::endl;
auto mus_shallowCopy = xAOD::shallowCopyContainer (*muons);
MuonCorr = mus_shallowCopy.first;
//    store->record( mus_shallowCopy.first,  "CalibMuons"    );
//    store->record( mus_shallowCopy.second, "CalibMuonsAux.");
for (auto muSC:*MuonCorr ) {
	if(m_muonCalibrationAndSmearingTool->applyCorrection(*muSC)!= CP::CorrectionCode::Ok){
		ANA_MSG_INFO ("execute(): Problem with Muon Calibration And Smearing Tool (Error or OutOfValidityRange) ");
	}
}

bool setLinkmus = xAOD::setOriginalObjectLink(*muons,*MuonCorr);
if(!setLinkmus) {
	ATH_MSG_WARNING("Failed to set original object links -- MET rebuilding cannot proceed.");
	return StatusCode::FAILURE;
}
ANA_CHECK( store->record(MuonCorr, "CalibMus") );


bool doMetCalculation =1;

if(doMetCalculation){
	std::string coreMetKey = "MET_Core_" + jetType;
	std::string metAssocKey = "METAssoc_" + jetType;
	coreMetKey.erase(coreMetKey.length() - 4);//this removes the string "Jets" from the end of the jetType
	metAssocKey.erase(metAssocKey.length() - 4 );
	//retrieve the MET core
	const xAOD::MissingETContainer* coreMet  = nullptr;
	evtStore()->retrieve(coreMet, coreMetKey);

	//retrieve the MET association map
	const xAOD::MissingETAssociationMap* metMap = nullptr;
	evtStore()->retrieve(metMap, "METAssoc_AntiKt4EMTopo");

	xAOD::MissingETContainer*    newMetContainer    = new xAOD::MissingETContainer();
	xAOD::MissingETAuxContainer* newMetAuxContainer = new xAOD::MissingETAuxContainer();
	newMetContainer->setStore(newMetAuxContainer);

	// It is necessary to reset the selected objects before every MET calculation
	metMap->resetObjSelectionFlags();

	//here we apply some basic cuts and rebuild the met at each step
	//Electrons
	ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
	for(const auto& m_el : *elsCorr) {
		if((m_el)->pt()>10*1000 && (m_MediumLH->accept(m_el)))  metElectrons.push_back(m_el);
		//          if((m_el)->pt()>10*1000  )  metElectrons.push_back(m_el);
	}
	//        for(const auto& m_el : *fwdelsCorr) {
	//          if((m_el)->pt()>10*GeV && m_FwdElectronIDToolLoose->accept(m_el))  metElectrons.push_back(m_el);
	//        }
	(metMaker)->rebuildMET("RefEle",                   //name of metElectrons in metContainer
			xAOD::Type::Electron,       //telling the rebuilder that this is electron met
			newMetContainer,            //filling this met container
			metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
			metMap)                     //and this association map
		;


	//Muons
	ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
	for(const auto& m_mu : *MuonCorr) {
		if((m_mu)->pt()>10*1000 && m_muonSelection->accept(*m_mu))  metMuons.push_back(m_mu);
	}
	//  if(!xAOD::setOriginalObjectLink(*muons, *(muonsCorr.first))) std::cout << "Failed to set the original object links" << std::endl;
	//    ANA_CHECK( store->record((muonsCorr.first), "muonaa") );
	(metMaker)->rebuildMET("RefMuon",
			xAOD::Type::Muon,
			newMetContainer,
			metMuons.asDataVector(),
			//                                   metMuons,
			metMap)
		;
	//Now time to rebuild jetMet and get the soft term
	//This adds the necessary soft term for both CST and TST
	//these functions create an xAODMissingET object with the given names inside the container

	(metMaker)->rebuildJetMET("RefJet",        //name of jet met
			"SoftClus",      //name of soft cluster term met
			"PVSoftTrk",     //name of soft track term met
			newMetContainer, //adding to this new met container
			calibJets,       //using this jet collection to calculate jet met
			coreMet,         //core met container
			metMap,          //with this association map
			false            //apply jet jvt cut
			)
		;

	(metMaker)->buildMETSum("FinalTrk" , newMetContainer, MissingETBase::Source::Track );

	xAOD::MissingET* metFinal = 0;
	if(!(metFinal=(*newMetContainer)["FinalTrk"])) std::cout<<"met retrieve failed"<<std::endl;
	calMet = metFinal->met()*0.001;
	calMpx = metFinal->mpx()*0.001;
	calMpy = metFinal->mpy()*0.001;

	delete newMetContainer;
	delete newMetAuxContainer;

}


h_event_cutflow->Fill(0);
if(   (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )  ) return StatusCode::SUCCESS; // go to the next event
h_event_cutflow->Fill(1);

//vertex cut
if(!primVtx)return StatusCode::SUCCESS;
h_event_cutflow->Fill(2);


if(PassMuonTrigger == false) return StatusCode::SUCCESS;
h_event_cutflow->Fill(3);

// std::cout<<"SB7"<<std::endl;
if(PtCut <1) return StatusCode::SUCCESS;
h_event_cutflow->Fill(4);

// std::cout<<"SB8"<<std::endl;

if(EtaCut <1) return StatusCode::SUCCESS;
h_event_cutflow->Fill(5);

if(LLHCut <1) return StatusCode::SUCCESS;
h_event_cutflow->Fill(6);

if(IsoCut <1) return StatusCode::SUCCESS;
h_event_cutflow->Fill(7);

if(d0z0Cut<1) return StatusCode::SUCCESS;
h_event_cutflow->Fill(8);


//  std::cout<<"2222"<<std::endl;

// std::cout<<"SB7"<<std::endl;


TLorentzVector m1;
TLorentzVector m2;
//  std::cout<<m_charge[0]<<std::endl;
//  std::cout<<m0_charge[0]<<std::endl;

//   if(!(NumberGoodEles==1&&m_charge[0]!=m0_charge[0]&&NumberGoodfwdEles==1))return StatusCode::SUCCESS;
if(!(NumberGoodMus==1&&NumberGoodfwdEles>0&&NBjets==0))return StatusCode::SUCCESS;
//   if(!(NumberGoodEles==2&&m_charge[0]!=m_charge[1]))return StatusCode::SUCCESS;
{
	m1.SetPtEtaPhiM(m_pt[0],m_eta[0],m_phi[0],ElePDGMass);
	m2.SetPtEtaPhiM(m0_pt[0],m0_eta[0],m0_phi[0],ElePDGMass);
	mass_Z = ((m1+m2).M());
	h_event_cutflow->Fill(9);

	if (!(mass_Z>70&&mass_Z<110)) return StatusCode::SUCCESS;
	h_event_cutflow->Fill(10);

	int r1=0;
	int r2=0;

	//  std::cout<<"3333"<<std::endl;

	if( isMC )
	{
		int sta = 0;
		int phn = 0;
		fpt1 = 0;
		const xAOD::TruthParticleContainer* particles = 0;
		ANA_CHECK(evtStore()->retrieve(particles, "TruthParticles"));
		std::pair< xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer* > particles_shallowCopy = xAOD::shallowCopyContainer( *particles );
		xAOD::TruthParticleContainer::iterator particleSC_itr = (particles_shallowCopy.first)->begin();
		xAOD::TruthParticleContainer::iterator particleSC_end = (particles_shallowCopy.first)->end();
		xAOD::TruthParticleContainer::iterator particleSC_itr1 = (particles_shallowCopy.first)->begin();
		xAOD::TruthParticleContainer::iterator particleSC_end1 = (particles_shallowCopy.first)->end();
		xAOD::TruthParticleContainer::iterator particleSC_itr2 = (particles_shallowCopy.first)->begin();
		xAOD::TruthParticleContainer::iterator particleSC_end2 = (particles_shallowCopy.first)->end();
		xAOD::TruthParticleContainer::iterator particleSC_itr3 = (particles_shallowCopy.first)->begin();
		xAOD::TruthParticleContainer::iterator particleSC_end3 = (particles_shallowCopy.first)->end();

		for( ; particleSC_itr1 != particleSC_end1 ; ++particleSC_itr1 )
		{
			if(!((*particleSC_itr1)->pdgId()==23) )continue;
			if(!((*particleSC_itr1)->status()==62) )continue;
			if(! ((*particleSC_itr1))) continue;
			double  nda1 = (*particleSC_itr1)->nChildren();
			for( int i = 0;i<nda1;i++)
			{
				if(((*particleSC_itr1)->child(i)->pdgId() ==22) ) phn++;
				if(!((*particleSC_itr1)->child(i)->status() ==1) ) continue;
				if(!((*particleSC_itr1)->child(i)->pdgId()==11||(*particleSC_itr1)->child(i)->pdgId()==-11))continue;
				tele_eta0 = (*particleSC_itr1)->child(i)->eta();
				tele_phi0 = (*particleSC_itr1)->child(i)->phi();
				tele_ID0  = (*particleSC_itr1)->child(i)->pdgId();
				tele_px0 = ((*particleSC_itr1)->child(i)->px()*0.001);
				tele_py0 = ((*particleSC_itr1)->child(i)->py()*0.001);
				tele_pt0 = sqrt(tele_px0*tele_px0+tele_py0*tele_py0);
				if ( tele_ID0 == -11 )
				{
					feta1 = tele_eta0;
					fpt1 = tele_pt0;
					fphi1 = tele_phi0;
					fID1 = tele_ID0;
				}

				if ( tele_ID0 == 11 )
				{
					feta2 = tele_eta0;
					fpt2 = tele_pt0;
					fphi2 = tele_phi0;
					fID2 = tele_ID0;
				}
			}
		}

		if (phn > 0)
		{
			for( ; particleSC_itr2 != particleSC_end2; ++particleSC_itr2 )
			{

				if(!((*particleSC_itr2)->pdgId()==11||(*particleSC_itr2)->pdgId()==-11 ) )continue;
				if(!((*particleSC_itr2)->status() == 3)) continue;
				sta++;
				//  if (((*particleSC_itr2)->px()*0.001) < 1) continue;
				ELEL.SetPxPyPzE( ((*particleSC_itr2)->px()*0.001),((*particleSC_itr2)->py()*0.001),((*particleSC_itr2)->pz()*0.001),((*particleSC_itr2)->e()*0.001));
				if (sta > 2 )
				{
					if( ( ELEL.Pt() < fpt1 ) &&( ELEL.Pt() < fpt2 )) continue;
				}

				if ((*particleSC_itr2)->pdgId() == -11)
				{
					feta1 =  ELEL.Eta();
					fpt1 =  ELEL.Pt();
					fphi1 =  ELEL.Phi();
					fID1 = -11;
				}

				if ((*particleSC_itr2)->pdgId() == 11)
				{
					feta2 =  ELEL.Eta();
					fpt2 =  ELEL.Pt();
					fphi2 =  ELEL.Phi();
					fID2 = 11;
				}
			}
		}

		for( ; particleSC_itr3 != particleSC_end3; ++particleSC_itr3 ){
			if(!((*particleSC_itr3)->pdgId()==23) )continue;
			if(!((*particleSC_itr3)->status()==22) )continue;
			Opx = ((*particleSC_itr3)->px()*0.001);
			Opy = ((*particleSC_itr3)->py()*0.001);
			Opz = ((*particleSC_itr3)->pz()*0.001);
			OE = ((*particleSC_itr3)->e()*0.001);
		}

		for( ; particleSC_itr != particleSC_end; ++particleSC_itr ){

			if(!((*particleSC_itr)->pdgId()==23) )continue;
			if(!((*particleSC_itr)->status()==62) )continue;
			Tpx = ((*particleSC_itr)->px()*0.001);
			Tpy = ((*particleSC_itr)->py()*0.001);
			Tpz = ((*particleSC_itr)->pz()*0.001);
			TE = ((*particleSC_itr)->e()*0.001);
			Tele_Pt = sqrt(Tpx*Tpx+Tpy*Tpy)  ;
			//  if ( Tele_Pt < 25) continue;
			MuonEta.SetPxPyPzE(Tpx,Tpy,Tpz,((*particleSC_itr)->e()*0.001));
			Tele_Eta = MuonEta.Eta();
			Tele_Phi = MuonEta.Phi();
			Tele_ID = (*particleSC_itr)->pdgId();
			barcode = (*particleSC_itr)->barcode();
			status = (*particleSC_itr)->status();
			if  (! ((*particleSC_itr))) continue;
			double  nda = (*particleSC_itr)->nChildren();
			int n = -1;
			for( int i = 0;i<nda;i++)
			{
				if( (*particleSC_itr)->child(i)->pdgId()==22)
				{

					n++;
					if (n>9) continue;
					GammaPx[n] = ((*particleSC_itr)->child(i)->px()*0.001);
					GammaPy[n] = ((*particleSC_itr)->child(i)->py()*0.001);
					GammaPz[n] = ((*particleSC_itr)->child(i)->pz()*0.001);
					GammaMother[n] = (*particleSC_itr)->child(i)->parent(0)->pdgId();
					// gamapt = sqrt(tele_px*tele_px+tele_py*tele_py);
					GammaE[n] = ((*particleSC_itr)->child(i)->e()*0.001);
					// status = (*particleSC_itr1)->status();
					// barcode =(*particleSC_itr1)->barcode();
					GammaNumber = n+1;

				}

				if(!((*particleSC_itr)->child(i)->status() ==1) ) continue;
				if(!((*particleSC_itr)->child(i)->pdgId()==11||(*particleSC_itr)->child(i)->pdgId()==-11))continue;
				tele_px = ((*particleSC_itr)->child(i)->px()*0.001);
				tele_py = ((*particleSC_itr)->child(i)->py()*0.001);
				tele_pz = ((*particleSC_itr)->child(i)->pz()*0.001);
				tele_pt = sqrt(tele_px*tele_px+tele_py*tele_py);
				TP.SetPxPyPzE(Tpx,Tpy,Tpz,((*particleSC_itr)->child(i)->e()*0.001));
				tele_eta = (*particleSC_itr)->child(i)->eta();
				tele_phi = (*particleSC_itr)->child(i)->phi();
				tele_ID  = (*particleSC_itr)->child(i)->pdgId();
				tele_E = ((*particleSC_itr)->child(i)->e()*0.001);
				//  if (Tele_Eta > 2.5||Tele_Eta <-2.5 ) continue;
				double deta1 = tele_eta - m_eta[0];
				double dphi1 = tele_phi - m_phi[0];
				double dR1 = sqrt(deta1*deta1+dphi1*dphi1);
				double deta2 = tele_eta - m0_eta[0];
				double dphi2 = tele_phi - m0_phi[0];
				double dR2 = sqrt(deta2*deta2+dphi2*dphi2);

				if (dR1>0.3&&dR2>0.3) continue;
				if (dR1<0.3&&dR2<0.3) continue;
				//  std::cout<<"3333"<<std::endl;
				//  if (dR1<0.3&&dR2<0.3)
				// std::cout<<"dr="<<dR1<<std::endl;
				if ( dR1 < 0.3)
					//  r1++;
				{
					r1++;
					dr1 = dR1;
					eta1 = tele_eta;
					pt1 = tele_pt;
					phi1 = tele_phi;
					ID1 = tele_ID;
					px1 =tele_px;
					py1 =tele_py;
					pz1 =tele_pz;
					e1 =tele_E;

					//   tree3->Fill();
				}

				if ( dR2 < 0.3)
					//  r2++;
				{
					r2++;
					dr2 =dR2;
					eta2 = tele_eta;
					pt2 = tele_pt;
					phi2 = tele_phi;
					ID2 = tele_ID;
					px2 =tele_px;
					py2 =tele_py;
					pz2 =tele_pz;
					e2  =tele_E;
				}
				//  else
				//  continue;
				//  tree3->Fill();
				//  tree4->Fill();
			}
		}

		delete particles_shallowCopy.first;
		delete particles_shallowCopy.second;

	}


	//  std::cout<<"2222"<<std::endl;


	Z_e = ((m1+m2).E());
	Z_pz = ((m1+m2).Pz());
	Z_rap = 0.5*log((Z_e+Z_pz)/(Z_e-Z_pz));
	Z_pt = ((m1+m2).Pt());

	ele1_pz = m1.Pz();
	ele1_e = m1.E();
	ele1_el = m_charge[0];
	ele2_el = m0_charge[0];
	ele2_e = m2.E();
	ele2_pz = m2.Pz();
	ele1_pt = m_pt[0];
	ele1_eta = m_eta[0];
	//    ele1_deta = m_deta[0];
	//    ele1_deta0 = m_deta0[0];
	ele1_phi = m_phi[0];
	ele2_pt = m0_pt[0];
	ele2_eta = m0_eta[0];
	ele2_deta = m0_deta[0];
	//    ele2_deta0 = m0_deta0[0];
	ele2_phi = m0_phi[0];
	if( isMC )  
	{
		if (r1!=1||r2!=1) return StatusCode::SUCCESS;

		//    if( fpt1 < 1 )
		// {
		//  std::cout<<EventNumber<<std::endl;
		//  std::cout<<fpt1<<std::endl;
		// }
		//   dr1 = dR1;
		//   eta1 = Tele_Eta;
		//   ID1 = Tele_ID;
		tree4->Fill();
		tree3->Fill();


	}
	tree2->Fill();
	store->clear();

}

// print out run and event number from retrieved object


return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
	// This method is the mirror image of initialize(), meaning it gets
	// called after the last event has been processed on the worker node
	// and allows you to finish up any objects you created in
	// initialize() before they are written to disk.  This is actually
	// fairly rare, since this happens separately for each worker node.
	// Most of the time you want to do your post-processing on the
	// submission node after all your histogram outputs have been
	// merged.
	// cleaning up trigger tools
	//   if( m_trigConfigTool ) {
	//      delete m_trigConfigTool;
	//      m_trigConfigTool = 0;
	//   }
	//   if( m_trigDecisionTool ) {
	//      delete m_trigDecisionTool;
	//      m_trigDecisionTool = 0;
	//    }
	//  if (iso_ele){
	//  delete iso_ele;
	//  iso_ele = 0;
	//  }
	//
	//  if (m_elcali)
	//   {delete m_elcali;
	//  m_elcali = 0;
	//  }
	//
	//  if (m_MediumLH)
	//  {delete m_MediumLH ;
	//   m_MediumLH = 0;
	//  }
	//    if (m_grl) {
	//    delete m_grl;
	//    m_grl = 0;
	//  }


	std::cout<<"Event_CutFlow     :"<<std::endl;
	std::cout<<"Total             :"<<h_event_cutflow->GetBinContent(1)<<std::endl;
	std::cout<<"Event Cleaning    :"<<h_event_cutflow->GetBinContent(2)<<std::endl;
	std::cout<<"Npv               :"<<h_event_cutflow->GetBinContent(3)<<std::endl;
	std::cout<<"Trigger           :"<<h_event_cutflow->GetBinContent(4)<<std::endl;
	std::cout<<"pt cut            :"<<h_event_cutflow->GetBinContent(5)<<std::endl;
	std::cout<<"eta cut           :"<<h_event_cutflow->GetBinContent(6)<<std::endl;
	std::cout<<"llh  cut          :"<<h_event_cutflow->GetBinContent(7)<<std::endl;
	std::cout<<"iso cut           :"<<h_event_cutflow->GetBinContent(8)<<std::endl;
	std::cout<<"d0z0cut           :"<<h_event_cutflow->GetBinContent(9)<<std::endl;
	std::cout<<"2 opposite e      :"<<h_event_cutflow->GetBinContent(10)<<std::endl;
	std::cout<<"mass cut          :"<<h_event_cutflow->GetBinContent(11)<<std::endl;
	return StatusCode::SUCCESS;
}

//double MyxAODAnalysis :: GetElectronIsoSF_Tight(auto *p){
//  double m_sf = 1;
//  const xAOD::Electron* el = dynamic_cast<const xAOD::Electrona*> (p);
//  if( m_Iso_SF->getEfficiencyScaleFactor(*el,m_sf) == CP::CorrectionCode::Error ){
//      Info( "GetElectronRecoSF()", "ElectronEfficiencyCorrection returns Error CorrectionCode");
//  }

//  return m_sf;

//} // end of GetElectronIsoSF
