#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>


MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
		ISvcLocator *pSvcLocator)
: EL::AnaAlgorithm (name, pSvcLocator)
	//    : m_grl ("GoodRunsListSelectionTool/grl", this)
{
	// get the output file, create a new TTree and connect it to that output
	// define what braches will go in that tree
	TH1::SetDefaultSumw2(kTRUE);
	declareProperty( "isMC", isMC = 0, "is simulation?" );
	declareProperty( "OldDeriv", OldDeriv = 0, "is new Derivation?" );
	declareProperty( "Year", Year=17  , "Year for MC or DATA?" );
	// Here you put any code for the base initialization of variables,
	// e.g. initialize all pointers to 0. Note that things like resetting
	// statistics variables should rather go into the initialize() function.
	//	declareProperty( "outputName", m_outputName,
	//			"Descriptive name for the processed sample" );

}

StatusCode MyxAODAnalysis :: fileExecute ()
{
	t_filename = wk()->inputFile()->GetName();
	bool isDuplicated = false;
	for(auto na : *vec_filename){
		if(t_filename==na){isDuplicated = true; break;}
	} 
	if(isDuplicated) return StatusCode::SUCCESS;

	// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisMetadata
	// get the MetaData tree once a new file is opened, with
	TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
	if (!MetaData) {
		Error("fileExecute()", "MetaData not found! Exiting.");
		return StatusCode::FAILURE;
	}
	MetaData->LoadTree(0);

	//check if file is from a DxAOD
	bool m_isDerivation = !MetaData->GetBranch("StreamAOD");

	if(m_isDerivation){
		const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
		if(!event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
			Error("fileExecute()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
			return StatusCode::FAILURE;
		}
		if ( incompleteCBC->size() != 0 ) {
			Warning("fileExecute()","Found incomplete Bookkeepers! Check file for corruption.");
			//       return StatusCode::FAILURE;
		}
		// Now, let's find the actual information
		const xAOD::CutBookkeeperContainer* completeCBC = 0;
		if(!event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
			Error("fileExecute()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
			return StatusCode::FAILURE;
		}

		// First, let's find the smallest cycle number,
		// i.e., the original first processing step/cycle
		int maxCycle = -1;
		const xAOD::CutBookkeeper* allEventsCBK=0;
		for ( auto cbk : *completeCBC ) {
			if(cbk->inputStream()=="StreamAOD" && cbk->name()=="AllExecutedEvents" && cbk->cycle()>maxCycle) { maxCycle=cbk->cycle(); allEventsCBK = cbk; }
		}
		t_nEventsProcessed  = allEventsCBK->nAcceptedEvents();
		t_sumOfWeights        = allEventsCBK->sumOfEventWeights();
		t_sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();

		vec_sumOfWeights -> push_back(t_sumOfWeights);
		vec_sumOfEvents -> push_back(t_nEventsProcessed);

	}
	else{
		vec_sumOfWeights -> push_back(0);
		vec_sumOfEvents -> push_back(0);
	}
	vec_filename -> push_back(t_filename);
	std::cout<<"bookkeeping "<<t_filename<<std::endl;

	return StatusCode::SUCCESS;
}
StatusCode MyxAODAnalysis :: initialize ()
{
	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected.


	//	h_event_cutflow = new TH1D("h_event_cutflow","h_event_cutflow",23,0,23);
	//	wk()->addOutput(h_event_cutflow);
	//	h_ele_cutflow = new TH1D("h_ele_cutflow","h_ele_cutflow",23,0,23);
	//	wk()->addOutput(h_ele_cutflow);

	//	m_bct = bct;
	//        TFile *outputFile = wk()->getOutputFile (m_outputName);
	//	ANA_MSG_INFO( "SampleName    = " << m_outputName );

	//	bct = new Trig::WebBunchCrossingTool("WebBunchCrossingTool");
	//	ANA_CHECK(bct->setProperty("OutputLevel", MSG::INFO));
	//	ANA_CHECK(bct->setProperty("ServerAddress", "atlas-trigconf.cern.ch"));
	//	m_bct = bct;
	//	ANA_CHECK(m_bct->initialize());

	vec_sumOfWeights = new std::vector<double>();
	vec_sumOfEvents = new std::vector<double>();
	vec_filename = new std::vector<std::string>();



	//===================GoodRunList===============
	m_grl = asg::AnaToolHandle<IGoodRunsListSelectionTool>("GoodRunsListSelectionTool/grl");
	std::vector<std::string> vecStringGRL;
	vecStringGRL.push_back(("GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml"));
	vecStringGRL.push_back(("GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"));
	vecStringGRL.push_back("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml");
	vecStringGRL.push_back("GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml");
	ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
	ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
	ANA_CHECK(m_grl.initialize());  


	//============pileup=============
	//uses the lumicalc files from the calibration file area!
	std::vector<std::string> listOfLumicalcFiles;
	if (Year ==16)
	{	
	listOfLumicalcFiles.push_back("GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root");
	listOfLumicalcFiles.push_back("GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root");
	}
	else if(Year ==17)
	{
	listOfLumicalcFiles.push_back("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root");
	}
	else if(Year ==18)
	{
	listOfLumicalcFiles.push_back("GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root");
	}
	std::vector<std::string> configFiles;
	if (Year ==17)
	{
	configFiles.push_back( ("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"));
	configFiles.push_back( ("dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.FS.v2/CI.prw.merged.root"));
	}
	else if (Year ==16)
	{
//	configFiles.push_back( PathResolverFindCalibFile("dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/2019-04-13T0604.prw.merged.root"));
	configFiles.push_back("dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/2019-04-13T0604.prw.merged.root");
	}
	else if (Year ==18)
	{
	configFiles.push_back( ("GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"));
	configFiles.push_back( ("dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16e.FS.v2/prw.merged.root"));
	}

	std::cout<<listOfLumicalcFiles.at(0)<<std::endl;
	pileuptool = asg::AnaToolHandle<CP::IPileupReweightingTool>("CP::PileupReweightingTool/pileuptool");
	ANA_CHECK(pileuptool.setProperty("LumiCalcFiles",listOfLumicalcFiles));
	ANA_CHECK(pileuptool.setProperty("ConfigFiles",configFiles));
//	if (Year == 17)
//	{
//	ANA_CHECK(pileuptool.setProperty("DataScaleFactor",1/1.03));
//	}
//	if (Year == 16)
//	{
//	ANA_CHECK(pileuptool.setProperty("DataScaleFactor",1/1.09));
//	}




	//       mcInfo = new LFVUtils::MCSampleInfo(EcmEnergy::LHCEnergy::ThirteenTeV);
	//       m_mcOver = new LFVUtils::MCSampleOverlap();

	//==============Trigger============
	m_trigDecisionTool = asg::AnaToolHandle<Trig::TrigDecisionTool>("Trig::TrigDecisionTool/TrigDecisionTool");
	m_trigConfigTool = asg::AnaToolHandle<TrigConf::ITrigConfigTool>("TrigConf::xAODConfigTool/xAODConfigTool");
	ANA_CHECK (m_trigConfigTool.initialize());
	m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle());
	m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision");
	ANA_CHECK (m_trigDecisionTool.initialize());


	//===============Trigger Match================
	m_trigMatch = asg::AnaToolHandle<Trig::IMatchingTool>( "Trig::MatchingTool/MatchingTool" );
	//	ANA_CHECK(m_trigMatch.setProperty("TriggerTool",m_trigDec));
	m_trigMatch.setProperty("TrigDecisionTool",m_trigDecisionTool.getHandle());
	ANA_CHECK(m_trigMatch.initialize());

	//================isolation===================
	isoGradient_ele = new CP::IsolationSelectionTool ( "isoGradient_ele" );
	ANA_CHECK(isoGradient_ele->setProperty("ElectronWP","Gradient") );
	ANA_CHECK(isoGradient_ele->initialize());

	isoLoose_ele = new CP::IsolationSelectionTool ( "isoLoose_ele" );
	ANA_CHECK(isoLoose_ele->setProperty("ElectronWP","Loose") );
	ANA_CHECK(isoLoose_ele->initialize());

	isoTrack_ele = new CP::IsolationSelectionTool ( "isoTrack_ele" );
	ANA_CHECK(isoTrack_ele->setProperty("ElectronWP","LooseTrackOnly") );
	ANA_CHECK(isoTrack_ele->initialize());

	isoHighpt_ele = new CP::IsolationSelectionTool ( "isoHighpt_ele" );
	ANA_CHECK(isoHighpt_ele->setProperty("ElectronWP","FixedCutHighPtCaloOnly") );
	ANA_CHECK(isoHighpt_ele->initialize());

	isoFCTight_ele = new CP::IsolationSelectionTool ( "isoFCTight_ele" );
	ANA_CHECK(isoFCTight_ele->setProperty("ElectronWP","FCTight") );
	ANA_CHECK(isoFCTight_ele->initialize());

	isoFCLoose_ele = new CP::IsolationSelectionTool ( "isoFCLoose_ele" );
	ANA_CHECK(isoFCLoose_ele->setProperty("ElectronWP","FCLoose") );
	ANA_CHECK(isoFCLoose_ele->initialize());

	isoPFTight_ele = new CP::IsolationSelectionTool ( "isoPFTight_ele" );
	ANA_CHECK(isoPFTight_ele->setProperty("ElectronWP","FixedCutPflowTight") );
	ANA_CHECK(isoPFTight_ele->initialize());

	isoPFLoose_ele = new CP::IsolationSelectionTool ( "isoPFLoose_ele" );
	ANA_CHECK(isoPFLoose_ele->setProperty("ElectronWP","FixedCutPflowLoose") );
	ANA_CHECK(isoPFLoose_ele->initialize());

	isoTrack_mu = new CP::IsolationSelectionTool ( "isoTrack_mu" );
	ANA_CHECK(isoTrack_mu->setProperty("MuonWP","LooseTrackOnly") );
	ANA_CHECK(isoTrack_mu->initialize());

	isoHighpt_mu = new CP::IsolationSelectionTool ( "isoHighpt_mu" );
	ANA_CHECK(isoHighpt_mu->setProperty("MuonWP","FixedCutHighPtTrackOnly") );
	ANA_CHECK(isoHighpt_mu->initialize());

	isoFCTight_mu = new CP::IsolationSelectionTool ( "isoFCTight_mu" );
	ANA_CHECK(isoFCTight_mu->setProperty("MuonWP","FCTight") );
	ANA_CHECK(isoFCTight_mu->initialize());

	isoFCLoose_mu = new CP::IsolationSelectionTool ( "isoFCLoose_mu" );
	ANA_CHECK(isoFCLoose_mu->setProperty("MuonWP","FCLoose") );
	ANA_CHECK(isoFCLoose_mu->initialize());

	isoPFTight_mu = new CP::IsolationSelectionTool ( "isoPFTight_mu" );
	ANA_CHECK(isoPFTight_mu->setProperty("MuonWP","FixedCutPflowTight") );
	ANA_CHECK(isoPFTight_mu->initialize());

	isoPFLoose_mu = new CP::IsolationSelectionTool ( "isoPFLoose_mu" );
	ANA_CHECK(isoPFLoose_mu->setProperty("MuonWP","FixedCutPflowLoose") );
	ANA_CHECK(isoPFLoose_mu->initialize());

	//===isolation correction====
	m_isoCorrTool = new CP::IsolationCorrectionTool("isoCorrTool");	
//	ANA_CHECK( asg::setProperty( m_isoCorrTool,  "CorrFile_ddshift", "IsolationCorrections/v3/isolation_ddcorrection_shift.root") ); 
	ANA_CHECK( asg::setProperty( m_isoCorrTool,  "IsMC", isMC) ); 
	//	ANA_CHECK( asg::setProperty( m_isoCorrTool,  "AFII_corr", true ) ); //true for fastsim, false for default fullsim
	ANA_CHECK( m_isoCorrTool->initialize() );


	//=============electron calibration==========
	m_elcali = asg::AnaToolHandle<CP::EgammaCalibrationAndSmearingTool>("CP::EgammaCalibrationAndSmearingTool/m_elcali");
	ANA_CHECK(m_elcali.setProperty("ESModel", "es2018_R21_v0"));
	ANA_CHECK(m_elcali.setProperty("decorrelationModel", "1NP_v1"));
//	ANA_CHECK(m_elcali.setProperty("randomRunNumber", "123"));


	//=============muon Selection==========
	m_muonSelectionHighPt = asg::AnaToolHandle<CP::IMuonSelectionTool>("CP::MuonSelectionTool/m_muonSelectionHighPt");
	ANA_CHECK (m_muonSelectionHighPt.setProperty("MaxEta", 2.5));
	ANA_CHECK (m_muonSelectionHighPt.setProperty("MuQuality", 4));
	ANA_CHECK (m_muonSelectionHighPt.initialize());


	//=============electron ID==========
	m_MediumLH = new AsgElectronLikelihoodTool("AsgElectronLikelihoodToolMedium");
	m_MediumLH->setProperty("primaryVertexContainer","PrimaryVertices" );
	ANA_CHECK(m_MediumLH->setProperty("WorkingPoint", "MediumLHElectron"));
	ANA_CHECK(m_MediumLH->initialize());


	m_LooseLH = new AsgElectronLikelihoodTool("AsgElectronLikelihoodToolLoose");
	m_LooseLH->setProperty("primaryVertexContainer","PrimaryVertices" );
	//	ANA_CHECK(m_LooseLH->setProperty("ConfigFile", "ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodLooseOfflineConfig2017_Smooth.conf"));
	ANA_CHECK(m_LooseLH->setProperty("WorkingPoint", "LooseLHElectron"));
	ANA_CHECK(m_LooseLH->initialize());

	m_TightLH = new AsgElectronLikelihoodTool("AsgElectronLikelihoodToolTight");
	m_TightLH->setProperty("primaryVertexContainer","PrimaryVertices" );
	m_TightLH->setProperty("WorkingPoint", "TightLHElectron" );
	ANA_CHECK(m_TightLH->initialize());


	//==============Efficiency=============
	m_Iso_SF = new AsgElectronEfficiencyCorrectionTool("Iso_SF");
	std::vector<std::string> inputFilesIso{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/isolation/efficiencySF.Isolation.MediumLLH_d0z0_v13_isolGradient.root"};
	ANA_CHECK(m_Iso_SF->setProperty("CorrectionFileNameList",inputFilesIso));
	ANA_CHECK(m_Iso_SF->setProperty("ForceDataType",1));
	ANA_CHECK(m_Iso_SF->initialize());

	m_El_Iso_SF_Tight = new AsgElectronEfficiencyCorrectionTool("El_Iso_SF_Tight");
	std::vector<std::string> inputFilesIsoTight{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/isolation/efficiencySF.Isolation.TightLLH_d0z0_v13_FCTight.root"};
	ANA_CHECK(m_El_Iso_SF_Tight->setProperty("CorrectionFileNameList",inputFilesIsoTight));
	ANA_CHECK(m_El_Iso_SF_Tight->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_Iso_SF_Tight->setProperty("CorrelationModel","TOTAL"));
	ANA_CHECK(m_El_Iso_SF_Tight->initialize());

	m_El_Iso_SF_Medium = new AsgElectronEfficiencyCorrectionTool("El_Iso_SF_Medium");
	std::vector<std::string> inputFilesIsoMedium{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/isolation/efficiencySF.Isolation.MediumLLH_d0z0_v13_FCTight.root"};
	ANA_CHECK(m_El_Iso_SF_Medium->setProperty("CorrectionFileNameList",inputFilesIsoMedium));
	ANA_CHECK(m_El_Iso_SF_Medium->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_Iso_SF_Medium->setProperty("CorrelationModel","TOTAL"));
	ANA_CHECK(m_El_Iso_SF_Medium->initialize());

	m_ID_SF = new AsgElectronEfficiencyCorrectionTool("El_ID_SF");
	std::vector<std::string> inputFilesID{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/offline/efficiencySF.offline.MediumLLH_d0z0_v13.root"};
	ANA_CHECK(m_ID_SF->setProperty("CorrectionFileNameList",inputFilesID));
	ANA_CHECK(m_ID_SF->setProperty("ForceDataType",1));
	ANA_CHECK(m_ID_SF->initialize());

	m_El_ID_SF_Tight = new AsgElectronEfficiencyCorrectionTool("El_ID_SF_Tight");
	std::vector<std::string> inputFilesIDTight{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/offline/efficiencySF.offline.TightLLH_d0z0_v13.root"};
	ANA_CHECK(m_El_ID_SF_Tight->setProperty("CorrectionFileNameList",inputFilesIDTight));
	ANA_CHECK(m_El_ID_SF_Tight->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_ID_SF_Tight->setProperty("CorrelationModel","TOTAL"));
	ANA_CHECK(m_El_ID_SF_Tight->initialize());

	m_El_ID_SF_Medium = new AsgElectronEfficiencyCorrectionTool("El_ID_SF_Medium");
	std::vector<std::string> inputFilesIDMedium{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/offline/efficiencySF.offline.MediumLLH_d0z0_v13.root"};
	ANA_CHECK(m_El_ID_SF_Medium->setProperty("CorrectionFileNameList",inputFilesIDMedium));
	ANA_CHECK(m_El_ID_SF_Medium->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_ID_SF_Medium->setProperty("CorrelationModel","TOTAL"));
	ANA_CHECK(m_El_ID_SF_Medium->initialize());

	m_El_Reco_SF = new AsgElectronEfficiencyCorrectionTool("El_Reco_SF");
	std::vector<std::string> inputFilesReco{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/offline/efficiencySF.offline.RecoTrk.root"};
	ANA_CHECK(m_El_Reco_SF->setProperty("CorrectionFileNameList",inputFilesReco));
	ANA_CHECK(m_El_Reco_SF->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_Reco_SF->setProperty("CorrelationModel","TOTAL"));
	ANA_CHECK(m_El_Reco_SF->initialize());

	m_El_Trig_SF_Tight = new AsgElectronEfficiencyCorrectionTool("El_Trig_SF_Tight");
	std::vector<std::string> inputFilesTrigTight{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.TightLLH_d0z0_v13_isolFCTight.root"};
	ANA_CHECK(m_El_Trig_SF_Tight->setProperty("CorrectionFileNameList",inputFilesTrigTight));
	ANA_CHECK(m_El_Trig_SF_Tight->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_Trig_SF_Tight->setProperty("CorrelationModel","TOTAL"));
	ANA_CHECK(m_El_Trig_SF_Tight->initialize());

	m_El_Trig_SF_Medium = new AsgElectronEfficiencyCorrectionTool("El_Trig_SF_Medium");
	std::vector<std::string> inputFilesTrigMedium{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolFCTgiht.root"};
	ANA_CHECK(m_El_Trig_SF_Medium->setProperty("CorrectionFileNameList",inputFilesTrigMedium));
	ANA_CHECK(m_El_Trig_SF_Medium->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_Trig_SF_Medium->setProperty("CorrelationModel","TOTAL"));

	m_El_Trig_Eff_Tight = new AsgElectronEfficiencyCorrectionTool("El_Trig_Eff_Tgiht");
	std::vector<std::string> inputFilesTrigEffTight{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiency.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.TightLLH_d0z0_v13_isolFCTight.root"};
	ANA_CHECK(m_El_Trig_Eff_Tight->setProperty("CorrectionFileNameList",inputFilesTrigEffTight));
	ANA_CHECK(m_El_Trig_Eff_Tight->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_Trig_Eff_Tight->setProperty("CorrelationModel","TOTAL"));
	ANA_CHECK(m_El_Trig_Eff_Tight->initialize());

	m_El_Trig_Eff_Medium = new AsgElectronEfficiencyCorrectionTool("El_Trig_Eff_Medium");
	std::vector<std::string> inputFilesTrigEffMedium{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiency.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolFCTight.root"};
	ANA_CHECK(m_El_Trig_Eff_Medium->setProperty("CorrectionFileNameList",inputFilesTrigEffMedium));
	ANA_CHECK(m_El_Trig_Eff_Medium->setProperty("ForceDataType",1));
	ANA_CHECK(m_El_Trig_Eff_Medium->setProperty("CorrelationModel","TOTAL"));
	ANA_CHECK(m_El_Trig_Eff_Medium->initialize());


	m_Trig_SF15 = new AsgElectronEfficiencyCorrectionTool("Trig_SF15");
	std::vector<std::string> inputFilesTrig{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiencySF.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.MediumLLH_d0z0_v13_isolGradient.2015.root"};
	ANA_CHECK(m_Trig_SF15->setProperty("CorrectionFileNameList",inputFilesTrig));
	ANA_CHECK(m_Trig_SF15->setProperty("ForceDataType",1));
	ANA_CHECK(m_Trig_SF15->initialize());

	m_Trig_SF16 = new AsgElectronEfficiencyCorrectionTool("Trig_SF16");
	std::vector<std::string> inputFilesTrig16{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiencySF.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2016.root "};
	ANA_CHECK(m_Trig_SF16->setProperty("CorrectionFileNameList",inputFilesTrig16));
	ANA_CHECK(m_Trig_SF16->setProperty("ForceDataType",1));
	ANA_CHECK(m_Trig_SF16->initialize());

	m_Trig_Eff15 = new AsgElectronEfficiencyCorrectionTool("Trig_Eff15");
	std::vector<std::string> inputFilesTrigEff15{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiency.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.MediumLLH_d0z0_v13_isolGradient.2015.root"};
	ANA_CHECK(m_Trig_Eff15->setProperty("CorrectionFileNameList",inputFilesTrigEff15));
	ANA_CHECK(m_Trig_Eff15->setProperty("ForceDataType",1));
	ANA_CHECK(m_Trig_Eff15->initialize());


	m_Trig_Eff16 = new AsgElectronEfficiencyCorrectionTool("Trig_Eff16");
	std::vector<std::string> inputFilesTrigEff16{"ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/trigger/efficiency.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2016.root "};
	ANA_CHECK(m_Trig_Eff16->setProperty("CorrectionFileNameList",inputFilesTrigEff16));
	ANA_CHECK(m_Trig_Eff16->setProperty("ForceDataType",1));
	ANA_CHECK(m_Trig_Eff16->initialize());



        //Muon Efficiency
	m_Mu_Reco_SF_HighPt = new CP::MuonEfficiencyScaleFactors("MuonRecoSF");
	ANA_CHECK(m_Mu_Reco_SF_HighPt->setProperty( "WorkingPoint", "HighPt" ));
//	ANA_CHECK(m_Mu_Reco_SF_HighPt->setProperty( "CalibrationRelease", "180905_TriggerUpdate" ));
	ANA_CHECK(m_Mu_Reco_SF_HighPt->initialize());

	m_Mu_Iso_SF_HighPt = new CP::MuonEfficiencyScaleFactors("MuonIsoSF");
	ANA_CHECK(m_Mu_Iso_SF_HighPt->setProperty( "WorkingPoint", "FCLooseIso" ));
//	ANA_CHECK(m_Mu_Iso_SF_HighPt->setProperty( "CalibrationRelease", "180905_TriggerUpdate" ));
	ANA_CHECK(m_Mu_Iso_SF_HighPt->initialize());

//	m_Mu_Iso_SF_HighPt = new CP::MuonEfficiencyScaleFactors("MuonIsoSF");
//	ANA_CHECK(m_Mu_Iso_SF_HighPt->setProperty( "WorkingPoint", "FixedCutHighPtTrackOnlyIso" ));
////	ANA_CHECK(m_Mu_Iso_SF_HighPt->setProperty( "CalibrationRelease", "180905_TriggerUpdate" ));
//	ANA_CHECK(m_Mu_Iso_SF_HighPt->initialize());

	m_Mu_Trig_SF_HighPt = new CP::MuonTriggerScaleFactors("MuonTrigSF");
	ANA_CHECK(m_Mu_Trig_SF_HighPt->setProperty( "MuonQuality", "HighPt" ));
//	ANA_CHECK(m_Mu_Trig_SF_HighPt->setProperty( "CalibrationRelease", "180905_TriggerUpdate" ));
	ANA_CHECK(m_Mu_Trig_SF_HighPt->initialize());

	m_Mu_TTVA_SF_HighPt = new CP::MuonEfficiencyScaleFactors("MuonTTVASF");
	ANA_CHECK(m_Mu_TTVA_SF_HighPt->setProperty( "WorkingPoint", "TTVA" ));
//	ANA_CHECK(m_Mu_TTVA_SF_HighPt->setProperty( "CalibrationRelease", "180905_TriggerUpdate" ));
	ANA_CHECK(m_Mu_TTVA_SF_HighPt->initialize());



	//	m_ForwardMedium = new  AsgForwardElectronIsEMSelector("m_ForwardMedium");
	//	ANA_CHECK(m_ForwardMedium->setProperty("WorkingPoint", "MediumForwardElectron"));
	//	ANA_CHECK(m_ForwardMedium->initialize());
	//
	//
	//	m_ForwardLoose = new  AsgForwardElectronIsEMSelector("m_ForwardLoose");
	//	ANA_CHECK(m_ForwardLoose->setProperty("WorkingPoint", "LooseForwardElectron"));
	//	ANA_CHECK(m_ForwardLoose->initialize());

	//B-tagging
	btagtool = new BTaggingSelectionTool("BTaggingSelectionTool");
	btagtool->setProperty("MaxEta", 2.5);
	btagtool->setProperty("MinPt", 25000.);
	ANA_CHECK(btagtool->setProperty("JetAuthor", "AntiKt4EMPFlowJets"));
	ANA_CHECK(btagtool->setProperty("TaggerName", "MV2c10"));
	ANA_CHECK(btagtool->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root" ));
	//  btagtool->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2016-11-25_v1.root" );
	ANA_CHECK(btagtool->setProperty("OperatingPoint", "FixedCutBEff_77"));
	ANA_CHECK(btagtool->initialize() );

	btagefftool =  new BTaggingEfficiencyTool( "BTaggingEfficiency" );
        ANA_CHECK(btagefftool->setProperty( "TaggerName", "MV2c10" ));
        ANA_CHECK(btagefftool->setProperty( "OperatingPoint", "FixedCutBEff_77" ));
        ANA_CHECK(btagefftool->setProperty( "JetAuthor", "AntiKt4EMPFlowJets" ));
        ANA_CHECK(btagefftool->setProperty( "ScaleFactorFileName",  "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root" ));
//        ANA_CHECK(btagefftool->setProperty( "ConeFlavourLabel", true ));
	ANA_CHECK(btagefftool->initialize() );

	//==================Muon Calibration================
	m_muonCalibrationAndSmearingTool2015 = asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool>("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool2015");
	ANA_CHECK(m_muonCalibrationAndSmearingTool2015.setProperty("Year","Data15"));
	ANA_CHECK(m_muonCalibrationAndSmearingTool2015.initialize());

        m_muonCalibrationAndSmearingTool2016 = asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool>("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool2016");
        ANA_CHECK(m_muonCalibrationAndSmearingTool2016.setProperty("Year","Data16"));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2016.setProperty("doSagittaMCDistortion",false));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2016.setProperty("StatComb",false));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2016.setProperty("SagittaRelease","sagittaBiasDataAll_03_02_19_Data16"));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2016.setProperty("SagittaCorr",true));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2016.setProperty("SagittaCorrPhaseSpace",true));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2016.initialize());

        m_muonCalibrationAndSmearingTool2017 = asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool>("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool2017");
        ANA_CHECK(m_muonCalibrationAndSmearingTool2017.setProperty("Year","Data17"));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2017.setProperty("doSagittaMCDistortion",false));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2017.setProperty("StatComb",false));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2017.setProperty("SagittaRelease","sagittaBiasDataAll_03_02_19_Data17"));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2017.setProperty("SagittaCorr",true));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2017.setProperty("SagittaCorrPhaseSpace",true));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2017.initialize());

        m_muonCalibrationAndSmearingTool2018 = asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool>("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool2018");
        ANA_CHECK(m_muonCalibrationAndSmearingTool2018.setProperty("Year","Data18"));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2018.setProperty("doSagittaMCDistortion",false));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2018.setProperty("StatComb",false));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2018.setProperty("SagittaRelease","sagittaBiasDataAll_03_02_19_Data18"));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2018.setProperty("SagittaCorr",true));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2018.setProperty("SagittaCorrPhaseSpace",true));
        ANA_CHECK(m_muonCalibrationAndSmearingTool2018.initialize());


	//JetCleaning
	m_cleaningTool = asg::AnaToolHandle<IJetSelector>("JetCleaningTool/JetCleaning");
	m_cleaningTool.setProperty("CutLevel", "LooseBad");
	m_cleaningTool.setProperty("DoUgly", false);
	ANA_CHECK( m_cleaningTool.initialize() );

	//JetCalibration
	m_jetCalibration = asg::AnaToolHandle<IJetCalibrationTool>("JetCalibrationTool/JetCalibTool");
	m_jetCalibration.setProperty("JetCollection","AntiKt4EMPFlow");
	m_jetCalibration.setProperty("ConfigFile","JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config");
	m_jetCalibration.setProperty("CalibArea","00-04-82");

	if(!isMC)
	{
		m_jetCalibration.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Insitu");
	}
	else
	{
		m_jetCalibration.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear");
	}

	m_jetCalibration.setProperty("IsData",!isMC);
	ANA_CHECK( m_jetCalibration.retrieve() );

	//ForwardJVT
	//	m_jetFrwJvtTool.setTypeAndName("JetForwardJvtTool/fJvt");
//	m_jetFrwJvtTool = asg::AnaToolHandle<IJetModifier>("JetForwardJvtTool/fJvt");
        m_jetFrwJvtTool= new JetForwardJvtTool( "JetForwardJvtTool" );
//	ANA_CHECK(m_jetFrwJvtTool->setProperty( "CentralMaxPt",60e3 ));
	ANA_CHECK(m_jetFrwJvtTool->initialize());

        //JVT tag
        m_pjvtag = asg::AnaToolHandle<IJetUpdateJvt> ("JetVertexTaggerTool/UpdateJVT");

	//	CP::JetJvtEfficiency *jvtsf = new CP::JetJvtEfficiency("jvtsf");
	//	ANA_CHECK(jvtsf->setProperty("WorkingPoint","Medium"));
	//	ANA_CHECK(jvtsf->setProperty("SFFile","JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root"));
	//	ANA_CHECK(jvtsf->initialize());
        
	//JVT effciency
	m_jvtsf = asg::AnaToolHandle<CP::IJetJvtEfficiency>("CP::JetJvtEfficiency/jetsf");
	ANA_CHECK(m_jvtsf.setProperty("WorkingPoint","Tight"));
	ANA_CHECK(m_jvtsf.setProperty("SFFile","JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlow.root"));
	ANA_CHECK(m_jvtsf.initialize());


	//==========================Tau Smearing=========================
	TauSmeTool = asg::AnaToolHandle<TauAnalysisTools::ITauSmearingTool>( "TauAnalysisTools::TauSmearingTool/TauSmeTool" );
	ANA_CHECK(TauSmeTool.setProperty( "ApplyMVATES", true ));
	ANA_CHECK(TauSmeTool.initialize());

	//Tau Selection

	//  TauSelTool = new TauAnalysisTools::TauSelectionTool( "TauSelectionTool" );
	//    TauSelTool->msg().setLevel( MSG::DEBUG );
	//      ANA_CHECK(TauSelTool->setProperty("CreateControlPlots", false ));
	//    TauSelTool->setProperty( "ConfigPath", PathResolverFindCalibFile("MyAnalysis/TauSelConfLoose.conf" ));
	////	ANA_CHECK(TauSelTool->setProperty("JetIDWP", int(TauAnalysisTools::JETIDBDTLOOSE)));
	////	ANA_CHECK(TauSelTool->setProperty("EleOLR",0));
	//
	//	  ANA_CHECK(TauSelTool->initialize());
	//


	TauSelTool = asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool>( "TauAnalysisTools::TauSelectionTool/TauSelTool" );
	ANA_CHECK(TauSelTool.setProperty( "ConfigPath", PathResolverFindCalibFile("MyAnalysis/TauElORLoose.conf" )));
	//	ANA_CHECK(TauSelTool->msg().setLevel( MSG::INFO ));
	ANA_CHECK(TauSelTool.initialize());

	TauSelTool_NoOLR = asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool>( "TauAnalysisTools::TauSelectionTool/TauSelTool_NoOLR" );
	ANA_CHECK(TauSelTool_NoOLR.setProperty( "ConfigPath", PathResolverFindCalibFile("MyAnalysis/TauSelConfLoose.conf" )));
	ANA_CHECK(TauSelTool_NoOLR.initialize());
	//	TauSelTool = asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool>( "TauAnalysisTools::TauSelectionTool/TauSelTool" );

	//Tau truth matching
	T2MT = asg::AnaToolHandle<TauAnalysisTools::ITauTruthMatchingTool>( "TauAnalysisTools::TauTruthMatchingTool/T2MT" );
	ANA_CHECK(T2MT.setProperty("WriteTruthTaus", true));
	ANA_CHECK(T2MT.initialize());

        //Tau Efficiency
	m_Tau_Reco_SF = asg::AnaToolHandle<TauAnalysisTools::ITauEfficiencyCorrectionsTool>("TauAnalysisTools::TauEfficiencyCorrectionsTool/TauReco");
	ANA_CHECK(m_Tau_Reco_SF.setProperty("EfficiencyCorrectionTypes",std::vector<int>({TauAnalysisTools::EfficiencyCorrectionType::SFRecoHadTau}) ) );
	ANA_CHECK(m_Tau_Reco_SF.setProperty("IDLevel", (int)TauAnalysisTools::e_JETID::JETIDBDTLOOSE ));
	ANA_CHECK(m_Tau_Reco_SF.setProperty("UseHighPtUncert", true ));
	ANA_CHECK(m_Tau_Reco_SF.initialize());

	m_Tau_ElOlr_SF = asg::AnaToolHandle<TauAnalysisTools::ITauEfficiencyCorrectionsTool>("TauAnalysisTools::TauEfficiencyCorrectionsTool/TauELO");
	ANA_CHECK(m_Tau_ElOlr_SF.setProperty( "EfficiencyCorrectionTypes", std::vector<int>({TauAnalysisTools::EfficiencyCorrectionType::SFEleOLRHadTau})) );
	ANA_CHECK(m_Tau_ElOlr_SF.setProperty("IDLevel", (int)TauAnalysisTools::e_JETID::JETIDBDTLOOSE ));
	ANA_CHECK(m_Tau_ElOlr_SF.setProperty("UseHighPtUncert", true ));
	ANA_CHECK(m_Tau_ElOlr_SF.initialize());

	m_Tau_JetID_SF = asg::AnaToolHandle<TauAnalysisTools::ITauEfficiencyCorrectionsTool>("TauAnalysisTools::TauEfficiencyCorrectionsTool/TauJetID");
	ANA_CHECK(m_Tau_JetID_SF.setProperty("EfficiencyCorrectionTypes",std::vector<int>({TauAnalysisTools::EfficiencyCorrectionType::SFJetIDHadTau}) ) );
	ANA_CHECK(m_Tau_JetID_SF.setProperty("IDLevel", (int)TauAnalysisTools::e_JETID::JETIDBDTLOOSE ));
	ANA_CHECK(m_Tau_JetID_SF.setProperty("UseHighPtUncert", true ));
	ANA_CHECK(m_Tau_JetID_SF.initialize());

	// MetMaker
	metSystTool = asg::AnaToolHandle<IMETSystematicsTool>("met::METSystematicsTool/metSystTool");
	metSystTool.retrieve();

	metMaker = asg::AnaToolHandle<IMETMaker>("met::METMaker/metMaker");
        ANA_CHECK( metMaker.setProperty("DoPFlow", true) );
//	ANA_CHECK( metMaker.setProperty("JetRejectionDec", "passFJVT") );
	ANA_CHECK( metMaker.retrieve() );


	//===============Overlap removal===================
	m_overlapRemovalTool = asg::AnaToolHandle<ORUtils::IOverlapRemovalTool>("ORUtils::OverlapRemovalTool/ORTool");
	m_overlapRemovalTool.setProperty("InputLabel", "isPreSel");
	m_overlapRemovalTool.setProperty("OutputLabel","isOverlap");
	m_overlapRemovalTool.retrieve();

	ORUtils::ORFlags orFlags("OverlapRemovalTool", "isPreSel", "isOverlap");
	orFlags.doElectrons = true;
	orFlags.doMuons     = true;
	orFlags.doJets      = true;
	orFlags.doTaus      = true;
	orFlags.doPhotons   = false;
	//	ORUtils::ToolBox toolBox;
	toolBox = new ORUtils::ToolBox();
       
	m_muTMstring = "HLT_mu50";
	ORUtils::recommendedTools(orFlags, *toolBox);
	 ANA_CHECK(toolBox->muJetORT.setProperty( "ApplyRelPt", true));
	ANA_CHECK(toolBox->initialize());
	//	m_overlapRemovalTool = std::move(toolBox->masterTool);


	m_mcInfo = new LFVUtils::MCSampleInfo(EcmEnergy::LHCEnergy::ThirteenTeV,0);
        m_mcOverlap = new LFVUtils::MCSampleOverlap(0);

  


	//========varible===============
	periodStart["2015ALL"] = 276262;
	periodStart["2016A"] = 296939;
	m_eventCounter = 0;
	m_numCleanEvents = 0;
	vw_triggers_elLFV[0] = "HLT_e60_lhmedium_nod0";
	vw_triggers_elLFV[1] = "HLT_e140_lhloose_nod0";
	vw_triggers_muLFV[0] = "HLT_mu50";
	vw_triggers_el15LFV[0] = "HLT_e60_lhmedium";
	vw_triggers_el15LFV[1] = "HLT_e120_lhloose";
	m_store = new xAOD::TStore();
	//  double PIE = 3.1415926;
	//  double Nummuon = 0;
	//  double GeV = 1000;
	init();
	return StatusCode::SUCCESS;

}



StatusCode MyxAODAnalysis :: execute ()
{
	//	xAOD::TEvent* event;
	event = wk()->xaodEvent();
	if (isMC)
	{	
	SelectedMuon    = new xAOD::MuonContainer;
	SelectedMuonAux = new xAOD::MuonAuxContainer;
	}
	fileExecute();
	//	std::unique_ptr<xAOD::TStore> store(new xAOD::TStore());

	if( (m_eventCounter % 1000) ==0 ) ANA_MSG_INFO("Event number = "<< m_eventCounter );
	m_eventCounter++;
	//    std::unique_ptr<xAOD::TEvent> eventD(new xAOD::TEvent( xAOD::TEvent::kClassAccess ) );


	ANA_CHECK(evtStore()->retrieve (eventInfo, "EventInfo")); 

	ANA_CHECK(evtStore()->retrieve (electrons, "Electrons"));

	ANA_CHECK (evtStore()->retrieve (muons, "Muons"));

	ANA_CHECK(evtStore()->retrieve (jets, "AntiKt4EMPFlowJets"));

	ANA_CHECK(evtStore()->retrieve( taus, "TauJets" ));

	ANA_CHECK(evtStore()->retrieve( photons, "Photons" ));

	ANA_CHECK(evtStore()->retrieve( vxContainer, "PrimaryVertices"));

	if(isMC)
	{
	 ANA_CHECK(evtStore()->retrieve (m_thjetCont, "AntiKt4TruthJets"));
	 ANA_CHECK( event->retrieve( xTruthParticleContainer, "TruthParticles" ) );
	}

	if(!isMC)
		runNumber = eventInfo->runNumber();
	else
	{
	pileuptool->apply( *eventInfo, true);
	runNumber = eventInfo->auxdecor<unsigned int>("RandomRunNumber");
	}


	//  std::unique_ptr<xAOD::TStore> store(new xAOD::TStore());
	loop();
	if (isMC)
	{	
	delete SelectedMuon;
	delete SelectedMuonAux;
	}
	return StatusCode::SUCCESS;
}

double MyxAODAnalysis :: GetKfactor(){

  double m_kfact = 1;
  if(isMC){
    if( (eventInfo->mcChannelNumber()>=301560 && eventInfo->mcChannelNumber()<=301579) ||
        (eventInfo->mcChannelNumber()>=301540 && eventInfo->mcChannelNumber()<=301559) ||
        (eventInfo->mcChannelNumber()>=303437 && eventInfo->mcChannelNumber()<=303455) ||
        (eventInfo->mcChannelNumber()>=301000 && eventInfo->mcChannelNumber()<=301018) ||
        (eventInfo->mcChannelNumber()>=301020 && eventInfo->mcChannelNumber()<=301038) ||
        (eventInfo->mcChannelNumber()>=301040 && eventInfo->mcChannelNumber()<=301058) ||
        (eventInfo->mcChannelNumber()>=361106 && eventInfo->mcChannelNumber()<=361108)  ){
      m_kfact = m_mcInfo->GetDYkFactor( eventInfo->mcChannelNumber() , xTruthParticleContainer);
    }
    if( (eventInfo->mcChannelNumber()>=402970 && eventInfo->mcChannelNumber()<=403044) ){
      m_kfact = m_mcInfo->GetRPVkFactor( eventInfo->mcChannelNumber() );
    }
    if( (eventInfo->mcChannelNumber()>=301954 && eventInfo->mcChannelNumber()<=302028) ){
      m_kfact = m_mcInfo->GetZPRIMEkFactor( eventInfo->mcChannelNumber(), xTruthParticleContainer);
    }
//    if( m_debug ) Info( "GetKfactor()", "sample = %i , kfact = %f ", eventInfo->mcChannelNumber(), m_kfact );
  }
//  else
//    if( m_debug ) Info( "GetKfactor()", "Running on Data, kfact value = 1 !!" );
  return m_kfact;

}





void MyxAODAnalysis :: electronIdentificationDecor(xAOD::Electron *el)
{
//	if (OldDeriv)
//	{	
	(el)->auxdata<char>("LHLoose") = false;
	(el)->auxdata<char>("LHMedium") = false;
	(el)->auxdata<char>("LHTight") = false;
	if ((el)->auxdata<char>("DFCommonElectronsLHTight")) (el)->auxdata<char>("LHTight") =true;
	if ((el)->auxdata<char>("DFCommonElectronsLHMedium")) (el)->auxdata<char>("LHMedium") =true;
	if ((el)->auxdata<char>("DFCommonElectronsLHLoose")) (el)->auxdata<char>("LHLoose") =true;
//	}
	if (isMC)
	{
	double Reco_SF = 1.;
	m_El_Reco_SF->getEfficiencyScaleFactor(*el,Reco_SF);
	(el)->auxdata<double>("RecoWeight") = Reco_SF;
	double ID_SF_Tight = 1.;
	m_El_ID_SF_Tight->getEfficiencyScaleFactor(*el,ID_SF_Tight);
	(el)->auxdata<double>("IDWeightTight") = ID_SF_Tight;
	double ID_SF_Medium = 1.;
	m_El_ID_SF_Medium->getEfficiencyScaleFactor(*el,ID_SF_Medium);
	(el)->auxdata<double>("IDWeightMedium") = ID_SF_Medium;
	}

}

void MyxAODAnalysis :: electronIsolationDecor(xAOD::Electron *el)
{
	(el)->auxdata<char>("IsoGradientEle") = false;
	(el)->auxdata<char>("IsoLooseEle") = false;
	(el)->auxdata<char>("IsoTrackEle") = false;
	(el)->auxdata<char>("IsoHighptEle") = false;
	(el)->auxdata<char>("IsoFCTightEle") = false;
	(el)->auxdata<char>("IsoFCLooseEle") = false;
//	(el)->auxdata<char>("IsoPFTightEle") = false;
//	(el)->auxdata<char>("IsoPFLooseEle") = false;
	if (isoGradient_ele->accept(*el)) (el)->auxdata<char>("IsoGradientEle") =true;
	if (isoLoose_ele->accept(*el)) (el)->auxdata<char>("IsoLooseEle") =true;
	if (isoTrack_ele->accept(*el)) (el)->auxdata<char>("IsoTrackEle") =true;
	if (isoHighpt_ele->accept(*el)) (el)->auxdata<char>("IsoHighptEle") =true;
	if (isoFCTight_ele->accept(*el)) (el)->auxdata<char>("IsoFCTightEle") =true;
	if (isoFCLoose_ele->accept(*el)) (el)->auxdata<char>("IsoFCLooseEle") =true;
//	if (isoPFTight_ele->accept(*el)) (el)->auxdata<char>("IsoPFTightEle") =true;
//	if (isoPFLoose_ele->accept(*el)) (el)->auxdata<char>("IsoPFLooseEle") =true;

	if(isMC)
	{
	double Iso_Tight_SF = 1.;
	double Iso_Medium_SF = 1.;
	m_El_Iso_SF_Tight->getEfficiencyScaleFactor(*el,Iso_Tight_SF);
	(el)->auxdata<double>("IsoWeightTight") = Iso_Tight_SF;
	m_El_Iso_SF_Medium->getEfficiencyScaleFactor(*el,Iso_Medium_SF);
	(el)->auxdata<double>("IsoWeightMedium") = Iso_Medium_SF;
	}

}


void MyxAODAnalysis :: muonIdentificationDecor(xAOD::Muon *mu)
{
	(mu)->auxdata<char>("IDHighptMu") = false;
	(mu)->auxdata<char>("IDBadMu") = false;
	if(m_muonSelectionHighPt->accept(*mu)) (mu)->auxdata<char>("IDHighptMu") = true;
	if(m_muonSelectionHighPt->isBadMuon(*mu)) (mu)->auxdata<char>("IDBadMu") = true;

	if(isMC)
	{
         float Reco_SF_Highpt = 1.;
	 float TTVA_SF_Highpt = 1.;
	 m_Mu_Reco_SF_HighPt->getEfficiencyScaleFactor(*mu,Reco_SF_Highpt);
	 m_Mu_TTVA_SF_HighPt->getEfficiencyScaleFactor(*mu,TTVA_SF_Highpt);
	 (mu)->auxdata<double>("RecoWeightHighpt") = (double)Reco_SF_Highpt;
	 (mu)->auxdata<double>("TTVAWeightHighpt") = (double)TTVA_SF_Highpt;
	}

}


void MyxAODAnalysis :: muonIsolationDecor(xAOD::Muon *mu)
{

	(mu)->auxdata<char>("IsoTrackMu") = false;
	(mu)->auxdata<char>("IsoHighptMu") = false;
	(mu)->auxdata<char>("IsoFCTightMu") = false;
	(mu)->auxdata<char>("IsoFCLooseMu") = false;
	(mu)->auxdata<char>("IsoPFTightMu") = false;
	(mu)->auxdata<char>("IsoPFLooseMu") = false;
	if (isoTrack_mu->accept(*mu)) (mu)->auxdata<char>("IsoTrackMu") =true;
	if (isoHighpt_mu->accept(*mu)) (mu)->auxdata<char>("IsoHighptMu") =true;
	if (isoFCTight_mu->accept(*mu)) (mu)->auxdata<char>("IsoFCTightMu") =true;
	if (isoFCLoose_mu->accept(*mu)) (mu)->auxdata<char>("IsoFCLooseMu") =true;
	if (isoPFTight_mu->accept(*mu)) (mu)->auxdata<char>("IsoPFTightMu") =true;
	if (isoPFLoose_mu->accept(*mu)) (mu)->auxdata<char>("IsoPFLooseMu") =true;

	if (isMC)
	{
	float Iso_Tight_SF = 1.;
	m_Mu_Iso_SF_HighPt->getEfficiencyScaleFactor(*mu,Iso_Tight_SF);
	(mu)->auxdata<double>("IsoWeightHighpt") = (double)Iso_Tight_SF;
	}
	
}
//void MyxAODAnalysis :: electronTriggerDecor(xAOD::Electron *el)


void MyxAODAnalysis :: electronTriggerDecor(xAOD::Electron *el, string *arr, int size){
	//    (el)->auxdata<std::string>("matchedTrigger") = "None";
	myParticles.clear();
	myParticles.push_back( el );
	bool trigMatched = false;
	for(int ii = 0; ii<size;ii++){
		trigMatched = trigMatched || (m_trigMatch->match(myParticles,arr[ii].c_str(),0.07));
		//      if(m_trigMatch->match(myParticles,vw_triggers_el[ii].c_str(),0.07)) (el)->auxdata<std::string>("matchedTrigger") = (el)->auxdata<std::string>("matchedTrigger") + "_AND_" + vw_triggers_el[ii];
	}
	(el)->auxdata<char>("triggerMatched") = trigMatched;
        if (isMC)
	{
	 double Trig_Tight_SF = 1.;
	 m_El_Trig_SF_Tight->getEfficiencyScaleFactor(*el,Trig_Tight_SF);
	 (el)->auxdata<double>("TrigWeightTight") = Trig_Tight_SF;

	 double Trig_Medium_SF = 1.;
	 m_El_Trig_SF_Medium->getEfficiencyScaleFactor(*el,Trig_Medium_SF);
	 (el)->auxdata<double>("TrigWeightMedium") = Trig_Medium_SF;

	 double Trig_Tight_Eff = 1.;
	 m_El_Trig_Eff_Tight->getEfficiencyScaleFactor(*el,Trig_Tight_Eff);
	 (el)->auxdata<double>("TrigEffWeightTight") = Trig_Tight_Eff;

	 double Trig_Medium_Eff = 1.;
	 m_El_Trig_Eff_Medium->getEfficiencyScaleFactor(*el,Trig_Medium_Eff);
	 (el)->auxdata<double>("TrigEffWeightMedium") = Trig_Medium_Eff;
	}
}


void MyxAODAnalysis :: muonTriggerDecor(xAOD::Muon *mu, string *arr, int size){
	myParticles.clear();
	myParticles.push_back( mu );
	bool trigMatched = false;
	for(int ii = 0; ii<size;ii++)
	{
		trigMatched = trigMatched || (m_trigMatch->match(myParticles,arr[ii].c_str(),0.1));           }
	(mu)->auxdata<char>("triggerMatched") = trigMatched;



}

double MyxAODAnalysis :: GetMuonTriggerSF(const xAOD::IParticle *p ){
	double m_sf = 1;
	const xAOD::Muon* mu = dynamic_cast<const xAOD::Muon*> (p);
        xAOD::Muon* m_newMuon;

        m_newMuon = new xAOD::Muon;

        m_newMuon->makePrivateStore(*mu);
        SelectedMuon->setStore(SelectedMuonAux);
        SelectedMuon->push_back(m_newMuon);
        if( m_Mu_Trig_SF_HighPt->getTriggerScaleFactor(*SelectedMuon,m_sf,m_muTMstring) == CP::CorrectionCode::Error ){
         Info( "GetMuonTriggerSF()", "MuonTriggerScaleFactors returns Error CorrectionCode in getting SF");
        } // end of if statement

	m_newMuon = 0;
        SelectedMuon->clear();
        return m_sf;
	delete m_newMuon;
}

double  MyxAODAnalysis :: GetElectronL1CaloSF(const xAOD::IParticle *p){
      double m_sf = 1.0;
      const xAOD::Electron* el = dynamic_cast<const xAOD::Electron*> (p);
      float EMthresh[50] = {2008.96,1777.98,2053.32,1643.75,2016.56,1930.7,2485.62,2738.57,2712.91,1936.1,
                4152.37,1825.52,1783.29,2188.33,2520.03,2545.74,2947.04,3086.35,4090.8,3371.26,
                3773.61,4203.79,4813.52,5420.25,4753.5,4923.54,4338.46,4210.73,4182.22,3626.3,
                3460.46,3326.42,2916.75,2317.44,2443.85,2144.06,2026.63,1828.14,1685.24,4284.14,
                2063.57,2656.55,2617.98,2300.39,2029.07,2053.82,1903.34,2280.31,2139.5,2106.39};
      
      float EMsigma[50] = {488.849,477.262,692.451,667.071,709.393,535.159,807.23,868.081,514.064,568.739,
               2680.53,570.615,646.978,346.33,643.672,458.567,551.28,804.826,808.753,1155.14,
               1198.67,1388.94,1304.77,1079.12,1658.89,1596.59,1538.42,1526.29,1544.7,1114.52,
               1266.86,1225.65,979.65,657.256,517.2,482.163,537.074,458.072,578.225,2583.39,
               788.964,625.561,810.542,762.759,632.171,617.855,684.52,618.986,600.584,551.851};
      float pt = el->pt()/GeV;
      float eta = el->eta();
      int keta = eta*10 + 25;
      float sigma = sqrt( pt*(EMsigma[keta]/EMthresh[keta])*EMsigma[keta]);
      float thresh = EMthresh[keta];
      float wt =  erfc( (thresh - pt)/(sqrt(2.)*sigma) )/2.0 ; 
      m_sf = 1-wt;
  return m_sf;


}




StatusCode MyxAODAnalysis :: finalize ()
{
	// This method is the mirror image of initialize(), meaning it gets
	// called after the last event has been processed on the worker node
	// and allows you to finish up any objects you created in
	// initialize() before they are written to disk.  This is actually
	// fairly rare, since this happens separately for each worker node.
	// Most of the time you want to do your post-processing on the
	// submission node after all your histogram outputs have been
	// merged.
	finish();

	if (isoGradient_ele) delete isoGradient_ele;
	if (isoLoose_ele) delete isoLoose_ele;
	if (isoTrack_ele) delete isoTrack_ele;
	if (isoHighpt_ele) delete isoHighpt_ele;
	if (isoFCTight_ele) delete isoFCTight_ele;
	if (isoFCLoose_ele) delete isoFCLoose_ele;
	if (isoPFTight_ele) delete isoPFTight_ele;
	if (isoPFLoose_ele) delete isoPFLoose_ele;
	if (isoTrack_mu) delete isoTrack_mu;
	if (isoHighpt_mu) delete isoHighpt_mu;
	if (isoFCTight_mu) delete isoFCTight_mu;
	if (isoFCLoose_mu) delete isoFCLoose_mu;
	if (isoPFTight_mu) delete isoPFTight_mu;
	if (isoPFLoose_mu) delete isoPFLoose_mu;
	if (m_isoCorrTool) delete m_isoCorrTool;
	if (m_MediumLH) delete m_MediumLH;
	if (m_LooseLH) delete m_LooseLH;
	if (m_TightLH) delete m_TightLH;
	if (m_Iso_SF) delete m_Iso_SF;
	if (m_El_Iso_SF_Tight) delete m_El_Iso_SF_Tight;
	if (m_El_Iso_SF_Medium) delete m_El_Iso_SF_Medium;
	if (m_ID_SF) delete m_ID_SF;
	if (m_El_ID_SF_Tight) delete m_El_ID_SF_Tight;
	if (m_El_ID_SF_Medium) delete m_El_ID_SF_Medium;
	if (m_El_Reco_SF) delete m_El_Reco_SF;
	if (m_El_Trig_SF_Tight) delete m_El_Trig_SF_Tight;
	if (m_El_Trig_SF_Medium) delete m_El_Trig_SF_Medium;
	if (m_El_Trig_Eff_Tight) delete m_El_Trig_Eff_Tight;
	if (m_El_Trig_Eff_Medium) delete m_El_Trig_Eff_Medium;
	if (m_Trig_SF15) delete m_Trig_SF15;
	if (m_Trig_SF16) delete m_Trig_SF16;
	if (m_Trig_Eff15) delete m_Trig_Eff15;
	if (m_Trig_Eff16) delete m_Trig_Eff16;
	if (m_Mu_Reco_SF_HighPt) delete m_Mu_Reco_SF_HighPt;
	if (m_Mu_Iso_SF_HighPt) delete m_Mu_Iso_SF_HighPt;
	if (m_Mu_Trig_SF_HighPt) delete m_Mu_Trig_SF_HighPt;
	if (m_Mu_TTVA_SF_HighPt) delete m_Mu_TTVA_SF_HighPt;
	if (btagtool) delete btagtool;
	if (btagefftool) delete btagefftool;
	if (m_jetFrwJvtTool) delete m_jetFrwJvtTool;
	if (toolBox) delete toolBox;
	if (m_mcInfo) delete m_mcInfo;
	if (m_mcOverlap) delete m_mcOverlap;
//        if (m_pjvtag) delete m_pjvtag;
        delete m_store;
	return StatusCode::SUCCESS;
}

//double MyxAODAnalysis :: GetElectronIsoSF_Tight(auto *p){
//  double m_sf = 1;
//  const xAOD::Electron* el = dynamic_cast<const xAOD::Electrona*> (p);
//  if( m_Iso_SF->getEfficiencyScaleFactor(*el,m_sf) == CP::CorrectionCode::Error ){
//      Info( "GetElectronRecoSF()", "ElectronEfficiencyCorrection returns Error CorrectionCode");
//  }

//  return m_sf;

//} // end of GetElectronIsoSF



//Pre-Selection

bool MyxAODAnalysis :: passGRL ()
{
	//  if(isMC) return true;
	if(m_grl->passRunLB(*eventInfo)) return true;
	else return false;
}


bool MyxAODAnalysis :: passVTX ()
{
	const xAOD::VertexContainer* vertex = 0;
	ANA_CHECK(evtStore()->retrieve(vertex, "PrimaryVertices"));
	bool primVtx = false;
	double nVtx= 0;
	for(unsigned int i=0;i<vertex->size();i++){
		const xAOD::Vertex* vxcand = vertex->at(i);
		if(vxcand->nTrackParticles()>2){
			primVtx = true;
			nVtx++;
		}
	}
	return primVtx;
}

bool MyxAODAnalysis :: JetEventCleaning ()
{
        if ( eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad")) return true;

//   cout<< eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad")<<endl;
        else return false;
}

bool MyxAODAnalysis :: passEventCleaning ()
{
	//  if(isMC) return true;
	if((eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error)||(eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error)||(eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))) return false;
	else return true;
}

//bool MyxAODAnalysis :: passTrigger (std::vector<std::string> vec_triggers)
bool MyxAODAnalysis :: passTrigger (string *arr,int length)
{
	bool TriggerDecision = false;

	for(int ii = 0; ii<length;ii++){
		TriggerDecision = TriggerDecision || m_trigDecisionTool->isPassed(arr[ii].c_str());
	}
	//    for(auto arg : vec_triggers){
	//	        TriggerDecision = TriggerDecision ||   m_trigDecisionTool->isPassed(arg.c_str());
	//		  }
	return TriggerDecision;
}

void MyxAODAnalysis ::prwWeight()
{
//	PRWwei = pileuptool->getCombinedWeight(*eventInfo);
	PRWwei = (double)eventInfo->auxdecor<float>("PileupWeight");
//	cout << PRWwei <<endl;
	Eventwei = eventInfo->mcEventWeights().at(0);
//	eventInfo->auxdecor<unsigned int>("RandomRunNumber") = pileuptool->getRandomRunNumber( *eventInfo, true );
}


void MyxAODAnalysis :: d0z0CAL(const xAOD::TrackParticle *trk)
{
	float delta_z0=0.0; bool z0_Pass = false;
	float d0_significance=0.0;
	for (unsigned int i=0; i<vxContainer->size(); i++){
		const xAOD::Vertex* vtx = vxContainer->at(i);
		if ( vtx->vertexType() == xAOD::VxType::PriVtx ){
			delta_z0 = fabs((trk->z0() + trk->vz() - vtx->z())*sin(trk->theta()));
			if( delta_z0 < 0.5 ) z0_Pass = true;
		}
	} // end of loop

	//        double d0 = trk->d0();
	//        double cov = trk->definingParametersCovMatrix()(0,0);
	//        double beam = eventInfo->beamPosSigmaX()*eventInfo->beamPosSigmaX() + eventInfo->beamPosSigmaY()*eventInfo->beamPosSigmaY();

	d0_significance =  xAOD::TrackingHelpers::d0significance( trk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
	d0cal = d0_significance;   
	z0cal = z0_Pass;   
}
