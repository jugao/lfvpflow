#ifndef MyAnalysis_LFVAnalysis_H
#define MyAnalysis_LFVAnalysis_H

#include "MyAnalysis/MyxAODAnalysis.h"

class LFVAnalysis : public MyxAODAnalysis
{
   public:
   string m_outputName;//!
   TTree* lfvTree; //!  
   TTree *metaTree; //!

//Meta
  string filename; //!
  double sumOfWeights; //!
  int nEventsProcessed;//!
//event Info
  double EventWeight;//!
  double EventWeight_NoTrigSF;//!
  double PrwWeight;//!
  double MCWeight;//!
  double totalEvents;//!
  bool Event_Trigger_Pass;//!
  bool Event_Trigger_Match;//!
  int eventNumber; //!
  int mcChannelNumber;//!
  int EventTag;//!
  int RunNo;//!
  double lumB;//!
  double xsec;//!
  double kFactor;//!

  double dPhi_ej;//!
  double dPhi_ll;//!
  double Final_elpt;//!
  double Final_eleta;//!
  double Final_elphi;//!
  double Final_mupt;//!
  double Final_mueta;//!
  double Final_muphi;//!
  double Final_tauphi;//!
  double Final_taupt;//!
  double Final_taueta;//!
  bool isKeepEvent; //!
  bool isEvtClean; //!
  bool isVertexGood; //!
  bool isTrigPass; //!
  int N_jets;//!
  int m_numCleanEvents;//!

//Missing ET Info
  double MET_SumEt; //!
  double MET_Py; //!
  double MET_Px; //!
  double MET_Phi; //!
  double MET_Et; //!
  double event_met; //!
  double event_met_eta; //!
  double event_met_phi; //!
  double event_met_m; //!
  double event_mT; //!


//Electron para
  std::vector<int>    Ele_Charge;       //!
  std::vector<double> Ele_pt;       //!
  std::vector<double> Ele_eta;      //!
  std::vector<double> Ele_eta_calo; //!
  std::vector<double> Ele_phi;      //!
  std::vector<double> Ele_d0sig;    //!
  std::vector<double> Ele_dz0;    //!
  std::vector<bool>   Ele_OQ;      //!
  std::vector<bool>   Ele_d0pass; //!
  std::vector<bool>   Ele_z0pass; //!
  std::vector<bool>   Ele_MuonOLR; //!
  std::vector<bool>   Ele_ID_Medium;      //!
  std::vector<bool>   Ele_ID_Tight;      //!
  std::vector<bool>   Ele_ID_Loose;      //!
  std::vector<bool>   Ele_Iso;      //!
  std::vector<bool>   Ele_Iso_Loose;      //!
  std::vector<bool>   Ele_isTrigMch; //!
  std::vector<double> Ele_TrigEff;   //!
  std::vector<double> Ele_RecoSF;   //!
  std::vector<double> Ele_IDSF;     //!
  std::vector<double> Ele_IsoSF;     //!
  std::vector<double> Ele_L1CaloSF;     //!
  std::vector<bool> Ele_isOverlap;     //!

//Muon Para
  std::vector<double> Mu_pt; //!
  std::vector<double> Mu_eta; //!
  std::vector<double> Mu_phi; //!
  std::vector<double> Mu_d0sig; //!
  std::vector<double> Mu_dz0; //!
  std::vector<int> Mu_charge; //!
  std::vector<bool> Mu_d0pass; //!
  std::vector<bool> Mu_z0pass; //!
  std::vector<bool> Mu_Iso; //!
  std::vector<bool> Mu_isHighPt;  //!
  std::vector<bool> Mu_isID;  //!
  std::vector<bool> Mu_isTrigMch;  //!
  std::vector<double> Mu_TrigSF;   //!
  std::vector<double> Mu_RecoSF;   //!
  std::vector<double> Mu_TrackSF;   //!
  std::vector<double> Mu_IsoSF;    //!
  std::vector<bool> Mu_isOverlap;     //!

//Tau Para
  std::vector<double> Tau_pt;     //!
  std::vector<double> Tau_eta;     //!
  std::vector<double> Tau_phi;     //!
  std::vector<int> Tau_charge;     //!
  std::vector<int> Tau_ntracks;     //!
//  std::vector<bool> Tau_MuonOLR;     //!
 // std::vector<bool> Tau_EleOLR;     //!
  std::vector<bool> Tau_isOverlap;     //!
  std::vector<bool> TauSelector;     //!
  std::vector<bool> TauSelector_WithOLR;     //!
  std::vector<double> Tau_RecoSF; //!
  std::vector<double> Tau_IDSF; //!
  std::vector<double> Tau_EleOLRSF; //!

//Jet Para
  std::vector<double> Jet_pt;     //!
  std::vector<double> Jet_eta;     //!
  std::vector<double> Jet_phi;     //!
  std::vector<double> Jet_m;     //!
  std::vector<bool> Jet_isGoodB;     //!
  std::vector<double> Jet_JVTSF; //!
  std::vector<double> Jet_BTInefSF; //!
  std::vector<double> Jet_BTSF; //!

//Selection Tag
  bool m_electronMuon; //!
  bool m_electronTau; //!
  bool m_muonTau; //!
  bool Ele_TriggerMatched;//!
  bool Mu_TriggerMatched;//!
  std::vector<bool> Ele_isTight;//!
  std::vector<bool> Ele_isLoose;//!
  std::vector<bool> Ele_isLoose_IDLoose;//!

//CutFlow
  TH1D *h_CleanEventNum; //!
  TH1D *h_CutFlow_emu; //!
  TH1D *h_CutFlow_etau; //!
  TH1D *h_CutFlow_mutau; //!
  TH1D *h_CutFlow_el; //!
  TH1D *h_CutFlow_muon; //!
  TH1D *h_CutFlow_tau; //!
  TH1D *h_CutFlow_jet; //!
  TH1D *h_CutFlow_ph; //!


  bool DoEleID = false;  //!
  bool DoEleIso = false;  //!
  bool DoMuIso = false;  //!



  const xAOD::TruthParticle* xTruthTau;//!
  xAOD::ElectronContainer* elsCorr = 0; //!
  xAOD::ElectronContainer* fwdelsCorr = 0; //!
  xAOD::MuonContainer* muonsCorr = 0; //!
  xAOD::TauJetContainer* tausCorr = 0; //!
  xAOD::JetContainer* jetsCorr = 0; //!
  xAOD::PhotonContainer* phsCorr = 0; //!

  xAOD::MuonContainer *m_MetCorrMuons;                          //!
  xAOD::ElectronContainer *m_MetCorrElectrons;                  //!
  xAOD::TauJetContainer *m_MetCorrTaus;                         //!
  xAOD::PhotonContainer *m_MetCorrPhotons;                      //!

  bool doMetCalculation; //!

  public:

//  SelectionWZ (const std::string& name, ISvcLocator* pSvcLocator);
   LFVAnalysis (const std::string& name, ISvcLocator* pSvcLocator); // constructor for release 21
   virtual bool init(void);
   virtual bool loop(void);
   virtual bool finish(void);

  ClassDef(LFVAnalysis, 1)

};

#endif
