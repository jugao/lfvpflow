#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <EventLoop/Algorithm.h>
#include "EventLoop/OutputStream.h"


//#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include <xAODEgamma/ElectronContainer.h>

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

// FrameWork includes
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticleHelpers.h"

// ROOT
#include <TROOT.h>
#include <TVector3.h>
#include <TFile.h>
#include <TVector3.h>

// System include(s):
#include <memory>
#include <cstdlib>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <utility>

/*xAOD includes*/
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/TrackParticle.h"
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoopAlgs/AlgSelect.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "PathResolver/PathResolver.h"
#include "PATInterfaces/CorrectionCode.h" // to check the return correction code status of tools
#include <xAODCore/AuxContainerBase.h>
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"

//cut book keeper
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"

//**************************General Tool************************

// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include "PATInterfaces/SystematicRegistry.h"

//Pileup
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"

//Isolation
#include "IsolationSelection/IsolationSelectionTool.h"
#include "IsolationCorrections/IsolationCorrectionTool.h"

//Electron
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"

//Muon
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>

//Photon ID
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "EgammaAnalysisHelpers/PhotonHelpers.h"


// EGammaAmbiguity
#include "ElectronPhotonSelectorTools/EGammaAmbiguityTool.h"
//#include "EgammaAnalysisHelpers/PhotonHelpers.h"
//#include "EgammaAnalysisHelpers/PhotonSelectorHelpers.h"

// Jet Combined Performance
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetResolution/JERTool.h"
#include "JetResolution/JERSmearingTool.h"
#include <JetResolution/IJERTool.h>
#include "JetUncertainties/JetUncertaintiesTool.h"
#include <JetInterface/IJetSelector.h>
#include <JetMomentTools/JetForwardJvtTool.h>
#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"



//Trigger
#include "TrigConfxAOD/xAODConfigTool.h"
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include "TriggerMatchingTool/MatchingTool.h"

//systematics
#include "PATInterfaces/ISystematicsTool.h"
#include "PATInterfaces/SystematicVariation.h" 
#include <PATInterfaces/SystematicsUtil.h>
#include "PATInterfaces/SystematicRegistry.h"

//truth info 
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"


//Electron Efficiency
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
//MuonEfficiencyCorrections
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
//MuonTriggerCorr
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"

//OverLap removal
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/OverlapRemovalDefs.h"
#include "AssociationUtils/IOverlapRemovalTool.h"

// B-tagging
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTagging/BTagging.h"

// jvt efficiency
#include "JetJvtEfficiency/JetJvtEfficiency.h"


//Missing Et
#include "METUtilities/METSystematicsTool.h"
#include "METUtilities/METMaker.h"
#include "METUtilities/CutsMETMaker.h"
#include "METInterface/IMETSystematicsTool.h"
#include "METInterface/IMETMaker.h"

//Tau tool
#include "TauAnalysisTools/TauSmearingTool.h"
#include "TauAnalysisTools/TauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "TauAnalysisTools/TauTruthTrackMatchingTool.h"
#include "TauAnalysisTools/TauOverlappingElectronLLHDecorator.h"



//******************Particle Container*******************

//=================MET===============
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETContainer.h"

//=================Electron==========
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/EgammaxAODHelpers.h"

//=================Tau===============
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauDefs.h" 
#include "xAODTau/TauJet.h"

//=================Jet===============
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/Jet.h"

//=================Photon============
#include "xAODEgamma/PhotonContainer.h"

//=================Muon==============
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"


//*********Forward webbunch**********
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"

#include "LFVUtils/MCSampleInfo.h"
#include "LFVUtils/MCSampleOverlap.h"


using namespace std;

#undef ClassDef
#define ClassDef(CLASS, NUM) 

class MyxAODAnalysis : public EL::AnaAlgorithm
{
	public:

		//cald0z0
		double d0cal;//!
		bool z0cal;//!

		string t_filename;//!
		string m_outputName;//!
		ofstream typeout;//!
		int m_EventNumber; //!
		int m_eventCounter; //!
		int m_numCleanEvents;//!
		int runNumber;//!
		double PRWwei; //!
		double Eventwei; //!
//		int RandomRunNumber;//!
		uint64_t t_nEventsProcessed  ; //!
		double t_sumOfWeights        ; //!
		double t_sumOfWeightsSquared ; //!

		vector<double> *vec_sumOfWeights; //!
		vector<double> *vec_sumOfEvents; //!
		vector<string> *vec_filename; //!

		TH1D *totalWeightedEntries; //!
		const double GeV = 0.001; //!
		const double MuonPDGMass = 105.658367e-3; //!
		const double ElectronPDGMass = 0.511*0.001; //!
		const double TauPDGMass = 1776.82*0.001; //!
		const double pi = 3.14159; //!

		string jetType = "AntiKt4EMTopoJets";//!
		string coreMetKey;//!
		string metAssocKey;//!

		int isMC; //!
		int OldDeriv; //!
		int Region;//!
		int isFlawless;//!
		string mcVersion;//!
		string Year;//!

		const string m_inputTag = "preORselected";//!
		const string m_outputTag = "isOR";//!

		TH1D *h_event_cutflow; //!
		TH1D *h_event_cutflow_weight; //!
		TH1D *h_event_sumweight; //!
		TH1D *h_ele_cutflow; //!
		TH1D *h_muon_cutflow; //!
		TH1D *h_muon_pt; //!
		TH1D *h_mass_2e; //!
		TH1D *h_mass_2m; //!
		TH1D *h_mass_em; //!
		TH1D *h_TPRecoEff_cutflow; //!
		TH1D *h_weight; //!
		TH1D *h_mu_iso1; //!
		TH1D *h_mu_iso2; //!
		TH1D *h_el_iso1; //!
		TH1D *h_el_iso2; //!
		TH1D *h_el_iso3; //!





		//***************WebBunch****************
		//  Trig::WebBunchCrossingTool *bct; //!
		//  Trig::WebBunchCrossingTool *m_bct; //!

		xAOD::TEvent* event; //!
		xAOD::TStore* m_store; //!
		std::string m_muTMstring;//!

		const xAOD::ElectronContainer* electrons;//!
		const xAOD::ElectronContainer* forelectrons;//!
		const xAOD::PhotonContainer* photons;//!
		const xAOD::JetContainer* jets;//!
		const xAOD::JetContainer* m_thjetCont;//!
		const xAOD::MuonContainer* muons;//!
		const xAOD::TauJetContainer* taus;//!
		const xAOD::EventInfo* eventInfo;//!
		const xAOD::VertexContainer* vxContainer;//!
		const xAOD::TruthParticleContainer* xTruthParticleContainer;//!
		std::vector<const xAOD::IParticle*> myParticles;//!
		//**************************General Tool************************
		//GRL
		asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!

		//PRW
		//		asg::AnaToolHandle<CP::IPileupReweightingTool> m_prwTool; //!
		asg::AnaToolHandle<CP::IPileupReweightingTool> pileuptool;//!

		//Isolation
		CP::IsolationSelectionTool *isoGradient_ele; //!
		CP::IsolationSelectionTool *isoLoose_ele; //!
		CP::IsolationSelectionTool *isoTrack_ele; //!
		CP::IsolationSelectionTool *isoHighpt_ele; //!
		CP::IsolationSelectionTool *isoTrack_mu; //!
		CP::IsolationSelectionTool *isoHighpt_mu; //!
		// CP::IsolationCorrectionTool* m_isoCorrTool; //!
		CP::IIsolationCorrectionTool* m_isoCorrTool; //!
		//--Electron--
		//Calibration
		asg::AnaToolHandle<CP::EgammaCalibrationAndSmearingTool> m_elcali; //!
		asg::AnaToolHandle<CP::EgammaCalibrationAndSmearingTool> m_phcali; //!
		//ID
		AsgElectronLikelihoodTool* m_MediumLH ;
		AsgElectronLikelihoodTool* m_LooseLH ;
		AsgElectronLikelihoodTool* m_TightLH ;
		//ElectronEfficiencyCorrection
		AsgElectronEfficiencyCorrectionTool *m_ID_SF; //!
		AsgElectronEfficiencyCorrectionTool *m_El_ID_SF_Tight; //!
		AsgElectronEfficiencyCorrectionTool *m_El_ID_SF_Medium; //!
		AsgElectronEfficiencyCorrectionTool *m_El_Reco_SF; //!
		AsgElectronEfficiencyCorrectionTool *m_Trig_SF; //!
		AsgElectronEfficiencyCorrectionTool *m_El_Trig_SF_Tight; //!
		AsgElectronEfficiencyCorrectionTool *m_El_Trig_SF_Medium; //!
		AsgElectronEfficiencyCorrectionTool *m_Trig_SF15; //!
		AsgElectronEfficiencyCorrectionTool *m_Trig_SF16; //!
		AsgElectronEfficiencyCorrectionTool *m_Iso_SF; //!
		AsgElectronEfficiencyCorrectionTool *m_El_Iso_SF_Tight; //!
		AsgElectronEfficiencyCorrectionTool *m_El_Iso_SF_Medium; //!
		AsgElectronEfficiencyCorrectionTool *m_Trig_Eff; //!
		AsgElectronEfficiencyCorrectionTool *m_Trig_Eff15; //!
		AsgElectronEfficiencyCorrectionTool *m_Trig_Eff16; //!
		AsgElectronEfficiencyCorrectionTool *m_El_Trig_Eff_Tight; //!
		AsgElectronEfficiencyCorrectionTool *m_El_Trig_Eff_Medium; //!

		//Gamma ID
		AsgPhotonIsEMSelector* m_photonTightIsEMSelector; //!

		//--Jet--
		asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibration; //!
		asg::AnaToolHandle<IJetSelector> m_cleaningTool; //!
		//JetVertexTagger
		asg::AnaToolHandle<IJetUpdateJvt> m_pjvtag; //!

		//JetUncertainty
		JetUncertaintiesTool *m_jesUncert; //!

		//jvt efficiency
		//		CP::JetJvtEfficiency *jvtsf; //!
		//		CP::JetJvtEfficiency *fjvtsf; //!
		asg::AnaToolHandle<CP::IJetJvtEfficiency> m_jvtsf; //!
		//ForwardJVT
		JetForwardJvtTool* m_jetFrwJvtTool;

		//--Muon--
		// MuonCalibrationAndSmearing and Selection
		//   asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection; //!
		asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTight; //!
		asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionHighPt; //!
		asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionMedium; //!
		asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionLoose; //!
		asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool2015; //!
		asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool2016; //!
		asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool2017; //!
		asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool2018; //!

		//MuonEfficiencyScaleFactors
		CP::MuonEfficiencyScaleFactors* m_Mu_Reco_SF_HighPt; //!

		//MuonIsolationScaleFactor
		CP::MuonEfficiencyScaleFactors* m_Mu_Iso_SF_HighPt; //!

		//TrackToVertexAssociation
		CP::MuonEfficiencyScaleFactors* m_Mu_TTVA_SF_HighPt; //!

		//MuonTriggerScaleFactors
		CP::MuonTriggerScaleFactors* m_Mu_Trig_SF_HighPt; //!

		//--Tau--
		asg::AnaToolHandle<TauAnalysisTools::ITauSmearingTool> TauSmeTool; //!
		asg::AnaToolHandle<TauAnalysisTools::ITauTruthMatchingTool> T2MT; //!
		asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool> TauSelTool; //!
		asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool> TauSelTool_NoOLR; //!
//		  TauAnalysisTools::TauSelectionTool *TauSelTool; //!
		asg::AnaToolHandle<TauAnalysisTools::ITauEfficiencyCorrectionsTool> m_Tau_Reco_SF;       //!
                asg::AnaToolHandle<TauAnalysisTools::ITauEfficiencyCorrectionsTool> m_Tau_ElOlr_SF;      //!
                asg::AnaToolHandle<TauAnalysisTools::ITauEfficiencyCorrectionsTool> m_Tau_JetID_SF;      //!




		//Trigger
		asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool; //!
		asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; //!
		//Trigger match
		asg::AnaToolHandle<Trig::IMatchingTool> m_trigMatch; //!


		//MET
		asg::AnaToolHandle<IMETMaker> metMaker; //!
		asg::AnaToolHandle<IMETSystematicsTool> metSystTool; //!

		//OverLap Removal
		asg::AnaToolHandle<ORUtils::IOverlapRemovalTool> m_overlapRemovalTool; //!
		ORUtils::ToolBox *toolBox; //!


		std::vector<CP::SystematicSet> m_sysList; //!
		std::vector<CP::SystematicSet>::const_iterator sysListItr; //!
		std::string sys_name; //!

		//B-tagging
		BTaggingSelectionTool* btagtool; //!
		BTaggingEfficiencyTool * btagefftool; //!



		xAOD::JetContainer* calibJets; //!
		//		AsgForwardElectronIsEMSelector* m_ForwardMedium; //!
		//		AsgForwardElectronIsEMSelector* m_ForwardLoose; //!

		std::map<std::string,int> periodStart; //!
		std::map<std::string,std::vector<std::string>> trigList_el; //!
		std::vector<std::string> *vec_triggers_el; //!
		string vw_triggers_elLFV[2];//!
		string vw_triggers_muLFV[1];//!
		string vw_triggers_el15LFV[2];//!
		//  asg::AnaToolHandle<Trig::WebBunchCrossingTool> bct; //!
		//===LFV
		LFVUtils::MCSampleInfo *m_mcInfo; //!
	        LFVUtils::MCSampleOverlap *m_mcOverlap; //!


		// this is a standard algorithm constructor
		MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
		virtual StatusCode fileExecute () override;
		// these are the functions inherited from Algorithm
		virtual StatusCode initialize () override;
		virtual StatusCode execute () override;
		virtual StatusCode finalize () override;

		virtual bool init(void){return true;};
		virtual bool loop(void){return true;};
		virtual bool finish(void){return true;}
		ClassDef(MyxAODAnalysisT, 1);




		//********************** 
		//******Selection*******
		//**********************

		//Pre-Selection
		bool passGRL(); //!
		bool passVTX(); //!
		bool passEventCleaning(); //!
		bool passTrigger(string *arr, int length); //!
		//  bool passTrigger(std::vector<std::string> vec_triggers); //!
		void prwWeight();//!
		//
		void electronIdentificationDecor(xAOD::Electron *el); //!
		void electronIsolationDecor(xAOD::Electron *el); //!
		void muonIsolationDecor(xAOD::Muon *mu); //!
		void d0z0CAL(const xAOD::TrackParticle *trk);//!
		void electronTriggerDecor(xAOD::Electron *el, string *arr, int size);//!
		void muonTriggerDecor(xAOD::Muon *mu,string *arr, int size);//!
		double GetMuonTriggerSF(const xAOD::IParticle *p );//!
		void muonIdentificationDecor(xAOD::Muon *mu);//!
		double GetKfactor();//!
		


};

#endif
